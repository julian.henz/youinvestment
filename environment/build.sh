#!/usr/bin/env bash

# Install composer and npm and build our frontend files
find . -name ".gitignore" -type f -print0 | xargs -0 /bin/rm -f
find . -name ".gitkeep" -type f -print0 | xargs -0 /bin/rm -f
cd craft/
composer install --prefer-dist --no-ansi --no-interaction --no-progress
cd assets
npm install
npm run build