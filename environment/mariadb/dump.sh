#!/usr/bin/env bash

DOCKER_CONTAINER="youInvestmentDb"
USER="root"
PASSWORD="2013liestal"
# TODO_INIT change
DATABASE="craft-youInvestment"
OUTPUT_FILE=./initdb/dump.sql

/usr/local/bin/docker exec ${DOCKER_CONTAINER} /usr/bin/mysqldump -u ${USER} --password=${PASSWORD} --databases ${DATABASE} > ${OUTPUT_FILE}
echo '>>> dump.sql has been successfully updated'
