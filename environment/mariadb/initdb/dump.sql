-- MySQL dump 10.16  Distrib 10.1.32-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: craft-youInvestment
-- ------------------------------------------------------
-- Server version	10.1.32-MariaDB-1~jessie

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `craft-youInvestment`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `craft-youInvestment` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `craft-youInvestment`;

--
-- Table structure for table `assetindexdata`
--

DROP TABLE IF EXISTS `assetindexdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assetindexdata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sessionId` varchar(36) NOT NULL DEFAULT '',
  `volumeId` int(11) NOT NULL,
  `uri` text,
  `size` bigint(20) unsigned DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `recordId` int(11) DEFAULT NULL,
  `inProgress` tinyint(1) DEFAULT '0',
  `completed` tinyint(1) DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `assetindexdata_sessionId_volumeId_idx` (`sessionId`,`volumeId`),
  KEY `assetindexdata_volumeId_idx` (`volumeId`),
  CONSTRAINT `assetindexdata_volumeId_fk` FOREIGN KEY (`volumeId`) REFERENCES `volumes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assetindexdata`
--

LOCK TABLES `assetindexdata` WRITE;
/*!40000 ALTER TABLE `assetindexdata` DISABLE KEYS */;
/*!40000 ALTER TABLE `assetindexdata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assets`
--

DROP TABLE IF EXISTS `assets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assets` (
  `id` int(11) NOT NULL,
  `volumeId` int(11) DEFAULT NULL,
  `folderId` int(11) NOT NULL,
  `uploaderId` int(11) DEFAULT NULL,
  `filename` varchar(255) NOT NULL,
  `kind` varchar(50) NOT NULL DEFAULT 'unknown',
  `width` int(11) unsigned DEFAULT NULL,
  `height` int(11) unsigned DEFAULT NULL,
  `size` bigint(20) unsigned DEFAULT NULL,
  `focalPoint` varchar(13) DEFAULT NULL,
  `deletedWithVolume` tinyint(1) DEFAULT NULL,
  `keptFile` tinyint(1) DEFAULT NULL,
  `dateModified` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `assets_filename_folderId_idx` (`filename`,`folderId`),
  KEY `assets_folderId_idx` (`folderId`),
  KEY `assets_volumeId_idx` (`volumeId`),
  KEY `assets_uploaderId_fk` (`uploaderId`),
  CONSTRAINT `assets_folderId_fk` FOREIGN KEY (`folderId`) REFERENCES `volumefolders` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assets_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `assets_uploaderId_fk` FOREIGN KEY (`uploaderId`) REFERENCES `users` (`id`) ON DELETE SET NULL,
  CONSTRAINT `assets_volumeId_fk` FOREIGN KEY (`volumeId`) REFERENCES `volumes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assets`
--

LOCK TABLES `assets` WRITE;
/*!40000 ALTER TABLE `assets` DISABLE KEYS */;
/*!40000 ALTER TABLE `assets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assettransformindex`
--

DROP TABLE IF EXISTS `assettransformindex`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assettransformindex` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assetId` int(11) NOT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `format` varchar(255) DEFAULT NULL,
  `location` varchar(255) NOT NULL,
  `volumeId` int(11) DEFAULT NULL,
  `fileExists` tinyint(1) NOT NULL DEFAULT '0',
  `inProgress` tinyint(1) NOT NULL DEFAULT '0',
  `error` tinyint(1) NOT NULL DEFAULT '0',
  `dateIndexed` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `assettransformindex_volumeId_assetId_location_idx` (`volumeId`,`assetId`,`location`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assettransformindex`
--

LOCK TABLES `assettransformindex` WRITE;
/*!40000 ALTER TABLE `assettransformindex` DISABLE KEYS */;
/*!40000 ALTER TABLE `assettransformindex` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assettransforms`
--

DROP TABLE IF EXISTS `assettransforms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assettransforms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `mode` enum('stretch','fit','crop') NOT NULL DEFAULT 'crop',
  `position` enum('top-left','top-center','top-right','center-left','center-center','center-right','bottom-left','bottom-center','bottom-right') NOT NULL DEFAULT 'center-center',
  `width` int(11) unsigned DEFAULT NULL,
  `height` int(11) unsigned DEFAULT NULL,
  `format` varchar(255) DEFAULT NULL,
  `quality` int(11) DEFAULT NULL,
  `interlace` enum('none','line','plane','partition') NOT NULL DEFAULT 'none',
  `dimensionChangeTime` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `assettransforms_name_idx` (`name`),
  KEY `assettransforms_handle_idx` (`handle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assettransforms`
--

LOCK TABLES `assettransforms` WRITE;
/*!40000 ALTER TABLE `assettransforms` DISABLE KEYS */;
/*!40000 ALTER TABLE `assettransforms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `parentId` int(11) DEFAULT NULL,
  `deletedWithGroup` tinyint(1) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `categories_groupId_idx` (`groupId`),
  KEY `categories_parentId_fk` (`parentId`),
  CONSTRAINT `categories_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `categorygroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `categories_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `categories_parentId_fk` FOREIGN KEY (`parentId`) REFERENCES `categories` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categorygroups`
--

DROP TABLE IF EXISTS `categorygroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorygroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `categorygroups_name_idx` (`name`),
  KEY `categorygroups_handle_idx` (`handle`),
  KEY `categorygroups_structureId_idx` (`structureId`),
  KEY `categorygroups_fieldLayoutId_idx` (`fieldLayoutId`),
  KEY `categorygroups_dateDeleted_idx` (`dateDeleted`),
  CONSTRAINT `categorygroups_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `categorygroups_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `structures` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorygroups`
--

LOCK TABLES `categorygroups` WRITE;
/*!40000 ALTER TABLE `categorygroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `categorygroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categorygroups_sites`
--

DROP TABLE IF EXISTS `categorygroups_sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorygroups_sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '1',
  `uriFormat` text,
  `template` varchar(500) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `categorygroups_sites_groupId_siteId_unq_idx` (`groupId`,`siteId`),
  KEY `categorygroups_sites_siteId_idx` (`siteId`),
  CONSTRAINT `categorygroups_sites_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `categorygroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `categorygroups_sites_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorygroups_sites`
--

LOCK TABLES `categorygroups_sites` WRITE;
/*!40000 ALTER TABLE `categorygroups_sites` DISABLE KEYS */;
/*!40000 ALTER TABLE `categorygroups_sites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `changedattributes`
--

DROP TABLE IF EXISTS `changedattributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `changedattributes` (
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `attribute` varchar(255) NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `propagated` tinyint(1) NOT NULL,
  `userId` int(11) DEFAULT NULL,
  PRIMARY KEY (`elementId`,`siteId`,`attribute`),
  KEY `changedattributes_elementId_siteId_dateUpdated_idx` (`elementId`,`siteId`,`dateUpdated`),
  KEY `changedattributes_siteId_fk` (`siteId`),
  KEY `changedattributes_userId_fk` (`userId`),
  CONSTRAINT `changedattributes_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `changedattributes_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `changedattributes_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `changedattributes`
--

LOCK TABLES `changedattributes` WRITE;
/*!40000 ALTER TABLE `changedattributes` DISABLE KEYS */;
/*!40000 ALTER TABLE `changedattributes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `changedfields`
--

DROP TABLE IF EXISTS `changedfields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `changedfields` (
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `propagated` tinyint(1) NOT NULL,
  `userId` int(11) DEFAULT NULL,
  PRIMARY KEY (`elementId`,`siteId`,`fieldId`),
  KEY `changedfields_elementId_siteId_dateUpdated_idx` (`elementId`,`siteId`,`dateUpdated`),
  KEY `changedfields_siteId_fk` (`siteId`),
  KEY `changedfields_fieldId_fk` (`fieldId`),
  KEY `changedfields_userId_fk` (`userId`),
  CONSTRAINT `changedfields_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `changedfields_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `changedfields_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `changedfields_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `changedfields`
--

LOCK TABLES `changedfields` WRITE;
/*!40000 ALTER TABLE `changedfields` DISABLE KEYS */;
/*!40000 ALTER TABLE `changedfields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content`
--

DROP TABLE IF EXISTS `content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  `field_firstTitle` varchar(40) DEFAULT NULL,
  `field_richText` text,
  `field_secondTitle` varchar(40) DEFAULT NULL,
  `field_seo` text,
  `field_titel` varchar(400) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  KEY `content_siteId_idx` (`siteId`),
  KEY `content_title_idx` (`title`),
  CONSTRAINT `content_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `content_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content`
--

LOCK TABLES `content` WRITE;
/*!40000 ALTER TABLE `content` DISABLE KEYS */;
INSERT INTO `content` VALUES (1,1,2,NULL,'2021-01-07 10:01:15','2021-01-07 10:01:15','fa324a15-bbb8-48c0-bbc7-0067eb208729',NULL,NULL,NULL,NULL,NULL),(2,2,2,NULL,'2021-05-21 13:41:00','2021-05-21 13:41:02','bc43ea6d-f285-457b-a553-fc79b5535f0e',NULL,NULL,NULL,NULL,NULL),(3,3,2,NULL,'2021-05-21 13:41:00','2021-05-21 13:41:01','47afc07e-911a-4fac-9d3e-8e0ff17b233b',NULL,NULL,NULL,NULL,NULL),(4,4,2,'Impressum','2021-05-21 13:41:01','2021-05-21 13:41:01','54d4bc88-e8c0-466b-8be7-84e512f5cbc0',NULL,NULL,NULL,'{\"titleRaw\":[],\"descriptionRaw\":\"\",\"keywords\":[],\"score\":\"neutral\",\"social\":{\"twitter\":{\"handle\":null,\"title\":\"Impressum - youInvestment\",\"imageId\":null,\"description\":\"\"},\"facebook\":{\"handle\":null,\"title\":\"Impressum - youInvestment\",\"imageId\":null,\"description\":\"\"}},\"advanced\":{\"robots\":[],\"canonical\":null}}',NULL),(5,5,2,'Impressum','2021-05-21 13:41:01','2021-05-21 13:41:01','b2262a16-9f7f-426d-b00b-b714b9bc0d97',NULL,NULL,NULL,'{\"titleRaw\":[],\"descriptionRaw\":\"\",\"keywords\":[],\"score\":\"neutral\",\"social\":{\"twitter\":{\"handle\":null,\"title\":\"Impressum - youInvestment\",\"imageId\":null,\"description\":\"\"},\"facebook\":{\"handle\":null,\"title\":\"Impressum - youInvestment\",\"imageId\":null,\"description\":\"\"}},\"advanced\":{\"robots\":[],\"canonical\":null}}',NULL),(6,6,2,'Datenschutz','2021-05-21 13:41:02','2021-05-21 13:41:02','a3b776b7-e51c-4498-86ac-77e8ab03db24',NULL,NULL,NULL,'{\"titleRaw\":[],\"descriptionRaw\":\"\",\"keywords\":[],\"score\":\"neutral\",\"social\":{\"twitter\":{\"handle\":null,\"title\":\"Datenschutz - youInvestment\",\"imageId\":null,\"description\":\"\"},\"facebook\":{\"handle\":null,\"title\":\"Datenschutz - youInvestment\",\"imageId\":null,\"description\":\"\"}},\"advanced\":{\"robots\":[],\"canonical\":null}}',NULL),(7,7,2,'Datenschutz','2021-05-21 13:41:02','2021-05-21 13:41:02','6fd676ca-4095-4680-b7d7-090af07f404d',NULL,NULL,NULL,'{\"titleRaw\":[],\"descriptionRaw\":\"\",\"keywords\":[],\"score\":\"neutral\",\"social\":{\"twitter\":{\"handle\":null,\"title\":\"Datenschutz - youInvestment\",\"imageId\":null,\"description\":\"\"},\"facebook\":{\"handle\":null,\"title\":\"Datenschutz - youInvestment\",\"imageId\":null,\"description\":\"\"}},\"advanced\":{\"robots\":[],\"canonical\":null}}',NULL),(8,8,2,'Home','2021-05-21 13:41:02','2021-05-21 13:41:02','ba6015b3-97a0-4bf0-9b1b-d6e824b27311',NULL,NULL,NULL,'{\"titleRaw\":[],\"descriptionRaw\":\"\",\"keywords\":[],\"score\":\"neutral\",\"social\":{\"twitter\":{\"handle\":null,\"title\":\"Home - youInvestment\",\"imageId\":null,\"description\":\"\"},\"facebook\":{\"handle\":null,\"title\":\"Home - youInvestment\",\"imageId\":null,\"description\":\"\"}},\"advanced\":{\"robots\":[],\"canonical\":null}}',NULL),(9,9,2,'Home','2021-05-21 13:41:02','2021-05-21 13:41:02','678e53e3-2dbe-40ce-a6d8-983f50103856',NULL,NULL,NULL,'{\"titleRaw\":[],\"descriptionRaw\":\"\",\"keywords\":[],\"score\":\"neutral\",\"social\":{\"twitter\":{\"handle\":null,\"title\":\"Home - youInvestment\",\"imageId\":null,\"description\":\"\"},\"facebook\":{\"handle\":null,\"title\":\"Home - youInvestment\",\"imageId\":null,\"description\":\"\"}},\"advanced\":{\"robots\":[],\"canonical\":null}}',NULL);
/*!40000 ALTER TABLE `content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `craftidtokens`
--

DROP TABLE IF EXISTS `craftidtokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `craftidtokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `accessToken` text NOT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craftidtokens_userId_fk` (`userId`),
  CONSTRAINT `craftidtokens_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `craftidtokens`
--

LOCK TABLES `craftidtokens` WRITE;
/*!40000 ALTER TABLE `craftidtokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `craftidtokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deprecationerrors`
--

DROP TABLE IF EXISTS `deprecationerrors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deprecationerrors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `fingerprint` varchar(255) NOT NULL,
  `lastOccurrence` datetime NOT NULL,
  `file` varchar(255) NOT NULL,
  `line` smallint(6) unsigned DEFAULT NULL,
  `message` text,
  `traces` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `deprecationerrors_key_fingerprint_unq_idx` (`key`,`fingerprint`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deprecationerrors`
--

LOCK TABLES `deprecationerrors` WRITE;
/*!40000 ALTER TABLE `deprecationerrors` DISABLE KEYS */;
/*!40000 ALTER TABLE `deprecationerrors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `drafts`
--

DROP TABLE IF EXISTS `drafts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `drafts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sourceId` int(11) DEFAULT NULL,
  `creatorId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `notes` text,
  `trackChanges` tinyint(1) NOT NULL DEFAULT '0',
  `dateLastMerged` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `drafts_creatorId_fk` (`creatorId`),
  KEY `drafts_sourceId_fk` (`sourceId`),
  CONSTRAINT `drafts_creatorId_fk` FOREIGN KEY (`creatorId`) REFERENCES `users` (`id`) ON DELETE SET NULL,
  CONSTRAINT `drafts_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `drafts`
--

LOCK TABLES `drafts` WRITE;
/*!40000 ALTER TABLE `drafts` DISABLE KEYS */;
/*!40000 ALTER TABLE `drafts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elementindexsettings`
--

DROP TABLE IF EXISTS `elementindexsettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elementindexsettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `settings` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `elementindexsettings_type_unq_idx` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elementindexsettings`
--

LOCK TABLES `elementindexsettings` WRITE;
/*!40000 ALTER TABLE `elementindexsettings` DISABLE KEYS */;
/*!40000 ALTER TABLE `elementindexsettings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elements`
--

DROP TABLE IF EXISTS `elements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `draftId` int(11) DEFAULT NULL,
  `revisionId` int(11) DEFAULT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `archived` tinyint(1) NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `elements_dateDeleted_idx` (`dateDeleted`),
  KEY `elements_fieldLayoutId_idx` (`fieldLayoutId`),
  KEY `elements_type_idx` (`type`),
  KEY `elements_enabled_idx` (`enabled`),
  KEY `elements_archived_dateCreated_idx` (`archived`,`dateCreated`),
  KEY `elements_archived_dateDeleted_draftId_revisionId_idx` (`archived`,`dateDeleted`,`draftId`,`revisionId`),
  KEY `elements_draftId_fk` (`draftId`),
  KEY `elements_revisionId_fk` (`revisionId`),
  CONSTRAINT `elements_draftId_fk` FOREIGN KEY (`draftId`) REFERENCES `drafts` (`id`) ON DELETE CASCADE,
  CONSTRAINT `elements_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `elements_revisionId_fk` FOREIGN KEY (`revisionId`) REFERENCES `revisions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elements`
--

LOCK TABLES `elements` WRITE;
/*!40000 ALTER TABLE `elements` DISABLE KEYS */;
INSERT INTO `elements` VALUES (1,NULL,NULL,NULL,'craft\\elements\\User',1,0,'2021-01-07 10:01:15','2021-01-07 10:01:15',NULL,'a8b2aeb0-81be-4f5b-a88a-df473e56f0be'),(2,NULL,NULL,5,'craft\\elements\\GlobalSet',1,0,'2021-05-21 13:41:00','2021-05-21 13:41:02',NULL,'24aee758-3b1e-4075-8529-4677b30ce2c7'),(3,NULL,NULL,6,'craft\\elements\\GlobalSet',1,0,'2021-05-21 13:41:00','2021-05-21 13:41:01',NULL,'760dd7bf-1d0f-4f6f-addf-6b3f732ad96b'),(4,NULL,NULL,11,'craft\\elements\\Entry',1,0,'2021-05-21 13:41:01','2021-05-21 13:41:01',NULL,'f1647640-ea21-4356-943f-ffe56ae421f4'),(5,NULL,1,11,'craft\\elements\\Entry',1,0,'2021-05-21 13:41:01','2021-05-21 13:41:01',NULL,'b8e340ba-0e10-4b55-b8ba-f7e2317f7c3a'),(6,NULL,NULL,12,'craft\\elements\\Entry',1,0,'2021-05-21 13:41:02','2021-05-21 13:41:02',NULL,'55f6e67d-1824-4e49-983c-60dc62bbe0e6'),(7,NULL,2,12,'craft\\elements\\Entry',1,0,'2021-05-21 13:41:02','2021-05-21 13:41:02',NULL,'4ce86b47-1036-478d-9dc3-dabdebd61e96'),(8,NULL,NULL,13,'craft\\elements\\Entry',1,0,'2021-05-21 13:41:02','2021-05-21 13:41:02',NULL,'469713a2-2f81-4f9a-9a84-c3285cc23364'),(9,NULL,3,13,'craft\\elements\\Entry',1,0,'2021-05-21 13:41:02','2021-05-21 13:41:02',NULL,'ea698050-7813-4c11-8693-3570fe9e6c6a');
/*!40000 ALTER TABLE `elements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `elements_sites`
--

DROP TABLE IF EXISTS `elements_sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elements_sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `uri` varchar(255) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `elements_sites_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  KEY `elements_sites_siteId_idx` (`siteId`),
  KEY `elements_sites_slug_siteId_idx` (`slug`,`siteId`),
  KEY `elements_sites_enabled_idx` (`enabled`),
  KEY `elements_sites_uri_siteId_idx` (`uri`,`siteId`),
  CONSTRAINT `elements_sites_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `elements_sites_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elements_sites`
--

LOCK TABLES `elements_sites` WRITE;
/*!40000 ALTER TABLE `elements_sites` DISABLE KEYS */;
INSERT INTO `elements_sites` VALUES (1,1,2,NULL,NULL,1,'2021-01-07 10:01:15','2021-01-07 10:01:15','9e934777-2869-4808-928a-2f2094665e22'),(2,2,2,NULL,NULL,1,'2021-05-21 13:41:00','2021-05-21 13:41:00','04e01a41-01b1-4465-93eb-df3c7b90ad14'),(3,3,2,NULL,NULL,1,'2021-05-21 13:41:00','2021-05-21 13:41:00','50eb2dbb-d2fe-408f-ba93-81eb1076fdfb'),(4,4,2,'impressum','impressum',1,'2021-05-21 13:41:01','2021-05-21 13:41:01','7bd1e80f-4180-4ec9-85ab-95dc84151d06'),(5,5,2,'impressum','impressum',1,'2021-05-21 13:41:01','2021-05-21 13:41:01','e825124d-16ed-44c7-bf48-e9796e87f740'),(6,6,2,'datenschutz','datenschutz',1,'2021-05-21 13:41:02','2021-05-21 13:41:02','d05a0823-2f0e-4a25-a339-07d0afbb4178'),(7,7,2,'datenschutz','datenschutz',1,'2021-05-21 13:41:02','2021-05-21 13:41:02','b2e6b400-301e-4656-86d7-50f6a9943311'),(8,8,2,'home','__home__',1,'2021-05-21 13:41:02','2021-05-21 13:41:02','ad1e2016-eb58-4f8f-b3a4-da3044364870'),(9,9,2,'home','__home__',1,'2021-05-21 13:41:02','2021-05-21 13:41:02','e3734049-76e1-4703-9300-b4f6df214f1b');
/*!40000 ALTER TABLE `elements_sites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entries`
--

DROP TABLE IF EXISTS `entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entries` (
  `id` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `parentId` int(11) DEFAULT NULL,
  `typeId` int(11) NOT NULL,
  `authorId` int(11) DEFAULT NULL,
  `postDate` datetime DEFAULT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `deletedWithEntryType` tinyint(1) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `entries_postDate_idx` (`postDate`),
  KEY `entries_expiryDate_idx` (`expiryDate`),
  KEY `entries_authorId_idx` (`authorId`),
  KEY `entries_sectionId_idx` (`sectionId`),
  KEY `entries_typeId_idx` (`typeId`),
  KEY `entries_parentId_fk` (`parentId`),
  CONSTRAINT `entries_authorId_fk` FOREIGN KEY (`authorId`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entries_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entries_parentId_fk` FOREIGN KEY (`parentId`) REFERENCES `entries` (`id`) ON DELETE SET NULL,
  CONSTRAINT `entries_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entries_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `entrytypes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entries`
--

LOCK TABLES `entries` WRITE;
/*!40000 ALTER TABLE `entries` DISABLE KEYS */;
INSERT INTO `entries` VALUES (4,1,NULL,1,NULL,'2021-05-21 13:41:00',NULL,NULL,'2021-05-21 13:41:01','2021-05-21 13:41:01','6aaed623-01c9-4e46-945e-45f093c3e8cb'),(5,1,NULL,1,NULL,'2021-05-21 13:41:00',NULL,NULL,'2021-05-21 13:41:01','2021-05-21 13:41:01','a03844b7-28fa-4c08-8796-d653704b99eb'),(6,3,NULL,2,NULL,'2021-05-21 13:41:00',NULL,NULL,'2021-05-21 13:41:02','2021-05-21 13:41:02','d583cc03-e1ce-4cf1-9a61-f8ba3dde94ee'),(7,3,NULL,2,NULL,'2021-05-21 13:41:00',NULL,NULL,'2021-05-21 13:41:02','2021-05-21 13:41:02','cdd0fab7-250f-49d6-8183-785f663ee21a'),(8,2,NULL,3,NULL,'2021-05-21 13:41:00',NULL,NULL,'2021-05-21 13:41:02','2021-05-21 13:41:02','d36ac9ae-316b-4407-9498-cc6521a2aafb'),(9,2,NULL,3,NULL,'2021-05-21 13:41:00',NULL,NULL,'2021-05-21 13:41:02','2021-05-21 13:41:02','bfbeaa3c-a897-4377-97b6-f80966492eca');
/*!40000 ALTER TABLE `entries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entrytypes`
--

DROP TABLE IF EXISTS `entrytypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entrytypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sectionId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `hasTitleField` tinyint(1) NOT NULL DEFAULT '1',
  `titleTranslationMethod` varchar(255) NOT NULL DEFAULT 'site',
  `titleTranslationKeyFormat` text,
  `titleFormat` varchar(255) DEFAULT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `entrytypes_name_sectionId_idx` (`name`,`sectionId`),
  KEY `entrytypes_handle_sectionId_idx` (`handle`,`sectionId`),
  KEY `entrytypes_sectionId_idx` (`sectionId`),
  KEY `entrytypes_fieldLayoutId_idx` (`fieldLayoutId`),
  KEY `entrytypes_dateDeleted_idx` (`dateDeleted`),
  CONSTRAINT `entrytypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `entrytypes_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entrytypes`
--

LOCK TABLES `entrytypes` WRITE;
/*!40000 ALTER TABLE `entrytypes` DISABLE KEYS */;
INSERT INTO `entrytypes` VALUES (1,1,11,'Impressum','impressum',0,'site',NULL,'{section.name|raw}',1,'2021-05-21 13:41:01','2021-05-21 13:41:01',NULL,'9d82683d-e563-44d0-877d-3c99a3cb629e'),(2,3,12,'Datenschutz','datenschutz',0,'site',NULL,'{section.name|raw}',1,'2021-05-21 13:41:02','2021-05-21 13:41:02',NULL,'46256c7e-4ba9-4e8c-86ab-ffed5ab28925'),(3,2,13,'Home','home',0,'site',NULL,'{section.name|raw}',1,'2021-05-21 13:41:02','2021-05-21 13:41:02',NULL,'403e009b-13b0-48cd-b608-fd12efc175cc');
/*!40000 ALTER TABLE `entrytypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fieldgroups`
--

DROP TABLE IF EXISTS `fieldgroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fieldgroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fieldgroups_name_idx` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fieldgroups`
--

LOCK TABLES `fieldgroups` WRITE;
/*!40000 ALTER TABLE `fieldgroups` DISABLE KEYS */;
INSERT INTO `fieldgroups` VALUES (2,'Common','2021-05-21 13:40:59','2021-05-21 13:40:59','a8fcab52-fc56-45a3-8fb2-23ed44993312');
/*!40000 ALTER TABLE `fieldgroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fieldlayoutfields`
--

DROP TABLE IF EXISTS `fieldlayoutfields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fieldlayoutfields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `layoutId` int(11) NOT NULL,
  `tabId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `fieldlayoutfields_layoutId_fieldId_unq_idx` (`layoutId`,`fieldId`),
  KEY `fieldlayoutfields_sortOrder_idx` (`sortOrder`),
  KEY `fieldlayoutfields_tabId_idx` (`tabId`),
  KEY `fieldlayoutfields_fieldId_idx` (`fieldId`),
  CONSTRAINT `fieldlayoutfields_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fieldlayoutfields_layoutId_fk` FOREIGN KEY (`layoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fieldlayoutfields_tabId_fk` FOREIGN KEY (`tabId`) REFERENCES `fieldlayouttabs` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fieldlayoutfields`
--

LOCK TABLES `fieldlayoutfields` WRITE;
/*!40000 ALTER TABLE `fieldlayoutfields` DISABLE KEYS */;
INSERT INTO `fieldlayoutfields` VALUES (34,11,11,6,1,1,'2021-05-21 13:41:01','2021-05-21 13:41:01','9262b7ca-91b5-4db5-8e80-2402cb69c78c'),(35,11,12,8,0,0,'2021-05-21 13:41:01','2021-05-21 13:41:01','69dc464f-74d6-4eb7-91c9-6ec08ba950fb'),(36,4,15,25,0,0,'2021-05-21 13:41:01','2021-05-21 13:41:01','d51b0289-fb52-47ee-95a6-60c2fb54f4b5'),(37,4,15,26,0,1,'2021-05-21 13:41:01','2021-05-21 13:41:01','a0ef3e0b-a220-4b55-9dee-26b52b27ae39'),(38,4,15,23,0,2,'2021-05-21 13:41:01','2021-05-21 13:41:01','98fc51cf-4d11-4acc-9560-bad3ed6218b1'),(39,4,15,27,0,3,'2021-05-21 13:41:01','2021-05-21 13:41:01','cd33cd8c-ceda-47e4-8041-24aa0e299e05'),(40,4,15,28,0,4,'2021-05-21 13:41:01','2021-05-21 13:41:01','34cac72d-9140-40e3-89ba-a4dea442abde'),(41,4,15,24,0,5,'2021-05-21 13:41:01','2021-05-21 13:41:01','882f9cc0-9ad6-441c-89b0-0f22acccc0d9'),(42,6,16,3,0,0,'2021-05-21 13:41:01','2021-05-21 13:41:01','f84d8084-fdb4-4576-8482-be8859b866a5'),(43,6,16,7,0,1,'2021-05-21 13:41:01','2021-05-21 13:41:01','873a915a-d38a-4721-88d2-ea8291f63a92'),(44,6,16,2,0,2,'2021-05-21 13:41:01','2021-05-21 13:41:01','6f593a41-3c96-4bdb-98e9-af24dae01741'),(45,6,16,4,0,3,'2021-05-21 13:41:01','2021-05-21 13:41:01','e495d8a6-f5bb-4960-a9a4-548a542cc3fa'),(46,7,17,30,1,0,'2021-05-21 13:41:01','2021-05-21 13:41:01','6a71bf46-4df4-4f13-a4c1-cb336679085c'),(47,7,17,33,0,1,'2021-05-21 13:41:01','2021-05-21 13:41:01','c7f5ad51-8481-4dff-b211-efd79008e4e2'),(48,7,17,31,0,2,'2021-05-21 13:41:01','2021-05-21 13:41:01','86f75d4a-75e0-4152-abd4-d817176f35cc'),(49,7,17,34,1,3,'2021-05-21 13:41:01','2021-05-21 13:41:01','b30486e6-3df2-4a78-8c14-e95797b5eacc'),(50,7,17,32,0,4,'2021-05-21 13:41:01','2021-05-21 13:41:01','c77e40a8-12ca-4707-8c6f-6ea149da9fd5'),(51,7,17,29,0,5,'2021-05-21 13:41:01','2021-05-21 13:41:01','ec124e56-730a-4f62-b41d-80a0bccd74e2'),(52,3,18,19,1,0,'2021-05-21 13:41:02','2021-05-21 13:41:02','a9adf9e8-7021-421c-ae4f-d4bcef65027f'),(53,3,18,20,1,1,'2021-05-21 13:41:02','2021-05-21 13:41:02','472874fb-d0d1-47a9-8d7a-264fbb23ee0d'),(54,3,18,21,1,2,'2021-05-21 13:41:02','2021-05-21 13:41:02','05d8e243-1bab-4254-9269-4e1025d844c4'),(55,3,18,22,1,3,'2021-05-21 13:41:02','2021-05-21 13:41:02','a6bf1e3a-c8a2-4626-a6d3-f77951743ade'),(56,10,19,36,1,0,'2021-05-21 13:41:02','2021-05-21 13:41:02','36c5b971-a4b5-4c3a-9ddb-8b6bed6b6832'),(57,10,19,37,1,1,'2021-05-21 13:41:02','2021-05-21 13:41:02','0bfd07f9-b0ba-4ac2-abbd-ec1127be3826'),(58,10,19,35,1,2,'2021-05-21 13:41:02','2021-05-21 13:41:02','fc4a7ca3-b559-4abd-9a10-19487ae824bf'),(59,2,20,17,1,0,'2021-05-21 13:41:02','2021-05-21 13:41:02','beec7178-3df5-447a-b7d9-13c4cdf84be0'),(60,2,20,16,1,1,'2021-05-21 13:41:02','2021-05-21 13:41:02','0c732b8b-e364-4b48-b2a5-a97f07e75a50'),(61,2,20,18,1,2,'2021-05-21 13:41:02','2021-05-21 13:41:02','38691fca-6d7e-4f66-9437-e74293bb9017'),(62,12,21,6,1,1,'2021-05-21 13:41:02','2021-05-21 13:41:02','37ddf553-97db-4647-aa94-7a60e537a3bf'),(63,12,22,8,0,0,'2021-05-21 13:41:02','2021-05-21 13:41:02','56d1accd-9085-460b-a056-6dd15740d054'),(64,1,23,15,0,0,'2021-05-21 13:41:02','2021-05-21 13:41:02','7e080309-90f3-4e07-a403-7cea89d62b15'),(65,1,23,13,0,1,'2021-05-21 13:41:02','2021-05-21 13:41:02','a03e94b7-c20d-40aa-a03f-74f779a48174'),(66,1,23,10,0,2,'2021-05-21 13:41:02','2021-05-21 13:41:02','ea40aebe-bbc5-4cc7-aa4d-065533e932e5'),(67,1,23,14,0,3,'2021-05-21 13:41:02','2021-05-21 13:41:02','12e39c34-8496-4bf9-92cd-862e9c4b311d'),(68,1,23,11,0,4,'2021-05-21 13:41:02','2021-05-21 13:41:02','ec1b7e50-bd6a-4d5f-adb7-ab035a0ffce5'),(69,1,23,12,0,5,'2021-05-21 13:41:02','2021-05-21 13:41:02','a1789205-28d8-49e2-9d56-49e124aab73e'),(70,5,24,1,0,0,'2021-05-21 13:41:02','2021-05-21 13:41:02','e887f225-6755-4c11-bb77-cba241739ac9'),(71,13,25,5,0,1,'2021-05-21 13:41:02','2021-05-21 13:41:02','854be380-5922-4009-91d3-15d922f57b1e'),(72,13,26,8,0,0,'2021-05-21 13:41:02','2021-05-21 13:41:02','b58824c6-99fc-4064-bf05-8cea71fc89ad');
/*!40000 ALTER TABLE `fieldlayoutfields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fieldlayouts`
--

DROP TABLE IF EXISTS `fieldlayouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fieldlayouts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fieldlayouts_dateDeleted_idx` (`dateDeleted`),
  KEY `fieldlayouts_type_idx` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fieldlayouts`
--

LOCK TABLES `fieldlayouts` WRITE;
/*!40000 ALTER TABLE `fieldlayouts` DISABLE KEYS */;
INSERT INTO `fieldlayouts` VALUES (1,'craft\\elements\\MatrixBlock','2021-05-21 13:41:00','2021-05-21 13:41:00',NULL,'c17e9d4b-19f4-4aaf-93b1-d698a9083473'),(2,'craft\\elements\\MatrixBlock','2021-05-21 13:41:00','2021-05-21 13:41:00',NULL,'38af1f6a-b125-4a33-9710-36d3f635fdea'),(3,'craft\\elements\\MatrixBlock','2021-05-21 13:41:00','2021-05-21 13:41:00',NULL,'9ef20be7-31a0-4833-b539-11f084cfac44'),(4,'craft\\elements\\MatrixBlock','2021-05-21 13:41:00','2021-05-21 13:41:00',NULL,'ca14a160-af33-4fff-9cd9-8c31e6a6593b'),(5,'craft\\elements\\GlobalSet','2021-05-21 13:41:00','2021-05-21 13:41:00',NULL,'7e902028-73bf-4b93-9261-8b477cbeebe6'),(6,'craft\\elements\\GlobalSet','2021-05-21 13:41:00','2021-05-21 13:41:00',NULL,'3e1f527f-210e-4de0-9c52-093dc408e077'),(7,'craft\\elements\\MatrixBlock','2021-05-21 13:41:00','2021-05-21 13:41:00',NULL,'7027674a-06cc-43d1-8272-3bc9cabb1fed'),(8,'craft\\elements\\Asset','2021-05-21 13:41:01','2021-05-21 13:41:01',NULL,'66a3acfc-9e2f-462d-9363-5206d283791b'),(9,'craft\\elements\\Asset','2021-05-21 13:41:01','2021-05-21 13:41:01',NULL,'e0e4c89c-9b93-4d18-a065-4efd0861be5d'),(10,'craft\\elements\\MatrixBlock','2021-05-21 13:41:01','2021-05-21 13:41:01',NULL,'4982bec9-f34e-403e-b46f-0bcc1384757e'),(11,'craft\\elements\\Entry','2021-05-21 13:41:01','2021-05-21 13:41:01',NULL,'3bb8c342-fb0b-4cc7-a550-be7e745290bf'),(12,'craft\\elements\\Entry','2021-05-21 13:41:02','2021-05-21 13:41:02',NULL,'4d150b40-cb00-432e-b523-57410612ef34'),(13,'craft\\elements\\Entry','2021-05-21 13:41:02','2021-05-21 13:41:02',NULL,'c4ae2dd6-599c-4e06-a4b3-d48464040538');
/*!40000 ALTER TABLE `fieldlayouts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fieldlayouttabs`
--

DROP TABLE IF EXISTS `fieldlayouttabs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fieldlayouttabs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `layoutId` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `elements` text,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fieldlayouttabs_sortOrder_idx` (`sortOrder`),
  KEY `fieldlayouttabs_layoutId_idx` (`layoutId`),
  CONSTRAINT `fieldlayouttabs_layoutId_fk` FOREIGN KEY (`layoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fieldlayouttabs`
--

LOCK TABLES `fieldlayouttabs` WRITE;
/*!40000 ALTER TABLE `fieldlayouttabs` DISABLE KEYS */;
INSERT INTO `fieldlayouttabs` VALUES (11,11,'Content','[{\"type\":\"craft\\\\fieldlayoutelements\\\\EntryTitleField\",\"autocomplete\":false,\"class\":null,\"size\":null,\"name\":null,\"autocorrect\":true,\"autocapitalize\":true,\"disabled\":false,\"readonly\":false,\"title\":null,\"placeholder\":null,\"step\":null,\"min\":null,\"max\":null,\"requirable\":false,\"id\":null,\"containerAttributes\":[],\"inputContainerAttributes\":[],\"labelAttributes\":[],\"orientation\":null,\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"width\":100},{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":\"\",\"instructions\":\"\",\"tip\":null,\"warning\":null,\"required\":\"1\",\"width\":100,\"fieldUid\":\"57dee96c-7db3-4f24-aecd-0247689d33af\"}]',1,'2021-05-21 13:41:01','2021-05-21 13:41:01','daa37074-ec57-4a13-98ca-0001d39ef82a'),(12,11,'SEO','[{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":false,\"width\":100,\"fieldUid\":\"3051443f-8b0d-4289-b9aa-32d2fb661d04\"}]',2,'2021-05-21 13:41:01','2021-05-21 13:41:01','bbad1c80-9612-4deb-bdd8-ed6e472bf585'),(13,9,'Content','[{\"type\":\"craft\\\\fieldlayoutelements\\\\TitleField\",\"autocomplete\":false,\"class\":null,\"size\":null,\"name\":null,\"autocorrect\":true,\"autocapitalize\":true,\"disabled\":false,\"readonly\":false,\"title\":null,\"placeholder\":null,\"step\":null,\"min\":null,\"max\":null,\"requirable\":false,\"id\":null,\"containerAttributes\":[],\"inputContainerAttributes\":[],\"labelAttributes\":[],\"orientation\":null,\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"width\":100}]',1,'2021-05-21 13:41:01','2021-05-21 13:41:01','765ede4b-3aea-4d21-980b-ea38315f732a'),(14,8,'Content','[{\"type\":\"craft\\\\fieldlayoutelements\\\\TitleField\",\"autocomplete\":false,\"class\":null,\"size\":null,\"name\":null,\"autocorrect\":true,\"autocapitalize\":true,\"disabled\":false,\"readonly\":false,\"title\":null,\"placeholder\":null,\"step\":null,\"min\":null,\"max\":null,\"requirable\":false,\"id\":null,\"containerAttributes\":[],\"inputContainerAttributes\":[],\"labelAttributes\":[],\"orientation\":null,\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"width\":100}]',1,'2021-05-21 13:41:01','2021-05-21 13:41:01','cfde486d-e38d-4bcf-a962-af78f8476860'),(15,4,'Content','[{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":false,\"width\":100,\"fieldUid\":\"94ac48cd-56f4-4a1e-b253-642e578a7ff8\"},{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":false,\"width\":100,\"fieldUid\":\"d0f093c3-ec7a-485f-aa72-47422fbb714a\"},{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":false,\"width\":100,\"fieldUid\":\"4b102177-d991-4a25-89dd-c901bfb85531\"},{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":false,\"width\":100,\"fieldUid\":\"db31c084-1776-4689-80ab-a9d0f63570ac\"},{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":false,\"width\":100,\"fieldUid\":\"e8093e48-1a6c-41ad-a551-ea40c773ad42\"},{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":false,\"width\":100,\"fieldUid\":\"5fcf02d4-ca79-4845-b989-9b3206a156e1\"}]',1,'2021-05-21 13:41:01','2021-05-21 13:41:01','ac41f2fa-d3f8-4b1e-b504-9b413cc0835a'),(16,6,'Tab 1','[{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":false,\"width\":100,\"fieldUid\":\"138e98a9-42de-48bc-b510-6d191e15e10d\"},{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":false,\"width\":100,\"fieldUid\":\"2516d21b-a4cf-4c46-b249-36b8cda1c705\"},{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":false,\"width\":100,\"fieldUid\":\"8a447b6c-8baf-4bde-8ff8-1983ae8638f8\"},{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":false,\"width\":100,\"fieldUid\":\"bb73c66e-f14a-4f5c-856b-c4869e2aa7e5\"}]',1,'2021-05-21 13:41:01','2021-05-21 13:41:01','d4ef254d-4a42-4b1c-8a77-33f83b08e9c1'),(17,7,'Content','[{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":true,\"width\":100,\"fieldUid\":\"651a4895-d59b-4cef-9d66-fb958fafdcd1\"},{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":false,\"width\":100,\"fieldUid\":\"cf74e9d8-ee9a-478e-b286-029335d149cf\"},{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":false,\"width\":100,\"fieldUid\":\"99d95261-96b1-4f80-b7f3-48b820ff449f\"},{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":true,\"width\":100,\"fieldUid\":\"f111dd8f-f1ee-4aa9-97e2-a1d0941b9dc4\"},{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":false,\"width\":100,\"fieldUid\":\"bf2e5a22-00a1-4932-b368-d9d4bb0bae6e\"},{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":false,\"width\":100,\"fieldUid\":\"4be8fe22-9a7d-4562-bd37-a6c2f75e4fc8\"}]',1,'2021-05-21 13:41:01','2021-05-21 13:41:01','9d6de913-866f-49b3-8ce9-91601be26df0'),(18,3,'Content','[{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":true,\"width\":100,\"fieldUid\":\"4a513921-33e4-41ab-964f-76e8ab5a7ec1\"},{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":true,\"width\":100,\"fieldUid\":\"7abc4ce2-f551-4614-bca6-c7cc4c8fda6a\"},{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":true,\"width\":100,\"fieldUid\":\"ceba9e1b-8ab6-4012-8805-3b9581af35fc\"},{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":true,\"width\":100,\"fieldUid\":\"f142b145-8349-4a9f-b9d5-6b1b815b6bb7\"}]',1,'2021-05-21 13:41:02','2021-05-21 13:41:02','e0b4f176-64e8-4e78-b4e2-a6d83cc143f7'),(19,10,'Content','[{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":true,\"width\":100,\"fieldUid\":\"c27a80ef-83b3-4da1-a3b4-170cb07b16ef\"},{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":true,\"width\":100,\"fieldUid\":\"e9bdc3b6-4eb6-4043-8b52-813ac45b5aca\"},{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":true,\"width\":100,\"fieldUid\":\"433925d5-4dc7-4a35-8a80-42573edd3c1e\"}]',1,'2021-05-21 13:41:02','2021-05-21 13:41:02','33a49a40-c7f3-4ba0-83f3-5e84226357e0'),(20,2,'Content','[{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":true,\"width\":100,\"fieldUid\":\"7aa2a284-f4bc-4917-989a-cf71fc62aef4\"},{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":true,\"width\":100,\"fieldUid\":\"35e42c00-24b4-4493-a114-6100b1e82b46\"},{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":true,\"width\":100,\"fieldUid\":\"d9014845-730e-4aab-be4f-d2a9ae7039a9\"}]',1,'2021-05-21 13:41:02','2021-05-21 13:41:02','941f486f-505c-4ca4-befa-617654d72add'),(21,12,'Inhalt','[{\"type\":\"craft\\\\fieldlayoutelements\\\\EntryTitleField\",\"autocomplete\":false,\"class\":null,\"size\":null,\"name\":null,\"autocorrect\":true,\"autocapitalize\":true,\"disabled\":false,\"readonly\":false,\"title\":null,\"placeholder\":null,\"step\":null,\"min\":null,\"max\":null,\"requirable\":false,\"id\":null,\"containerAttributes\":[],\"inputContainerAttributes\":[],\"labelAttributes\":[],\"orientation\":null,\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"width\":100},{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":\"\",\"instructions\":\"\",\"tip\":null,\"warning\":null,\"required\":\"1\",\"width\":100,\"fieldUid\":\"57dee96c-7db3-4f24-aecd-0247689d33af\"}]',1,'2021-05-21 13:41:02','2021-05-21 13:41:02','eca79483-d824-45cd-b02f-858ec9239c27'),(22,12,'SEO','[{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":false,\"width\":100,\"fieldUid\":\"3051443f-8b0d-4289-b9aa-32d2fb661d04\"}]',2,'2021-05-21 13:41:02','2021-05-21 13:41:02','77356456-5af0-4e4e-8903-3375be20d116'),(23,1,'Content','[{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":false,\"width\":100,\"fieldUid\":\"daa6ae63-4514-429c-92ae-3662b6c0c8b7\"},{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":false,\"width\":100,\"fieldUid\":\"5c08b56b-7d90-4196-b7dc-3ce429006d54\"},{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":false,\"width\":100,\"fieldUid\":\"0b9f6e5a-e94a-432f-8bc5-5e60f076bf1d\"},{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":false,\"width\":100,\"fieldUid\":\"7ce298ad-35b8-47ee-8ed9-7ac00e81eda3\"},{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":false,\"width\":100,\"fieldUid\":\"3b6ac2b4-44b5-4dd3-a94f-41d5950b6385\"},{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":false,\"width\":100,\"fieldUid\":\"53a6f531-cd7b-481b-ac60-9ab3593a547a\"}]',1,'2021-05-21 13:41:02','2021-05-21 13:41:02','3f1942b1-4e9d-41e4-a916-8d9668cf780c'),(24,5,'Tab 1','[{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":false,\"width\":100,\"fieldUid\":\"0a46a4ed-9556-4b96-84fa-d8eda3ade2ba\"}]',1,'2021-05-21 13:41:02','2021-05-21 13:41:02','e7256b4c-37ff-4976-a5ac-1b01e3984ddf'),(25,13,'Content','[{\"type\":\"craft\\\\fieldlayoutelements\\\\EntryTitleField\",\"autocomplete\":false,\"class\":null,\"size\":null,\"name\":null,\"autocorrect\":true,\"autocapitalize\":true,\"disabled\":false,\"readonly\":false,\"title\":null,\"placeholder\":null,\"step\":null,\"min\":null,\"max\":null,\"requirable\":false,\"id\":null,\"containerAttributes\":[],\"inputContainerAttributes\":[],\"labelAttributes\":[],\"orientation\":null,\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"width\":100},{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":false,\"width\":100,\"fieldUid\":\"2f04b707-164a-4ec2-aaa7-c570fc70a429\"}]',1,'2021-05-21 13:41:02','2021-05-21 13:41:02','f702bfef-6e03-4c10-bd30-5a5cdb9bfcf9'),(26,13,'SEO','[{\"type\":\"craft\\\\fieldlayoutelements\\\\CustomField\",\"label\":null,\"instructions\":null,\"tip\":null,\"warning\":null,\"required\":false,\"width\":100,\"fieldUid\":\"3051443f-8b0d-4289-b9aa-32d2fb661d04\"}]',2,'2021-05-21 13:41:02','2021-05-21 13:41:02','4fffc896-277d-4a50-8d48-431171d1bf33');
/*!40000 ALTER TABLE `fieldlayouttabs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fields`
--

DROP TABLE IF EXISTS `fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(64) NOT NULL,
  `context` varchar(255) NOT NULL DEFAULT 'global',
  `instructions` text,
  `searchable` tinyint(1) NOT NULL DEFAULT '1',
  `translationMethod` varchar(255) NOT NULL DEFAULT 'none',
  `translationKeyFormat` text,
  `type` varchar(255) NOT NULL,
  `settings` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fields_handle_context_idx` (`handle`,`context`),
  KEY `fields_groupId_idx` (`groupId`),
  KEY `fields_context_idx` (`context`),
  CONSTRAINT `fields_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `fieldgroups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fields`
--

LOCK TABLES `fields` WRITE;
/*!40000 ALTER TABLE `fields` DISABLE KEYS */;
INSERT INTO `fields` VALUES (1,2,'Adresse','adresse','global','',0,'site',NULL,'craft\\fields\\Matrix','{\"contentTable\":\"{{%matrixcontent_adresse}}\",\"maxBlocks\":\"\",\"minBlocks\":\"\",\"propagationMethod\":\"all\"}','2021-05-21 13:40:59','2021-05-21 13:40:59','0a46a4ed-9556-4b96-84fa-d8eda3ade2ba'),(2,2,'Feature Image','featureImage','global','',0,'site',NULL,'craft\\fields\\Assets','{\"allowSelfRelations\":false,\"allowUploads\":true,\"allowedKinds\":[\"image\"],\"defaultUploadLocationSource\":\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\",\"defaultUploadLocationSubpath\":\"\",\"limit\":\"1\",\"localizeRelations\":false,\"previewMode\":\"full\",\"restrictFiles\":\"1\",\"selectionLabel\":\"Add Feature Image\",\"showSiteMenu\":false,\"showUnpermittedFiles\":false,\"showUnpermittedVolumes\":false,\"singleUploadLocationSource\":\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\",\"singleUploadLocationSubpath\":\"\",\"source\":null,\"sources\":[\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"],\"targetSiteId\":null,\"useSingleFolder\":false,\"validateRelatedElements\":false,\"viewMode\":\"large\"}','2021-05-21 13:40:59','2021-05-21 13:40:59','8a447b6c-8baf-4bde-8ff8-1983ae8638f8'),(3,2,'FirstTitle','firstTitle','global','',0,'none',NULL,'craft\\fields\\PlainText','{\"byteLimit\":null,\"charLimit\":10,\"code\":\"\",\"columnType\":null,\"initialRows\":\"4\",\"multiline\":\"\",\"placeholder\":\"\",\"uiMode\":\"normal\"}','2021-05-21 13:40:59','2021-05-21 13:40:59','138e98a9-42de-48bc-b510-6d191e15e10d'),(4,2,'Startseite Bild','homeNavigationImage','global','',0,'site',NULL,'craft\\fields\\Assets','{\"allowSelfRelations\":false,\"allowUploads\":true,\"allowedKinds\":null,\"defaultUploadLocationSource\":\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\",\"defaultUploadLocationSubpath\":\"\",\"limit\":\"\",\"localizeRelations\":false,\"previewMode\":\"full\",\"restrictFiles\":\"\",\"selectionLabel\":\"\",\"showSiteMenu\":false,\"showUnpermittedFiles\":false,\"showUnpermittedVolumes\":false,\"singleUploadLocationSource\":\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\",\"singleUploadLocationSubpath\":\"\",\"source\":null,\"sources\":[\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"],\"targetSiteId\":null,\"useSingleFolder\":false,\"validateRelatedElements\":false,\"viewMode\":\"list\"}','2021-05-21 13:40:59','2021-05-21 13:40:59','bb73c66e-f14a-4f5c-856b-c4869e2aa7e5'),(5,2,'SeitenInhalte','pageEntrys','global','',0,'site',NULL,'craft\\fields\\Matrix','{\"contentTable\":\"{{%matrixcontent_pageentrys}}\",\"maxBlocks\":\"\",\"minBlocks\":\"\",\"propagationMethod\":\"all\"}','2021-05-21 13:40:59','2021-05-21 13:40:59','2f04b707-164a-4ec2-aaa7-c570fc70a429'),(6,2,'Rich Text','richText','global','',0,'none',NULL,'craft\\redactor\\Field','{\"availableTransforms\":\"*\",\"availableVolumes\":\"*\",\"cleanupHtml\":true,\"columnType\":\"text\",\"configSelectionMode\":\"choose\",\"defaultTransform\":\"\",\"manualConfig\":\"\",\"purifierConfig\":\"\",\"purifyHtml\":\"1\",\"redactorConfig\":\"Simple.json\",\"removeEmptyTags\":\"1\",\"removeInlineStyles\":\"1\",\"removeNbsp\":\"1\",\"showHtmlButtonForNonAdmins\":\"\",\"showUnpermittedFiles\":false,\"showUnpermittedVolumes\":false,\"uiMode\":\"enlarged\"}','2021-05-21 13:40:59','2021-05-21 13:40:59','57dee96c-7db3-4f24-aecd-0247689d33af'),(7,2,'SecondTitle','secondTitle','global','',0,'none',NULL,'craft\\fields\\PlainText','{\"byteLimit\":null,\"charLimit\":10,\"code\":\"\",\"columnType\":null,\"initialRows\":\"4\",\"multiline\":\"\",\"placeholder\":\"\",\"uiMode\":\"normal\"}','2021-05-21 13:40:59','2021-05-21 13:40:59','2516d21b-a4cf-4c46-b249-36b8cda1c705'),(8,2,'SEO','seo','global','',0,'none',NULL,'ether\\seo\\fields\\SeoField','{\"description\":\"\",\"hideSocial\":\"\",\"robots\":[\"\",\"\",\"\",\"\",\"\",\"\"],\"socialImage\":\"\",\"suffixAsPrefix\":null,\"title\":[{\"key\":\"1\",\"locked\":\"0\",\"template\":\"{title}\"},{\"key\":\"2\",\"locked\":\"1\",\"template\":\" - {{ siteName }}\"}],\"titleSuffix\":null}','2021-05-21 13:41:00','2021-05-21 13:41:00','3051443f-8b0d-4289-b9aa-32d2fb661d04'),(9,2,'Titel','titel','global','',0,'none',NULL,'craft\\fields\\PlainText','{\"byteLimit\":null,\"charLimit\":100,\"code\":\"\",\"columnType\":null,\"initialRows\":\"4\",\"multiline\":\"\",\"placeholder\":\"\",\"uiMode\":\"normal\"}','2021-05-21 13:41:00','2021-05-21 13:41:00','b90564fa-6300-465d-b445-7295838b8730'),(10,NULL,'Postleitzahl','plz','matrixBlockType:5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88','',0,'none',NULL,'craft\\fields\\PlainText','{\"byteLimit\":null,\"charLimit\":null,\"code\":\"\",\"columnType\":null,\"initialRows\":\"4\",\"multiline\":\"\",\"placeholder\":\"\",\"uiMode\":\"normal\"}','2021-05-21 13:41:00','2021-05-21 13:41:00','0b9f6e5a-e94a-432f-8bc5-5e60f076bf1d'),(11,NULL,'Telefon','phone','matrixBlockType:5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88','',0,'none',NULL,'craft\\fields\\PlainText','{\"byteLimit\":null,\"charLimit\":null,\"code\":\"\",\"columnType\":null,\"initialRows\":\"4\",\"multiline\":\"\",\"placeholder\":\"\",\"uiMode\":\"normal\"}','2021-05-21 13:41:00','2021-05-21 13:41:00','3b6ac2b4-44b5-4dd3-a94f-41d5950b6385'),(12,NULL,'Email','mail','matrixBlockType:5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88','',0,'none',NULL,'craft\\fields\\PlainText','{\"byteLimit\":null,\"charLimit\":null,\"code\":\"\",\"columnType\":null,\"initialRows\":\"4\",\"multiline\":\"\",\"placeholder\":\"\",\"uiMode\":\"normal\"}','2021-05-21 13:41:00','2021-05-21 13:41:00','53a6f531-cd7b-481b-ac60-9ab3593a547a'),(13,NULL,'Strasse, Nr','street','matrixBlockType:5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88','',0,'none',NULL,'craft\\fields\\PlainText','{\"byteLimit\":null,\"charLimit\":null,\"code\":\"\",\"columnType\":null,\"initialRows\":\"4\",\"multiline\":\"\",\"placeholder\":\"\",\"uiMode\":\"normal\"}','2021-05-21 13:41:00','2021-05-21 13:41:00','5c08b56b-7d90-4196-b7dc-3ce429006d54'),(14,NULL,'Stadt','city','matrixBlockType:5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88','',0,'none',NULL,'craft\\fields\\PlainText','{\"byteLimit\":null,\"charLimit\":null,\"code\":\"\",\"columnType\":null,\"initialRows\":\"4\",\"multiline\":\"\",\"placeholder\":\"\",\"uiMode\":\"normal\"}','2021-05-21 13:41:00','2021-05-21 13:41:00','7ce298ad-35b8-47ee-8ed9-7ac00e81eda3'),(15,NULL,'Firma','company','matrixBlockType:5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88','',0,'none',NULL,'craft\\fields\\PlainText','{\"byteLimit\":null,\"charLimit\":null,\"code\":\"\",\"columnType\":null,\"initialRows\":\"4\",\"multiline\":\"\",\"placeholder\":\"\",\"uiMode\":\"normal\"}','2021-05-21 13:41:00','2021-05-21 13:41:00','daa6ae63-4514-429c-92ae-3662b6c0c8b7'),(16,NULL,'Text','text','matrixBlockType:3941620b-2988-47eb-8e03-c0d5596d2850','',0,'none',NULL,'craft\\redactor\\Field','{\"availableTransforms\":\"*\",\"availableVolumes\":\"*\",\"cleanupHtml\":true,\"columnType\":\"text\",\"configSelectionMode\":\"choose\",\"defaultTransform\":\"\",\"manualConfig\":\"\",\"purifierConfig\":\"\",\"purifyHtml\":\"1\",\"redactorConfig\":\"\",\"removeEmptyTags\":\"1\",\"removeInlineStyles\":\"1\",\"removeNbsp\":\"1\",\"showHtmlButtonForNonAdmins\":\"\",\"showUnpermittedFiles\":false,\"showUnpermittedVolumes\":false,\"uiMode\":\"enlarged\"}','2021-05-21 13:41:00','2021-05-21 13:41:00','35e42c00-24b4-4493-a114-6100b1e82b46'),(17,NULL,'Titel','titel','matrixBlockType:3941620b-2988-47eb-8e03-c0d5596d2850','',0,'none',NULL,'craft\\fields\\PlainText','{\"byteLimit\":null,\"charLimit\":100,\"code\":\"\",\"columnType\":null,\"initialRows\":\"4\",\"multiline\":\"\",\"placeholder\":\"\",\"uiMode\":\"normal\"}','2021-05-21 13:41:00','2021-05-21 13:41:00','7aa2a284-f4bc-4917-989a-cf71fc62aef4'),(18,NULL,'Bild','image','matrixBlockType:3941620b-2988-47eb-8e03-c0d5596d2850','',0,'site',NULL,'craft\\fields\\Assets','{\"allowSelfRelations\":false,\"allowUploads\":true,\"allowedKinds\":null,\"defaultUploadLocationSource\":\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\",\"defaultUploadLocationSubpath\":\"\",\"limit\":\"1\",\"localizeRelations\":false,\"previewMode\":\"full\",\"restrictFiles\":\"\",\"selectionLabel\":\"\",\"showSiteMenu\":true,\"showUnpermittedFiles\":false,\"showUnpermittedVolumes\":false,\"singleUploadLocationSource\":\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\",\"singleUploadLocationSubpath\":\"\",\"source\":null,\"sources\":[\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"],\"targetSiteId\":null,\"useSingleFolder\":false,\"validateRelatedElements\":false,\"viewMode\":\"list\"}','2021-05-21 13:41:00','2021-05-21 13:41:00','d9014845-730e-4aab-be4f-d2a9ae7039a9'),(19,NULL,'Navigation Bild','navigationImage','matrixBlockType:34079b35-1790-43d7-969a-07e64284d68b','',0,'site',NULL,'craft\\fields\\Assets','{\"allowSelfRelations\":false,\"allowUploads\":true,\"allowedKinds\":null,\"defaultUploadLocationSource\":\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\",\"defaultUploadLocationSubpath\":\"\",\"limit\":\"1\",\"localizeRelations\":false,\"previewMode\":\"full\",\"restrictFiles\":\"\",\"selectionLabel\":\"\",\"showSiteMenu\":true,\"showUnpermittedFiles\":false,\"showUnpermittedVolumes\":false,\"singleUploadLocationSource\":\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\",\"singleUploadLocationSubpath\":\"\",\"source\":null,\"sources\":[\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"],\"targetSiteId\":null,\"useSingleFolder\":false,\"validateRelatedElements\":false,\"viewMode\":\"list\"}','2021-05-21 13:41:00','2021-05-21 13:41:00','4a513921-33e4-41ab-964f-76e8ab5a7ec1'),(20,NULL,'Kleiner Titel','smalltitel','matrixBlockType:34079b35-1790-43d7-969a-07e64284d68b','',0,'none',NULL,'craft\\fields\\PlainText','{\"byteLimit\":null,\"charLimit\":100,\"code\":\"\",\"columnType\":null,\"initialRows\":\"4\",\"multiline\":\"\",\"placeholder\":\"\",\"uiMode\":\"normal\"}','2021-05-21 13:41:00','2021-05-21 13:41:00','7abc4ce2-f551-4614-bca6-c7cc4c8fda6a'),(21,NULL,'Titel','biggTitle','matrixBlockType:34079b35-1790-43d7-969a-07e64284d68b','',0,'none',NULL,'craft\\redactor\\Field','{\"availableTransforms\":\"*\",\"availableVolumes\":\"*\",\"cleanupHtml\":true,\"columnType\":\"text\",\"configSelectionMode\":\"choose\",\"defaultTransform\":\"\",\"manualConfig\":\"\",\"purifierConfig\":\"\",\"purifyHtml\":\"1\",\"redactorConfig\":\"\",\"removeEmptyTags\":\"1\",\"removeInlineStyles\":\"1\",\"removeNbsp\":\"1\",\"showHtmlButtonForNonAdmins\":\"\",\"showUnpermittedFiles\":false,\"showUnpermittedVolumes\":false,\"uiMode\":\"enlarged\"}','2021-05-21 13:41:00','2021-05-21 13:41:00','ceba9e1b-8ab6-4012-8805-3b9581af35fc'),(22,NULL,'Bild','image','matrixBlockType:34079b35-1790-43d7-969a-07e64284d68b','',0,'site',NULL,'craft\\fields\\Assets','{\"allowSelfRelations\":false,\"allowUploads\":true,\"allowedKinds\":null,\"defaultUploadLocationSource\":\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\",\"defaultUploadLocationSubpath\":\"\",\"limit\":\"1\",\"localizeRelations\":false,\"previewMode\":\"full\",\"restrictFiles\":\"\",\"selectionLabel\":\"\",\"showSiteMenu\":true,\"showUnpermittedFiles\":false,\"showUnpermittedVolumes\":false,\"singleUploadLocationSource\":\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\",\"singleUploadLocationSubpath\":\"\",\"source\":null,\"sources\":[\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"],\"targetSiteId\":null,\"useSingleFolder\":false,\"validateRelatedElements\":false,\"viewMode\":\"list\"}','2021-05-21 13:41:00','2021-05-21 13:41:00','f142b145-8349-4a9f-b9d5-6b1b815b6bb7'),(23,NULL,'Drites Bild','thirdImage','matrixBlockType:24221623-0275-4d62-a202-d46fe0c06d09','',0,'site',NULL,'craft\\fields\\Assets','{\"allowSelfRelations\":false,\"allowUploads\":true,\"allowedKinds\":null,\"defaultUploadLocationSource\":\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\",\"defaultUploadLocationSubpath\":\"\",\"limit\":\"1\",\"localizeRelations\":false,\"previewMode\":\"full\",\"restrictFiles\":\"\",\"selectionLabel\":\"\",\"showSiteMenu\":true,\"showUnpermittedFiles\":false,\"showUnpermittedVolumes\":false,\"singleUploadLocationSource\":\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\",\"singleUploadLocationSubpath\":\"\",\"source\":null,\"sources\":[\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"],\"targetSiteId\":null,\"useSingleFolder\":false,\"validateRelatedElements\":false,\"viewMode\":\"list\"}','2021-05-21 13:41:00','2021-05-21 13:41:00','4b102177-d991-4a25-89dd-c901bfb85531'),(24,NULL,'Navigation Bild','navigationImage','matrixBlockType:24221623-0275-4d62-a202-d46fe0c06d09','',0,'site',NULL,'craft\\fields\\Assets','{\"allowSelfRelations\":false,\"allowUploads\":true,\"allowedKinds\":null,\"defaultUploadLocationSource\":\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\",\"defaultUploadLocationSubpath\":\"\",\"limit\":\"1\",\"localizeRelations\":false,\"previewMode\":\"full\",\"restrictFiles\":\"\",\"selectionLabel\":\"\",\"showSiteMenu\":false,\"showUnpermittedFiles\":false,\"showUnpermittedVolumes\":false,\"singleUploadLocationSource\":\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\",\"singleUploadLocationSubpath\":\"\",\"source\":null,\"sources\":[\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"],\"targetSiteId\":null,\"useSingleFolder\":false,\"validateRelatedElements\":false,\"viewMode\":\"list\"}','2021-05-21 13:41:00','2021-05-21 13:41:00','5fcf02d4-ca79-4845-b989-9b3206a156e1'),(25,NULL,'Erstes Bild','firstImage','matrixBlockType:24221623-0275-4d62-a202-d46fe0c06d09','',0,'site',NULL,'craft\\fields\\Assets','{\"allowSelfRelations\":false,\"allowUploads\":true,\"allowedKinds\":null,\"defaultUploadLocationSource\":\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\",\"defaultUploadLocationSubpath\":\"\",\"limit\":\"1\",\"localizeRelations\":false,\"previewMode\":\"full\",\"restrictFiles\":\"\",\"selectionLabel\":\"\",\"showSiteMenu\":true,\"showUnpermittedFiles\":false,\"showUnpermittedVolumes\":false,\"singleUploadLocationSource\":\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\",\"singleUploadLocationSubpath\":\"\",\"source\":null,\"sources\":[\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"],\"targetSiteId\":null,\"useSingleFolder\":false,\"validateRelatedElements\":false,\"viewMode\":\"list\"}','2021-05-21 13:41:00','2021-05-21 13:41:00','94ac48cd-56f4-4a1e-b253-642e578a7ff8'),(26,NULL,'Zweites Bild','secondImage','matrixBlockType:24221623-0275-4d62-a202-d46fe0c06d09','',0,'site',NULL,'craft\\fields\\Assets','{\"allowSelfRelations\":false,\"allowUploads\":true,\"allowedKinds\":null,\"defaultUploadLocationSource\":\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\",\"defaultUploadLocationSubpath\":\"\",\"limit\":\"1\",\"localizeRelations\":false,\"previewMode\":\"full\",\"restrictFiles\":\"\",\"selectionLabel\":\"\",\"showSiteMenu\":true,\"showUnpermittedFiles\":false,\"showUnpermittedVolumes\":false,\"singleUploadLocationSource\":\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\",\"singleUploadLocationSubpath\":\"\",\"source\":null,\"sources\":\"*\",\"targetSiteId\":null,\"useSingleFolder\":false,\"validateRelatedElements\":false,\"viewMode\":\"list\"}','2021-05-21 13:41:00','2021-05-21 13:41:00','d0f093c3-ec7a-485f-aa72-47422fbb714a'),(27,NULL,'Kleiner Titel','smallTitle','matrixBlockType:24221623-0275-4d62-a202-d46fe0c06d09','',0,'none',NULL,'craft\\fields\\PlainText','{\"byteLimit\":null,\"charLimit\":100,\"code\":\"\",\"columnType\":null,\"initialRows\":\"4\",\"multiline\":\"\",\"placeholder\":\"\",\"uiMode\":\"normal\"}','2021-05-21 13:41:00','2021-05-21 13:41:00','db31c084-1776-4689-80ab-a9d0f63570ac'),(28,NULL,'Grosser Titel','biggTitle','matrixBlockType:24221623-0275-4d62-a202-d46fe0c06d09','',0,'none',NULL,'craft\\redactor\\Field','{\"availableTransforms\":\"*\",\"availableVolumes\":\"*\",\"cleanupHtml\":true,\"columnType\":\"text\",\"configSelectionMode\":\"choose\",\"defaultTransform\":\"\",\"manualConfig\":\"\",\"purifierConfig\":\"\",\"purifyHtml\":\"1\",\"redactorConfig\":\"\",\"removeEmptyTags\":\"1\",\"removeInlineStyles\":\"1\",\"removeNbsp\":\"1\",\"showHtmlButtonForNonAdmins\":\"\",\"showUnpermittedFiles\":false,\"showUnpermittedVolumes\":false,\"uiMode\":\"enlarged\"}','2021-05-21 13:41:00','2021-05-21 13:41:00','e8093e48-1a6c-41ad-a551-ea40c773ad42'),(29,NULL,'Block Position','rightLeft','matrixBlockType:5676be3e-cf5d-4812-b000-f654fc171776','',0,'none',NULL,'craft\\fields\\Lightswitch','{\"default\":false,\"offLabel\":\"links\",\"onLabel\":\"rechts\"}','2021-05-21 13:41:00','2021-05-21 13:41:00','4be8fe22-9a7d-4562-bd37-a6c2f75e4fc8'),(30,NULL,'Navigation Bild','navigationImage','matrixBlockType:5676be3e-cf5d-4812-b000-f654fc171776','',0,'site',NULL,'craft\\fields\\Assets','{\"allowSelfRelations\":false,\"allowUploads\":true,\"allowedKinds\":null,\"defaultUploadLocationSource\":\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\",\"defaultUploadLocationSubpath\":\"\",\"limit\":\"1\",\"localizeRelations\":false,\"previewMode\":\"full\",\"restrictFiles\":\"\",\"selectionLabel\":\"\",\"showSiteMenu\":true,\"showUnpermittedFiles\":false,\"showUnpermittedVolumes\":false,\"singleUploadLocationSource\":\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\",\"singleUploadLocationSubpath\":\"\",\"source\":null,\"sources\":[\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"],\"targetSiteId\":null,\"useSingleFolder\":false,\"validateRelatedElements\":false,\"viewMode\":\"list\"}','2021-05-21 13:41:00','2021-05-21 13:41:00','651a4895-d59b-4cef-9d66-fb958fafdcd1'),(31,NULL,'Titel','titel','matrixBlockType:5676be3e-cf5d-4812-b000-f654fc171776','',0,'none',NULL,'craft\\redactor\\Field','{\"availableTransforms\":\"*\",\"availableVolumes\":\"*\",\"cleanupHtml\":true,\"columnType\":\"text\",\"configSelectionMode\":\"choose\",\"defaultTransform\":\"\",\"manualConfig\":\"\",\"purifierConfig\":\"\",\"purifyHtml\":\"1\",\"redactorConfig\":\"\",\"removeEmptyTags\":\"1\",\"removeInlineStyles\":\"1\",\"removeNbsp\":\"1\",\"showHtmlButtonForNonAdmins\":\"\",\"showUnpermittedFiles\":false,\"showUnpermittedVolumes\":false,\"uiMode\":\"enlarged\"}','2021-05-21 13:41:00','2021-05-21 13:41:00','99d95261-96b1-4f80-b7f3-48b820ff449f'),(32,NULL,'Bild','image','matrixBlockType:5676be3e-cf5d-4812-b000-f654fc171776','',0,'site',NULL,'craft\\fields\\Assets','{\"allowSelfRelations\":false,\"allowUploads\":true,\"allowedKinds\":null,\"defaultUploadLocationSource\":\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\",\"defaultUploadLocationSubpath\":\"\",\"limit\":\"1\",\"localizeRelations\":false,\"previewMode\":\"full\",\"restrictFiles\":\"\",\"selectionLabel\":\"\",\"showSiteMenu\":true,\"showUnpermittedFiles\":false,\"showUnpermittedVolumes\":false,\"singleUploadLocationSource\":\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\",\"singleUploadLocationSubpath\":\"\",\"source\":null,\"sources\":[\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"],\"targetSiteId\":null,\"useSingleFolder\":false,\"validateRelatedElements\":false,\"viewMode\":\"list\"}','2021-05-21 13:41:00','2021-05-21 13:41:00','bf2e5a22-00a1-4932-b368-d9d4bb0bae6e'),(33,NULL,'Kleiner Titel','smalltitel','matrixBlockType:5676be3e-cf5d-4812-b000-f654fc171776','',0,'none',NULL,'craft\\fields\\PlainText','{\"byteLimit\":null,\"charLimit\":100,\"code\":\"\",\"columnType\":null,\"initialRows\":\"4\",\"multiline\":\"\",\"placeholder\":\"\",\"uiMode\":\"normal\"}','2021-05-21 13:41:00','2021-05-21 13:41:00','cf74e9d8-ee9a-478e-b286-029335d149cf'),(34,NULL,'Text','text','matrixBlockType:5676be3e-cf5d-4812-b000-f654fc171776','',0,'none',NULL,'craft\\redactor\\Field','{\"availableTransforms\":\"*\",\"availableVolumes\":\"*\",\"cleanupHtml\":true,\"columnType\":\"text\",\"configSelectionMode\":\"choose\",\"defaultTransform\":\"\",\"manualConfig\":\"\",\"purifierConfig\":\"\",\"purifyHtml\":\"1\",\"redactorConfig\":\"Simple.json\",\"removeEmptyTags\":\"1\",\"removeInlineStyles\":\"1\",\"removeNbsp\":\"1\",\"showHtmlButtonForNonAdmins\":\"\",\"showUnpermittedFiles\":false,\"showUnpermittedVolumes\":false,\"uiMode\":\"enlarged\"}','2021-05-21 13:41:00','2021-05-21 13:41:00','f111dd8f-f1ee-4aa9-97e2-a1d0941b9dc4'),(35,NULL,'Titel','titel','matrixBlockType:05fdf65f-1c94-44d5-bfc3-ec3b4181ccef','',0,'none',NULL,'craft\\redactor\\Field','{\"availableTransforms\":\"*\",\"availableVolumes\":\"*\",\"cleanupHtml\":true,\"columnType\":\"text\",\"configSelectionMode\":\"choose\",\"defaultTransform\":\"\",\"manualConfig\":\"\",\"purifierConfig\":\"\",\"purifyHtml\":\"1\",\"redactorConfig\":\"\",\"removeEmptyTags\":\"1\",\"removeInlineStyles\":\"1\",\"removeNbsp\":\"1\",\"showHtmlButtonForNonAdmins\":\"\",\"showUnpermittedFiles\":false,\"showUnpermittedVolumes\":false,\"uiMode\":\"enlarged\"}','2021-05-21 13:41:01','2021-05-21 13:41:01','433925d5-4dc7-4a35-8a80-42573edd3c1e'),(36,NULL,'Navigation Bild','navigationImage','matrixBlockType:05fdf65f-1c94-44d5-bfc3-ec3b4181ccef','',0,'site',NULL,'craft\\fields\\Assets','{\"allowSelfRelations\":false,\"allowUploads\":true,\"allowedKinds\":null,\"defaultUploadLocationSource\":\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\",\"defaultUploadLocationSubpath\":\"\",\"limit\":\"1\",\"localizeRelations\":false,\"previewMode\":\"full\",\"restrictFiles\":\"\",\"selectionLabel\":\"\",\"showSiteMenu\":true,\"showUnpermittedFiles\":false,\"showUnpermittedVolumes\":false,\"singleUploadLocationSource\":\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\",\"singleUploadLocationSubpath\":\"\",\"source\":null,\"sources\":[\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"],\"targetSiteId\":null,\"useSingleFolder\":false,\"validateRelatedElements\":false,\"viewMode\":\"list\"}','2021-05-21 13:41:01','2021-05-21 13:41:01','c27a80ef-83b3-4da1-a3b4-170cb07b16ef'),(37,NULL,'Kleiner Titel','smalltitel','matrixBlockType:05fdf65f-1c94-44d5-bfc3-ec3b4181ccef','',0,'none',NULL,'craft\\fields\\PlainText','{\"byteLimit\":null,\"charLimit\":100,\"code\":\"\",\"columnType\":null,\"initialRows\":\"4\",\"multiline\":\"\",\"placeholder\":\"\",\"uiMode\":\"normal\"}','2021-05-21 13:41:01','2021-05-21 13:41:01','e9bdc3b6-4eb6-4043-8b52-813ac45b5aca');
/*!40000 ALTER TABLE `fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `globalsets`
--

DROP TABLE IF EXISTS `globalsets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `globalsets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `globalsets_name_idx` (`name`),
  KEY `globalsets_handle_idx` (`handle`),
  KEY `globalsets_fieldLayoutId_idx` (`fieldLayoutId`),
  CONSTRAINT `globalsets_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `globalsets_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `globalsets`
--

LOCK TABLES `globalsets` WRITE;
/*!40000 ALTER TABLE `globalsets` DISABLE KEYS */;
INSERT INTO `globalsets` VALUES (2,'Kontakt Daten','kontaktDaten',5,'2021-05-21 13:41:00','2021-05-21 13:41:00','24aee758-3b1e-4075-8529-4677b30ce2c7'),(3,'Header','header',6,'2021-05-21 13:41:00','2021-05-21 13:41:00','760dd7bf-1d0f-4f6f-addf-6b3f732ad96b');
/*!40000 ALTER TABLE `globalsets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gqlschemas`
--

DROP TABLE IF EXISTS `gqlschemas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gqlschemas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `scope` text,
  `isPublic` tinyint(1) NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gqlschemas`
--

LOCK TABLES `gqlschemas` WRITE;
/*!40000 ALTER TABLE `gqlschemas` DISABLE KEYS */;
/*!40000 ALTER TABLE `gqlschemas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gqltokens`
--

DROP TABLE IF EXISTS `gqltokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gqltokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `accessToken` varchar(255) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `expiryDate` datetime DEFAULT NULL,
  `lastUsed` datetime DEFAULT NULL,
  `schemaId` int(11) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `gqltokens_accessToken_unq_idx` (`accessToken`),
  UNIQUE KEY `gqltokens_name_unq_idx` (`name`),
  KEY `gqltokens_schemaId_fk` (`schemaId`),
  CONSTRAINT `gqltokens_schemaId_fk` FOREIGN KEY (`schemaId`) REFERENCES `gqlschemas` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gqltokens`
--

LOCK TABLES `gqltokens` WRITE;
/*!40000 ALTER TABLE `gqltokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `gqltokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `info`
--

DROP TABLE IF EXISTS `info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` varchar(50) NOT NULL,
  `schemaVersion` varchar(15) NOT NULL,
  `maintenance` tinyint(1) NOT NULL DEFAULT '0',
  `configVersion` char(12) NOT NULL DEFAULT '000000000000',
  `fieldVersion` char(12) NOT NULL DEFAULT '000000000000',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `info`
--

LOCK TABLES `info` WRITE;
/*!40000 ALTER TABLE `info` DISABLE KEYS */;
INSERT INTO `info` VALUES (1,'3.5.18','3.5.13',0,'yopdbkvnarhe','hwphkrzpcbda','2021-01-07 10:01:15','2021-05-21 13:41:02','6f788c2d-2b9a-4dca-abd0-e5b06396b394');
/*!40000 ALTER TABLE `info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `matrixblocks`
--

DROP TABLE IF EXISTS `matrixblocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matrixblocks` (
  `id` int(11) NOT NULL,
  `ownerId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `typeId` int(11) NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `deletedWithOwner` tinyint(1) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `matrixblocks_ownerId_idx` (`ownerId`),
  KEY `matrixblocks_fieldId_idx` (`fieldId`),
  KEY `matrixblocks_typeId_idx` (`typeId`),
  KEY `matrixblocks_sortOrder_idx` (`sortOrder`),
  CONSTRAINT `matrixblocks_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixblocks_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixblocks_ownerId_fk` FOREIGN KEY (`ownerId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixblocks_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `matrixblocktypes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `matrixblocks`
--

LOCK TABLES `matrixblocks` WRITE;
/*!40000 ALTER TABLE `matrixblocks` DISABLE KEYS */;
/*!40000 ALTER TABLE `matrixblocks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `matrixblocktypes`
--

DROP TABLE IF EXISTS `matrixblocktypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matrixblocktypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `matrixblocktypes_name_fieldId_idx` (`name`,`fieldId`),
  KEY `matrixblocktypes_handle_fieldId_idx` (`handle`,`fieldId`),
  KEY `matrixblocktypes_fieldId_idx` (`fieldId`),
  KEY `matrixblocktypes_fieldLayoutId_idx` (`fieldLayoutId`),
  CONSTRAINT `matrixblocktypes_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixblocktypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `matrixblocktypes`
--

LOCK TABLES `matrixblocktypes` WRITE;
/*!40000 ALTER TABLE `matrixblocktypes` DISABLE KEYS */;
INSERT INTO `matrixblocktypes` VALUES (1,1,1,'Adress','address',1,'2021-05-21 13:41:00','2021-05-21 13:41:00','5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88'),(2,5,2,'News Block','newsBlock',3,'2021-05-21 13:41:00','2021-05-21 13:41:00','3941620b-2988-47eb-8e03-c0d5596d2850'),(3,5,3,'Formular Block','formularBlock',4,'2021-05-21 13:41:00','2021-05-21 13:41:00','34079b35-1790-43d7-969a-07e64284d68b'),(4,5,4,'SliderBlock','sliderBlock',5,'2021-05-21 13:41:00','2021-05-21 13:41:00','24221623-0275-4d62-a202-d46fe0c06d09'),(5,5,7,'Titel, Text, Bild Block','textImageTitleBlock',1,'2021-05-21 13:41:00','2021-05-21 13:41:00','5676be3e-cf5d-4812-b000-f654fc171776'),(6,5,10,'TitelBlock','titelBlock',2,'2021-05-21 13:41:01','2021-05-21 13:41:01','05fdf65f-1c94-44d5-bfc3-ec3b4181ccef');
/*!40000 ALTER TABLE `matrixblocktypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `matrixcontent_adresse`
--

DROP TABLE IF EXISTS `matrixcontent_adresse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matrixcontent_adresse` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  `field_address_plz` text,
  `field_address_phone` text,
  `field_address_mail` text,
  `field_address_street` text,
  `field_address_city` text,
  `field_address_company` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `matrixcontent_adresse_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  KEY `matrixcontent_adresse_siteId_fk` (`siteId`),
  CONSTRAINT `matrixcontent_adresse_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixcontent_adresse_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `matrixcontent_adresse`
--

LOCK TABLES `matrixcontent_adresse` WRITE;
/*!40000 ALTER TABLE `matrixcontent_adresse` DISABLE KEYS */;
/*!40000 ALTER TABLE `matrixcontent_adresse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `matrixcontent_pageentrys`
--

DROP TABLE IF EXISTS `matrixcontent_pageentrys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matrixcontent_pageentrys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  `field_newsBlock_text` text,
  `field_newsBlock_titel` varchar(400) DEFAULT NULL,
  `field_formularBlock_smalltitel` varchar(400) DEFAULT NULL,
  `field_formularBlock_biggTitle` text,
  `field_sliderBlock_smallTitle` varchar(400) DEFAULT NULL,
  `field_sliderBlock_biggTitle` text,
  `field_textImageTitleBlock_rightLeft` tinyint(1) DEFAULT NULL,
  `field_textImageTitleBlock_titel` text,
  `field_textImageTitleBlock_smalltitel` varchar(400) DEFAULT NULL,
  `field_textImageTitleBlock_text` text,
  `field_titelBlock_titel` text,
  `field_titelBlock_smalltitel` varchar(400) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `matrixcontent_pageentrys_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  KEY `matrixcontent_pageentrys_siteId_fk` (`siteId`),
  CONSTRAINT `matrixcontent_pageentrys_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `matrixcontent_pageentrys_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `matrixcontent_pageentrys`
--

LOCK TABLES `matrixcontent_pageentrys` WRITE;
/*!40000 ALTER TABLE `matrixcontent_pageentrys` DISABLE KEYS */;
/*!40000 ALTER TABLE `matrixcontent_pageentrys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `track` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applyTime` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `migrations_track_name_unq_idx` (`track`,`name`)
) ENGINE=InnoDB AUTO_INCREMENT=185 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'craft','Install','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','907ec4fa-4445-48eb-9498-2719b3c8caf2'),(2,'craft','m150403_183908_migrations_table_changes','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','a86d55a5-09d6-4b8f-bd0a-8ae8296b9705'),(3,'craft','m150403_184247_plugins_table_changes','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','332b01ea-e177-44d9-ae21-2233a55fa314'),(4,'craft','m150403_184533_field_version','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','84f31940-f096-4da0-a1c1-02cdedd97d9b'),(5,'craft','m150403_184729_type_columns','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','d6c2205c-afde-478d-8fb5-c32905fb4fba'),(6,'craft','m150403_185142_volumes','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','5f9144b6-8739-4a72-9fa8-93d19a4f2e9a'),(7,'craft','m150428_231346_userpreferences','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','00ef381c-96b1-44c1-89be-c8c15ef048ed'),(8,'craft','m150519_150900_fieldversion_conversion','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','e7fab924-38bd-4c58-846f-22fce12fd8a7'),(9,'craft','m150617_213829_update_email_settings','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','53035d58-7c15-4f3a-8cc9-a698de29e8b2'),(10,'craft','m150721_124739_templatecachequeries','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','69f4ee52-fd62-4bcc-a335-c7ec140bd9db'),(11,'craft','m150724_140822_adjust_quality_settings','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','d8488fc3-96d8-48a4-8f36-0bf94e1f087b'),(12,'craft','m150815_133521_last_login_attempt_ip','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','a7ae7358-dcb9-4be1-92e1-29bbed8bd056'),(13,'craft','m151002_095935_volume_cache_settings','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','f82916e3-7c83-4f80-abdc-bb94850e3cf9'),(14,'craft','m151005_142750_volume_s3_storage_settings','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','77673031-c584-4000-ae0e-e859cdb47a05'),(15,'craft','m151016_133600_delete_asset_thumbnails','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','015b9420-2ac2-4d0f-928f-938dd77e8a8f'),(16,'craft','m151209_000000_move_logo','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','71355ecf-f9fd-48cd-a568-ee2eb1c68220'),(17,'craft','m151211_000000_rename_fileId_to_assetId','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','7c425a17-7d42-4452-8923-f29786bd01bc'),(18,'craft','m151215_000000_rename_asset_permissions','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','f3ad0670-ba54-4923-bb87-73fbca5c1a8f'),(19,'craft','m160707_000001_rename_richtext_assetsource_setting','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','e4353dbf-d43d-4fb3-9130-5ca577dd2203'),(20,'craft','m160708_185142_volume_hasUrls_setting','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','6cf18c0a-ae86-4112-a9d3-f7e3c61b05ae'),(21,'craft','m160714_000000_increase_max_asset_filesize','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','b87b16b6-57ff-4902-9421-4d68fb15b438'),(22,'craft','m160727_194637_column_cleanup','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','b7a75563-e184-4ae4-8d37-ba31c8dc7858'),(23,'craft','m160804_110002_userphotos_to_assets','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','1a6a457c-3366-46ba-a1b8-9fb8166fa1b8'),(24,'craft','m160807_144858_sites','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','c09331f1-d36b-4970-950a-ec142f5c364b'),(25,'craft','m160829_000000_pending_user_content_cleanup','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','54ef35e7-383f-4010-aa92-048389adf802'),(26,'craft','m160830_000000_asset_index_uri_increase','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','f310799d-a811-42b0-b6f7-fc462d8adb21'),(27,'craft','m160912_230520_require_entry_type_id','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','7d0258d3-b5ed-4fe3-8a6c-ea487943dce3'),(28,'craft','m160913_134730_require_matrix_block_type_id','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','d83b602a-0d4f-4630-a1f3-de5e9037aadc'),(29,'craft','m160920_174553_matrixblocks_owner_site_id_nullable','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','e8d185b0-8cca-4c3e-af12-58d26817db4b'),(30,'craft','m160920_231045_usergroup_handle_title_unique','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','412f3f4a-057a-4347-bdab-1d017b789a00'),(31,'craft','m160925_113941_route_uri_parts','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','fa8ec2c2-0c4b-4a2c-9e45-296ba3cbc710'),(32,'craft','m161006_205918_schemaVersion_not_null','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','b992c34c-98a9-4ccf-9ad1-09c28e1d4f35'),(33,'craft','m161007_130653_update_email_settings','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','582e479e-fc27-4d3c-89be-6e039f19264d'),(34,'craft','m161013_175052_newParentId','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','d8967efa-e9d5-4559-80f5-0b3fcb009374'),(35,'craft','m161021_102916_fix_recent_entries_widgets','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','5f021cc1-4b84-4b6c-ba07-5ece9509219f'),(36,'craft','m161021_182140_rename_get_help_widget','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','fc08e5b3-a703-4f7d-9b0f-e5bd646f138e'),(37,'craft','m161025_000000_fix_char_columns','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','8cef459b-b5e3-4d2b-ba41-2fab01831e43'),(38,'craft','m161029_124145_email_message_languages','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','3609dbb8-454c-4f90-ac0b-01e84a733423'),(39,'craft','m161108_000000_new_version_format','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','ed1e80a7-3302-4f61-af0b-3c0d6091eeca'),(40,'craft','m161109_000000_index_shuffle','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','078aeeac-35fb-42eb-b76c-3ddd90098dd4'),(41,'craft','m161122_185500_no_craft_app','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','281483e2-1ed8-4fcc-8472-7bbf7752ae36'),(42,'craft','m161125_150752_clear_urlmanager_cache','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','4d0c13fa-4b2d-4cb9-9d6e-8a6e4b9fcbfa'),(43,'craft','m161220_000000_volumes_hasurl_notnull','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','22efea4a-1971-41d7-bd07-a79cf456be29'),(44,'craft','m170114_161144_udates_permission','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','fe7d9244-5f86-4a60-a9d5-a0107721d44b'),(45,'craft','m170120_000000_schema_cleanup','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','8990e932-6c66-45e6-8097-0e870d463360'),(46,'craft','m170126_000000_assets_focal_point','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','dccaa4ff-912b-4005-ad5c-63fc6f240bae'),(47,'craft','m170206_142126_system_name','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','a0c73b90-1c31-4da5-88e8-bdfb41c93b62'),(48,'craft','m170217_044740_category_branch_limits','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','eb733716-e886-4376-b5f8-35c54f8f1c7f'),(49,'craft','m170217_120224_asset_indexing_columns','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','3d018522-f69f-4b30-80b8-b367f025e9b4'),(50,'craft','m170223_224012_plain_text_settings','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','e7dfdc78-1fd5-43fd-a446-342a755ca10e'),(51,'craft','m170227_120814_focal_point_percentage','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','5e8e7030-505a-4e56-8866-e04f96bfd2a2'),(52,'craft','m170228_171113_system_messages','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','fd3bce10-5505-4474-a29a-b24fe1438cb6'),(53,'craft','m170303_140500_asset_field_source_settings','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','a94ff578-5f0d-4d4e-95f4-de54f0c455d6'),(54,'craft','m170306_150500_asset_temporary_uploads','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','dca83fc9-349a-4559-b582-624eba21ab87'),(55,'craft','m170523_190652_element_field_layout_ids','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','23029c7f-c24b-4a6c-8f94-ad35c44a353c'),(56,'craft','m170621_195237_format_plugin_handles','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','bcb853cf-222d-4369-829a-8971bea1f092'),(57,'craft','m170630_161027_deprecation_line_nullable','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','2b06104b-4817-4f3f-9ffb-0a1f8aea76dd'),(58,'craft','m170630_161028_deprecation_changes','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','bf76b17d-65da-4b09-882c-b27dacc3fdbb'),(59,'craft','m170703_181539_plugins_table_tweaks','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','b45c52d4-738c-4e76-97bd-6763564f5c9f'),(60,'craft','m170704_134916_sites_tables','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','27f969e1-8be7-4174-b3eb-a9946c304fba'),(61,'craft','m170706_183216_rename_sequences','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','b60ec078-5187-448d-b5a5-d465f6b2ab14'),(62,'craft','m170707_094758_delete_compiled_traits','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','9ab06f53-98f6-4e48-b115-74eb5ceb3c6f'),(63,'craft','m170731_190138_drop_asset_packagist','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','727e019e-adff-431f-a4b3-9f7f578a2022'),(64,'craft','m170810_201318_create_queue_table','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','30acfc79-fca4-476c-9b4a-f9957ad730f1'),(65,'craft','m170903_192801_longblob_for_queue_jobs','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','570b7b5a-a61c-4556-98ac-7d45ac002955'),(66,'craft','m170914_204621_asset_cache_shuffle','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','d0104960-fd44-4373-b02b-b450268c22ab'),(67,'craft','m171011_214115_site_groups','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','e3e8623d-0213-4d81-925f-4957fe51faf6'),(68,'craft','m171012_151440_primary_site','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','fe3e4599-31d3-4f8c-8da9-7eaae6008ff3'),(69,'craft','m171013_142500_transform_interlace','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','3060dd3b-b461-4ff4-98ec-c9b260e57dbb'),(70,'craft','m171016_092553_drop_position_select','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','c9b8b318-fe58-4829-a542-b9cab6f89e2a'),(71,'craft','m171016_221244_less_strict_translation_method','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','3f3a7c57-6f16-4477-be84-4ad37f061771'),(72,'craft','m171107_000000_assign_group_permissions','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','3325fbfb-5c0c-4e36-9c33-3df4060d7f10'),(73,'craft','m171117_000001_templatecache_index_tune','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','9a6b4e5f-1963-4b03-8f62-06120e933fdb'),(74,'craft','m171126_105927_disabled_plugins','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','357b34c9-0a32-4b52-840a-73c12ccc90b9'),(75,'craft','m171130_214407_craftidtokens_table','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','6240d94d-fa66-4a1c-9e10-6a30de7990c7'),(76,'craft','m171202_004225_update_email_settings','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','c835bb59-c031-49df-9b32-729454a9dc6b'),(77,'craft','m171204_000001_templatecache_index_tune_deux','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','8665c162-b26a-4920-9449-3d8d7d982111'),(78,'craft','m171205_130908_remove_craftidtokens_refreshtoken_column','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','af787f3d-8924-430f-8525-19f282c56eb0'),(79,'craft','m171218_143135_longtext_query_column','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','57c2723c-bb8c-4ba5-8f6f-8cd1611784aa'),(80,'craft','m171231_055546_environment_variables_to_aliases','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','5a0c0be4-649c-4e27-94db-9e8cf3319057'),(81,'craft','m180113_153740_drop_users_archived_column','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','f960bec6-3977-462d-872b-659cad3c4417'),(82,'craft','m180122_213433_propagate_entries_setting','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','16274f2c-45f6-4ea2-9709-7516cbb50d9b'),(83,'craft','m180124_230459_fix_propagate_entries_values','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','b7a9f565-3cb3-4acd-8c7c-89eb00ee4dad'),(84,'craft','m180128_235202_set_tag_slugs','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','dfffa255-405b-4b5a-9dd5-2bd930b7e2f1'),(85,'craft','m180202_185551_fix_focal_points','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','0b3e2394-0665-41d7-9da8-d4d073759f32'),(86,'craft','m180217_172123_tiny_ints','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','046843bd-7b3b-4d3a-aa79-72fcbbff86fe'),(87,'craft','m180321_233505_small_ints','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','8b147276-da44-4550-a51b-7f9d7c0bb5a6'),(88,'craft','m180328_115523_new_license_key_statuses','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','9d2625d6-10b4-4537-ba5b-df6dc82c4411'),(89,'craft','m180404_182320_edition_changes','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','07aedc29-dbf9-45db-b581-d996a74d65ca'),(90,'craft','m180411_102218_fix_db_routes','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','285783bb-3c7c-40b3-9767-1c8ad12a33ac'),(91,'craft','m180416_205628_resourcepaths_table','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','81b109ae-6237-4c8a-8b3d-e9074cd82578'),(92,'craft','m180418_205713_widget_cleanup','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','b54ef90a-00dc-4794-a805-9305339864a2'),(93,'craft','m180425_203349_searchable_fields','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','23fde155-44d7-4617-bc50-6e9933245c19'),(94,'craft','m180516_153000_uids_in_field_settings','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','3ed1dc13-9b98-4702-b023-43b648b294be'),(95,'craft','m180517_173000_user_photo_volume_to_uid','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','d18882fd-0ad8-4223-9b20-11df01bf72c6'),(96,'craft','m180518_173000_permissions_to_uid','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','73d53d9b-585b-4e74-af8b-d068553b4e3a'),(97,'craft','m180520_173000_matrix_context_to_uids','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','c4b2a64e-9805-4c31-bb8b-5fd87d55d9e5'),(98,'craft','m180521_172900_project_config_table','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','0120f93d-f6c1-49e7-bb2b-9c6cebafd29e'),(99,'craft','m180521_173000_initial_yml_and_snapshot','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','c8b699aa-18b7-48f2-84d5-b89e09346bdd'),(100,'craft','m180731_162030_soft_delete_sites','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','426d9592-2a84-41d7-a210-7ddb93fafaba'),(101,'craft','m180810_214427_soft_delete_field_layouts','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','4e6d0a45-98c2-497a-819b-113d03d66cc2'),(102,'craft','m180810_214439_soft_delete_elements','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','35c3188c-f333-4550-9448-97d547dd8094'),(103,'craft','m180824_193422_case_sensitivity_fixes','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','3f9afbff-66c7-4831-8417-1ccf966336ae'),(104,'craft','m180901_151639_fix_matrixcontent_tables','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','6ad5c3fe-c256-4f69-a789-0abcff487981'),(105,'craft','m180904_112109_permission_changes','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','1e826ecb-b60a-4db1-a172-9637123ddf49'),(106,'craft','m180910_142030_soft_delete_sitegroups','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','68581f3d-8c5d-47f6-ac41-eca72885f50c'),(107,'craft','m181011_160000_soft_delete_asset_support','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','9f10b1e6-ab4e-49ce-beed-01899ae96112'),(108,'craft','m181016_183648_set_default_user_settings','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','cdd5c7bb-0558-4866-b0f1-c84871fe7123'),(109,'craft','m181017_225222_system_config_settings','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','7b0d42fc-2149-4bf1-88c1-95a24c785055'),(110,'craft','m181018_222343_drop_userpermissions_from_config','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','2375f3c1-71e6-4355-823a-c00c8019ae72'),(111,'craft','m181029_130000_add_transforms_routes_to_config','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','0a4da50a-461f-41d9-beb2-bf7dde34d4e0'),(112,'craft','m181112_203955_sequences_table','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','3314da84-71b1-4338-9f87-c324f6cc2bb8'),(113,'craft','m181121_001712_cleanup_field_configs','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','299a8294-b780-4586-84d5-7abe8f2557fe'),(114,'craft','m181128_193942_fix_project_config','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','8e49dc47-3a9c-4a19-a6f1-01db26f635ba'),(115,'craft','m181130_143040_fix_schema_version','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','621b578b-f9c7-4602-8eae-2bc44ad0abc5'),(116,'craft','m181211_143040_fix_entry_type_uids','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','78dcffc7-d960-4ba4-8a96-150ac9c901b7'),(117,'craft','m181217_153000_fix_structure_uids','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','fc9c1fe3-a18f-49e6-91f8-85e81987ca14'),(118,'craft','m190104_152725_store_licensed_plugin_editions','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','edeb12a1-f562-474b-a022-fcd7dd90f13e'),(119,'craft','m190108_110000_cleanup_project_config','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','dc750cfb-1957-40d3-a69d-ff2b88a9788c'),(120,'craft','m190108_113000_asset_field_setting_change','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','f33df196-67c2-48be-bdcd-3f52c8047d17'),(121,'craft','m190109_172845_fix_colspan','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','42d4587e-0a90-4a15-aa6e-4bc4dee31edb'),(122,'craft','m190110_150000_prune_nonexisting_sites','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','2dda5e60-296d-410a-a2b5-a4e0c925f5f6'),(123,'craft','m190110_214819_soft_delete_volumes','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','203ba3a8-d313-430e-b287-41dc410df57e'),(124,'craft','m190112_124737_fix_user_settings','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','c1de2ca8-e700-4e26-9199-217384e63fc9'),(125,'craft','m190112_131225_fix_field_layouts','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','2f363c26-c734-4d8f-bfc2-af5b17597743'),(126,'craft','m190112_201010_more_soft_deletes','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','2f394bdd-e79b-4aa5-8b2e-14ab87a62c99'),(127,'craft','m190114_143000_more_asset_field_setting_changes','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','32342a62-0bc9-4ff8-a1c9-27c5a53044b3'),(128,'craft','m190121_120000_rich_text_config_setting','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','e8418d26-dc39-43fb-ae7f-a92c7ccc8c35'),(129,'craft','m190125_191628_fix_email_transport_password','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','24815b01-fd17-4e3c-aa7c-28717aae52b9'),(130,'craft','m190128_181422_cleanup_volume_folders','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','3aecfda6-9ee5-49b5-b2a3-a04febef1903'),(131,'craft','m190205_140000_fix_asset_soft_delete_index','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','87d17e45-8ded-4c35-9d3a-72f584c06649'),(132,'craft','m190218_143000_element_index_settings_uid','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','c8b6577d-1d89-4cee-9aba-75ad0051df8e'),(133,'craft','m190312_152740_element_revisions','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','d5f64215-ef91-4706-a3d4-665a75630ad3'),(134,'craft','m190327_235137_propagation_method','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','248ee777-2bfc-49ef-a18e-af5c296013f2'),(135,'craft','m190401_223843_drop_old_indexes','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','3ebbc852-28b1-45b5-8318-31d0b3358d6b'),(136,'craft','m190416_014525_drop_unique_global_indexes','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','435ee6ca-d153-401d-a7ef-b282c1db701b'),(137,'craft','m190417_085010_add_image_editor_permissions','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','fad00f56-6261-4912-a94c-6a000fe3d5ef'),(138,'craft','m190502_122019_store_default_user_group_uid','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','59d0f2b5-30c0-4d55-9ae9-2ed358e3b834'),(139,'craft','m190504_150349_preview_targets','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','de2d5d99-4d2f-4f4a-af4d-ccd3275b1dea'),(140,'craft','m190516_184711_job_progress_label','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','821c3097-3c5e-4cbe-a87d-12e3baa85567'),(141,'craft','m190523_190303_optional_revision_creators','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','6ad9ef90-38e1-45f3-a257-cb60adbb1f03'),(142,'craft','m190529_204501_fix_duplicate_uids','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','ee7c854d-cc19-457b-ba72-9c0a482efa9c'),(143,'craft','m190605_223807_unsaved_drafts','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','c3694161-9198-4891-a9bd-efbe895c2f1f'),(144,'craft','m190607_230042_entry_revision_error_tables','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','c3651080-f52d-4b98-aa90-b8e625ad931e'),(145,'craft','m190608_033429_drop_elements_uid_idx','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','8749dcbe-5315-438d-8821-bd854d0a3cb4'),(146,'craft','m190617_164400_add_gqlschemas_table','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','5757cb98-1101-44dc-a6db-045fa8e4a7b4'),(147,'craft','m190624_234204_matrix_propagation_method','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','83315af5-a069-4deb-b3c5-6b3d14fc44c3'),(148,'craft','m190711_153020_drop_snapshots','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','76c8686c-f400-450e-9b5b-112ee42257f6'),(149,'craft','m190712_195914_no_draft_revisions','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','f5c0ecf9-868c-4296-9f1a-7821336d382f'),(150,'craft','m190723_140314_fix_preview_targets_column','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','7899f268-91ed-446a-9d3c-28f6b890d651'),(151,'craft','m190820_003519_flush_compiled_templates','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','649e66bb-7cde-4630-9f72-84030582ce24'),(152,'craft','m190823_020339_optional_draft_creators','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','da03d157-b7a2-4b64-bb10-5309c2b890d1'),(153,'craft','m190913_152146_update_preview_targets','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','395d0c01-01dd-49d3-86de-7a6f40833735'),(154,'craft','m191107_122000_add_gql_project_config_support','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','eac8af0a-ee51-4560-97c0-46a502b2a912'),(155,'craft','m191204_085100_pack_savable_component_settings','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','9b6d4d84-0744-43be-af10-ec42f5f2c5a6'),(156,'craft','m191206_001148_change_tracking','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','cdb71dac-ecf8-4bb8-a060-7e538d89345b'),(157,'craft','m191216_191635_asset_upload_tracking','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','5c4ec503-257e-4b26-b54c-ebfdab0a4f95'),(158,'craft','m191222_002848_peer_asset_permissions','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','81d0a653-9cf8-4aa9-9a08-b7310655af39'),(159,'craft','m200127_172522_queue_channels','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','31855cb5-66b2-412c-81ab-4aef7975e98e'),(160,'craft','m200211_175048_truncate_element_query_cache','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','d1d678d3-f1bb-4ad3-a854-4d6483eacefb'),(161,'craft','m200213_172522_new_elements_index','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','8f198d22-e424-49db-a3ae-68f42ca61626'),(162,'craft','m200228_195211_long_deprecation_messages','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','0933a40e-560b-490b-948a-1ef62e094ee4'),(163,'craft','m200306_054652_disabled_sites','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','3551c731-b751-4f5e-b1ab-b051e799f293'),(164,'craft','m200522_191453_clear_template_caches','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','0cba299e-088f-476a-81a4-b08ad9603f29'),(165,'craft','m200606_231117_migration_tracks','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','fd3229e0-956f-40c7-a7f5-aa13a5121e9b'),(166,'craft','m200619_215137_title_translation_method','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','9e3cb39e-be07-4120-ab01-c554e537c87c'),(167,'craft','m200620_005028_user_group_descriptions','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','fe249bce-7d51-4135-9495-d456d64987a7'),(168,'craft','m200620_230205_field_layout_changes','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','a3f671be-7b05-49d3-903d-be601c835dd3'),(169,'craft','m200625_131100_move_entrytypes_to_top_project_config','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','c60d95ce-fa16-4e73-9b22-f9b4118eade4'),(170,'craft','m200629_112700_remove_project_config_legacy_files','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','fabc323f-f56e-4338-b9bf-5056e2da5406'),(171,'craft','m200630_183000_drop_configmap','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','b9eab324-9b65-437d-82ac-0b6777b6d090'),(172,'craft','m200715_113400_transform_index_error_flag','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','bbb1cfbe-1be5-438d-833a-8b20adf3c3cf'),(173,'craft','m200716_110900_replace_file_asset_permissions','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','499cb9cd-0e26-4cca-bf57-6d736f13a95e'),(174,'craft','m200716_153800_public_token_settings_in_project_config','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','ed39c0f7-78b0-4c0d-bb93-611ec37106e9'),(175,'craft','m200720_175543_drop_unique_constraints','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','0ffeee8b-dd5d-4ee1-a324-180790c748e9'),(176,'craft','m200825_051217_project_config_version','2021-01-07 10:01:16','2021-01-07 10:01:16','2021-01-07 10:01:16','9fb8a319-d46e-4235-92b4-a1478ecc8a61'),(177,'plugin:redactor','m180430_204710_remove_old_plugins','2021-05-21 13:40:56','2021-05-21 13:40:56','2021-05-21 13:40:56','2e704fd7-f009-49c3-b421-5731a984a93b'),(178,'plugin:redactor','Install','2021-05-21 13:40:56','2021-05-21 13:40:56','2021-05-21 13:40:56','892d6510-00bc-4959-8bb7-777da08e3c83'),(179,'plugin:redactor','m190225_003922_split_cleanup_html_settings','2021-05-21 13:40:56','2021-05-21 13:40:56','2021-05-21 13:40:56','0c30ee8f-a589-49aa-89c1-ffa5a6f0910b'),(180,'plugin:seo','Install','2021-05-21 13:40:58','2021-05-21 13:40:58','2021-05-21 13:40:58','901ddd8e-197f-4fe3-81ac-41d403c184fe'),(181,'plugin:seo','m180906_152947_add_site_id_to_redirects','2021-05-21 13:40:58','2021-05-21 13:40:58','2021-05-21 13:40:58','823726b3-3ebb-4425-8854-84829fd7479c'),(182,'plugin:seo','m190114_152300_upgrade_to_new_data_format','2021-05-21 13:40:58','2021-05-21 13:40:58','2021-05-21 13:40:58','93b70e07-1f41-4e3e-8baa-88d5a41dff72'),(183,'plugin:seo','m200518_110721_add_order_to_redirects','2021-05-21 13:40:58','2021-05-21 13:40:58','2021-05-21 13:40:58','02bbe1cf-c803-4d36-a9a9-1f6192578d40'),(184,'plugin:seo','m201207_124200_add_product_types_to_sitemap','2021-05-21 13:40:58','2021-05-21 13:40:58','2021-05-21 13:40:58','d0e20e61-a26a-4b16-b22c-7e1804b07760');
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plugins`
--

DROP TABLE IF EXISTS `plugins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plugins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `handle` varchar(255) NOT NULL,
  `version` varchar(255) NOT NULL,
  `schemaVersion` varchar(255) NOT NULL,
  `licenseKeyStatus` enum('valid','invalid','mismatched','astray','unknown') NOT NULL DEFAULT 'unknown',
  `licensedEdition` varchar(255) DEFAULT NULL,
  `installDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `plugins_handle_unq_idx` (`handle`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plugins`
--

LOCK TABLES `plugins` WRITE;
/*!40000 ALTER TABLE `plugins` DISABLE KEYS */;
INSERT INTO `plugins` VALUES (1,'contact-form','2.2.7','1.0.0','unknown',NULL,'2021-05-21 13:40:55','2021-05-21 13:40:55','2021-05-21 13:40:55','6c55741d-e9ad-4683-9b21-fba3e1f9d4ac'),(2,'obfuscator','1.0.4','1.0.0','unknown',NULL,'2021-05-21 13:40:56','2021-05-21 13:40:56','2021-05-21 13:40:56','d341dc62-3b52-43e6-aaed-33595c324bbd'),(3,'redactor','2.8.7','2.3.0','unknown',NULL,'2021-05-21 13:40:56','2021-05-21 13:40:56','2021-05-21 13:40:56','683c074c-1619-4cf8-a534-9554940dc74b'),(4,'redactor-custom-styles','3.0.4','1.0.0','unknown',NULL,'2021-05-21 13:40:57','2021-05-21 13:40:57','2021-05-21 13:40:57','c396e5cb-c9a7-4bec-af8b-3b629bac0a99'),(5,'seo','3.7.1','3.2.0','unknown',NULL,'2021-05-21 13:40:58','2021-05-21 13:40:58','2021-05-21 13:40:58','662c59e1-c18a-42ed-b1ac-3efb2e99a4f1');
/*!40000 ALTER TABLE `plugins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projectconfig`
--

DROP TABLE IF EXISTS `projectconfig`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projectconfig` (
  `path` varchar(255) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projectconfig`
--

LOCK TABLES `projectconfig` WRITE;
/*!40000 ALTER TABLE `projectconfig` DISABLE KEYS */;
INSERT INTO `projectconfig` VALUES ('dateModified','1621601830'),('email.fromEmail','\"info@youInvestment.ch\"'),('email.fromName','\"youInvestment\"'),('email.replyToEmail','null'),('email.template','null'),('email.transportSettings.encryptionMethod','\"ssl\"'),('email.transportSettings.host','\"smtp.eu.mailgun.org\"'),('email.transportSettings.password','\"Bw{fmzVENCVN;D2FLPw6\"'),('email.transportSettings.port','\"465\"'),('email.transportSettings.timeout','\"10\"'),('email.transportSettings.useAuthentication','\"1\"'),('email.transportSettings.username','\"yoen-website@mailgun.youengineering.com\"'),('email.transportType','\"craft\\\\mail\\\\transportadapters\\\\Smtp\"'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.fieldLayouts.c4ae2dd6-599c-4e06-a4b3-d48464040538.tabs.0.elements.0.autocapitalize','true'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.fieldLayouts.c4ae2dd6-599c-4e06-a4b3-d48464040538.tabs.0.elements.0.autocomplete','false'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.fieldLayouts.c4ae2dd6-599c-4e06-a4b3-d48464040538.tabs.0.elements.0.autocorrect','true'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.fieldLayouts.c4ae2dd6-599c-4e06-a4b3-d48464040538.tabs.0.elements.0.class','null'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.fieldLayouts.c4ae2dd6-599c-4e06-a4b3-d48464040538.tabs.0.elements.0.disabled','false'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.fieldLayouts.c4ae2dd6-599c-4e06-a4b3-d48464040538.tabs.0.elements.0.id','null'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.fieldLayouts.c4ae2dd6-599c-4e06-a4b3-d48464040538.tabs.0.elements.0.instructions','null'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.fieldLayouts.c4ae2dd6-599c-4e06-a4b3-d48464040538.tabs.0.elements.0.label','null'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.fieldLayouts.c4ae2dd6-599c-4e06-a4b3-d48464040538.tabs.0.elements.0.max','null'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.fieldLayouts.c4ae2dd6-599c-4e06-a4b3-d48464040538.tabs.0.elements.0.min','null'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.fieldLayouts.c4ae2dd6-599c-4e06-a4b3-d48464040538.tabs.0.elements.0.name','null'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.fieldLayouts.c4ae2dd6-599c-4e06-a4b3-d48464040538.tabs.0.elements.0.orientation','null'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.fieldLayouts.c4ae2dd6-599c-4e06-a4b3-d48464040538.tabs.0.elements.0.placeholder','null'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.fieldLayouts.c4ae2dd6-599c-4e06-a4b3-d48464040538.tabs.0.elements.0.readonly','false'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.fieldLayouts.c4ae2dd6-599c-4e06-a4b3-d48464040538.tabs.0.elements.0.requirable','false'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.fieldLayouts.c4ae2dd6-599c-4e06-a4b3-d48464040538.tabs.0.elements.0.size','null'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.fieldLayouts.c4ae2dd6-599c-4e06-a4b3-d48464040538.tabs.0.elements.0.step','null'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.fieldLayouts.c4ae2dd6-599c-4e06-a4b3-d48464040538.tabs.0.elements.0.tip','null'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.fieldLayouts.c4ae2dd6-599c-4e06-a4b3-d48464040538.tabs.0.elements.0.title','null'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.fieldLayouts.c4ae2dd6-599c-4e06-a4b3-d48464040538.tabs.0.elements.0.type','\"craft\\\\fieldlayoutelements\\\\EntryTitleField\"'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.fieldLayouts.c4ae2dd6-599c-4e06-a4b3-d48464040538.tabs.0.elements.0.warning','null'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.fieldLayouts.c4ae2dd6-599c-4e06-a4b3-d48464040538.tabs.0.elements.0.width','100'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.fieldLayouts.c4ae2dd6-599c-4e06-a4b3-d48464040538.tabs.0.elements.1.fieldUid','\"2f04b707-164a-4ec2-aaa7-c570fc70a429\"'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.fieldLayouts.c4ae2dd6-599c-4e06-a4b3-d48464040538.tabs.0.elements.1.instructions','null'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.fieldLayouts.c4ae2dd6-599c-4e06-a4b3-d48464040538.tabs.0.elements.1.label','null'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.fieldLayouts.c4ae2dd6-599c-4e06-a4b3-d48464040538.tabs.0.elements.1.required','false'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.fieldLayouts.c4ae2dd6-599c-4e06-a4b3-d48464040538.tabs.0.elements.1.tip','null'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.fieldLayouts.c4ae2dd6-599c-4e06-a4b3-d48464040538.tabs.0.elements.1.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.fieldLayouts.c4ae2dd6-599c-4e06-a4b3-d48464040538.tabs.0.elements.1.warning','null'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.fieldLayouts.c4ae2dd6-599c-4e06-a4b3-d48464040538.tabs.0.elements.1.width','100'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.fieldLayouts.c4ae2dd6-599c-4e06-a4b3-d48464040538.tabs.0.name','\"Content\"'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.fieldLayouts.c4ae2dd6-599c-4e06-a4b3-d48464040538.tabs.0.sortOrder','1'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.fieldLayouts.c4ae2dd6-599c-4e06-a4b3-d48464040538.tabs.1.elements.0.fieldUid','\"3051443f-8b0d-4289-b9aa-32d2fb661d04\"'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.fieldLayouts.c4ae2dd6-599c-4e06-a4b3-d48464040538.tabs.1.elements.0.instructions','null'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.fieldLayouts.c4ae2dd6-599c-4e06-a4b3-d48464040538.tabs.1.elements.0.label','null'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.fieldLayouts.c4ae2dd6-599c-4e06-a4b3-d48464040538.tabs.1.elements.0.required','false'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.fieldLayouts.c4ae2dd6-599c-4e06-a4b3-d48464040538.tabs.1.elements.0.tip','null'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.fieldLayouts.c4ae2dd6-599c-4e06-a4b3-d48464040538.tabs.1.elements.0.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.fieldLayouts.c4ae2dd6-599c-4e06-a4b3-d48464040538.tabs.1.elements.0.warning','null'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.fieldLayouts.c4ae2dd6-599c-4e06-a4b3-d48464040538.tabs.1.elements.0.width','100'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.fieldLayouts.c4ae2dd6-599c-4e06-a4b3-d48464040538.tabs.1.name','\"SEO\"'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.fieldLayouts.c4ae2dd6-599c-4e06-a4b3-d48464040538.tabs.1.sortOrder','2'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.handle','\"home\"'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.hasTitleField','false'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.name','\"Home\"'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.section','\"8d13db31-6006-40c9-93ed-af826ba88337\"'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.sortOrder','1'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.titleFormat','\"{section.name|raw}\"'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.titleTranslationKeyFormat','null'),('entryTypes.403e009b-13b0-48cd-b608-fd12efc175cc.titleTranslationMethod','\"site\"'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.fieldLayouts.4d150b40-cb00-432e-b523-57410612ef34.tabs.0.elements.0.autocapitalize','true'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.fieldLayouts.4d150b40-cb00-432e-b523-57410612ef34.tabs.0.elements.0.autocomplete','false'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.fieldLayouts.4d150b40-cb00-432e-b523-57410612ef34.tabs.0.elements.0.autocorrect','true'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.fieldLayouts.4d150b40-cb00-432e-b523-57410612ef34.tabs.0.elements.0.class','null'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.fieldLayouts.4d150b40-cb00-432e-b523-57410612ef34.tabs.0.elements.0.disabled','false'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.fieldLayouts.4d150b40-cb00-432e-b523-57410612ef34.tabs.0.elements.0.id','null'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.fieldLayouts.4d150b40-cb00-432e-b523-57410612ef34.tabs.0.elements.0.instructions','null'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.fieldLayouts.4d150b40-cb00-432e-b523-57410612ef34.tabs.0.elements.0.label','null'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.fieldLayouts.4d150b40-cb00-432e-b523-57410612ef34.tabs.0.elements.0.max','null'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.fieldLayouts.4d150b40-cb00-432e-b523-57410612ef34.tabs.0.elements.0.min','null'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.fieldLayouts.4d150b40-cb00-432e-b523-57410612ef34.tabs.0.elements.0.name','null'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.fieldLayouts.4d150b40-cb00-432e-b523-57410612ef34.tabs.0.elements.0.orientation','null'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.fieldLayouts.4d150b40-cb00-432e-b523-57410612ef34.tabs.0.elements.0.placeholder','null'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.fieldLayouts.4d150b40-cb00-432e-b523-57410612ef34.tabs.0.elements.0.readonly','false'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.fieldLayouts.4d150b40-cb00-432e-b523-57410612ef34.tabs.0.elements.0.requirable','false'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.fieldLayouts.4d150b40-cb00-432e-b523-57410612ef34.tabs.0.elements.0.size','null'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.fieldLayouts.4d150b40-cb00-432e-b523-57410612ef34.tabs.0.elements.0.step','null'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.fieldLayouts.4d150b40-cb00-432e-b523-57410612ef34.tabs.0.elements.0.tip','null'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.fieldLayouts.4d150b40-cb00-432e-b523-57410612ef34.tabs.0.elements.0.title','null'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.fieldLayouts.4d150b40-cb00-432e-b523-57410612ef34.tabs.0.elements.0.type','\"craft\\\\fieldlayoutelements\\\\EntryTitleField\"'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.fieldLayouts.4d150b40-cb00-432e-b523-57410612ef34.tabs.0.elements.0.warning','null'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.fieldLayouts.4d150b40-cb00-432e-b523-57410612ef34.tabs.0.elements.0.width','100'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.fieldLayouts.4d150b40-cb00-432e-b523-57410612ef34.tabs.0.elements.1.fieldUid','\"57dee96c-7db3-4f24-aecd-0247689d33af\"'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.fieldLayouts.4d150b40-cb00-432e-b523-57410612ef34.tabs.0.elements.1.instructions','\"\"'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.fieldLayouts.4d150b40-cb00-432e-b523-57410612ef34.tabs.0.elements.1.label','\"\"'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.fieldLayouts.4d150b40-cb00-432e-b523-57410612ef34.tabs.0.elements.1.required','\"1\"'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.fieldLayouts.4d150b40-cb00-432e-b523-57410612ef34.tabs.0.elements.1.tip','null'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.fieldLayouts.4d150b40-cb00-432e-b523-57410612ef34.tabs.0.elements.1.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.fieldLayouts.4d150b40-cb00-432e-b523-57410612ef34.tabs.0.elements.1.warning','null'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.fieldLayouts.4d150b40-cb00-432e-b523-57410612ef34.tabs.0.elements.1.width','100'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.fieldLayouts.4d150b40-cb00-432e-b523-57410612ef34.tabs.0.name','\"Inhalt\"'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.fieldLayouts.4d150b40-cb00-432e-b523-57410612ef34.tabs.0.sortOrder','1'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.fieldLayouts.4d150b40-cb00-432e-b523-57410612ef34.tabs.1.elements.0.fieldUid','\"3051443f-8b0d-4289-b9aa-32d2fb661d04\"'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.fieldLayouts.4d150b40-cb00-432e-b523-57410612ef34.tabs.1.elements.0.instructions','null'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.fieldLayouts.4d150b40-cb00-432e-b523-57410612ef34.tabs.1.elements.0.label','null'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.fieldLayouts.4d150b40-cb00-432e-b523-57410612ef34.tabs.1.elements.0.required','false'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.fieldLayouts.4d150b40-cb00-432e-b523-57410612ef34.tabs.1.elements.0.tip','null'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.fieldLayouts.4d150b40-cb00-432e-b523-57410612ef34.tabs.1.elements.0.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.fieldLayouts.4d150b40-cb00-432e-b523-57410612ef34.tabs.1.elements.0.warning','null'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.fieldLayouts.4d150b40-cb00-432e-b523-57410612ef34.tabs.1.elements.0.width','100'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.fieldLayouts.4d150b40-cb00-432e-b523-57410612ef34.tabs.1.name','\"SEO\"'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.fieldLayouts.4d150b40-cb00-432e-b523-57410612ef34.tabs.1.sortOrder','2'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.handle','\"datenschutz\"'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.hasTitleField','false'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.name','\"Datenschutz\"'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.section','\"98c8b9df-a9ab-4d7b-8bff-02c330477b94\"'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.sortOrder','1'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.titleFormat','\"{section.name|raw}\"'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.titleTranslationKeyFormat','null'),('entryTypes.46256c7e-4ba9-4e8c-86ab-ffed5ab28925.titleTranslationMethod','\"site\"'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.fieldLayouts.3bb8c342-fb0b-4cc7-a550-be7e745290bf.tabs.0.elements.0.autocapitalize','true'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.fieldLayouts.3bb8c342-fb0b-4cc7-a550-be7e745290bf.tabs.0.elements.0.autocomplete','false'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.fieldLayouts.3bb8c342-fb0b-4cc7-a550-be7e745290bf.tabs.0.elements.0.autocorrect','true'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.fieldLayouts.3bb8c342-fb0b-4cc7-a550-be7e745290bf.tabs.0.elements.0.class','null'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.fieldLayouts.3bb8c342-fb0b-4cc7-a550-be7e745290bf.tabs.0.elements.0.disabled','false'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.fieldLayouts.3bb8c342-fb0b-4cc7-a550-be7e745290bf.tabs.0.elements.0.id','null'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.fieldLayouts.3bb8c342-fb0b-4cc7-a550-be7e745290bf.tabs.0.elements.0.instructions','null'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.fieldLayouts.3bb8c342-fb0b-4cc7-a550-be7e745290bf.tabs.0.elements.0.label','null'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.fieldLayouts.3bb8c342-fb0b-4cc7-a550-be7e745290bf.tabs.0.elements.0.max','null'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.fieldLayouts.3bb8c342-fb0b-4cc7-a550-be7e745290bf.tabs.0.elements.0.min','null'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.fieldLayouts.3bb8c342-fb0b-4cc7-a550-be7e745290bf.tabs.0.elements.0.name','null'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.fieldLayouts.3bb8c342-fb0b-4cc7-a550-be7e745290bf.tabs.0.elements.0.orientation','null'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.fieldLayouts.3bb8c342-fb0b-4cc7-a550-be7e745290bf.tabs.0.elements.0.placeholder','null'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.fieldLayouts.3bb8c342-fb0b-4cc7-a550-be7e745290bf.tabs.0.elements.0.readonly','false'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.fieldLayouts.3bb8c342-fb0b-4cc7-a550-be7e745290bf.tabs.0.elements.0.requirable','false'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.fieldLayouts.3bb8c342-fb0b-4cc7-a550-be7e745290bf.tabs.0.elements.0.size','null'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.fieldLayouts.3bb8c342-fb0b-4cc7-a550-be7e745290bf.tabs.0.elements.0.step','null'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.fieldLayouts.3bb8c342-fb0b-4cc7-a550-be7e745290bf.tabs.0.elements.0.tip','null'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.fieldLayouts.3bb8c342-fb0b-4cc7-a550-be7e745290bf.tabs.0.elements.0.title','null'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.fieldLayouts.3bb8c342-fb0b-4cc7-a550-be7e745290bf.tabs.0.elements.0.type','\"craft\\\\fieldlayoutelements\\\\EntryTitleField\"'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.fieldLayouts.3bb8c342-fb0b-4cc7-a550-be7e745290bf.tabs.0.elements.0.warning','null'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.fieldLayouts.3bb8c342-fb0b-4cc7-a550-be7e745290bf.tabs.0.elements.0.width','100'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.fieldLayouts.3bb8c342-fb0b-4cc7-a550-be7e745290bf.tabs.0.elements.1.fieldUid','\"57dee96c-7db3-4f24-aecd-0247689d33af\"'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.fieldLayouts.3bb8c342-fb0b-4cc7-a550-be7e745290bf.tabs.0.elements.1.instructions','\"\"'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.fieldLayouts.3bb8c342-fb0b-4cc7-a550-be7e745290bf.tabs.0.elements.1.label','\"\"'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.fieldLayouts.3bb8c342-fb0b-4cc7-a550-be7e745290bf.tabs.0.elements.1.required','\"1\"'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.fieldLayouts.3bb8c342-fb0b-4cc7-a550-be7e745290bf.tabs.0.elements.1.tip','null'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.fieldLayouts.3bb8c342-fb0b-4cc7-a550-be7e745290bf.tabs.0.elements.1.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.fieldLayouts.3bb8c342-fb0b-4cc7-a550-be7e745290bf.tabs.0.elements.1.warning','null'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.fieldLayouts.3bb8c342-fb0b-4cc7-a550-be7e745290bf.tabs.0.elements.1.width','100'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.fieldLayouts.3bb8c342-fb0b-4cc7-a550-be7e745290bf.tabs.0.name','\"Content\"'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.fieldLayouts.3bb8c342-fb0b-4cc7-a550-be7e745290bf.tabs.0.sortOrder','1'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.fieldLayouts.3bb8c342-fb0b-4cc7-a550-be7e745290bf.tabs.1.elements.0.fieldUid','\"3051443f-8b0d-4289-b9aa-32d2fb661d04\"'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.fieldLayouts.3bb8c342-fb0b-4cc7-a550-be7e745290bf.tabs.1.elements.0.instructions','null'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.fieldLayouts.3bb8c342-fb0b-4cc7-a550-be7e745290bf.tabs.1.elements.0.label','null'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.fieldLayouts.3bb8c342-fb0b-4cc7-a550-be7e745290bf.tabs.1.elements.0.required','false'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.fieldLayouts.3bb8c342-fb0b-4cc7-a550-be7e745290bf.tabs.1.elements.0.tip','null'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.fieldLayouts.3bb8c342-fb0b-4cc7-a550-be7e745290bf.tabs.1.elements.0.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.fieldLayouts.3bb8c342-fb0b-4cc7-a550-be7e745290bf.tabs.1.elements.0.warning','null'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.fieldLayouts.3bb8c342-fb0b-4cc7-a550-be7e745290bf.tabs.1.elements.0.width','100'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.fieldLayouts.3bb8c342-fb0b-4cc7-a550-be7e745290bf.tabs.1.name','\"SEO\"'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.fieldLayouts.3bb8c342-fb0b-4cc7-a550-be7e745290bf.tabs.1.sortOrder','2'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.handle','\"impressum\"'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.hasTitleField','false'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.name','\"Impressum\"'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.section','\"c26c6845-e3fa-4bdf-a418-c0de68a63354\"'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.sortOrder','1'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.titleFormat','\"{section.name|raw}\"'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.titleTranslationKeyFormat','null'),('entryTypes.9d82683d-e563-44d0-877d-3c99a3cb629e.titleTranslationMethod','\"site\"'),('fieldGroups.a8fcab52-fc56-45a3-8fb2-23ed44993312.name','\"Common\"'),('fields.0a46a4ed-9556-4b96-84fa-d8eda3ade2ba.contentColumnType','\"string\"'),('fields.0a46a4ed-9556-4b96-84fa-d8eda3ade2ba.fieldGroup','\"a8fcab52-fc56-45a3-8fb2-23ed44993312\"'),('fields.0a46a4ed-9556-4b96-84fa-d8eda3ade2ba.handle','\"adresse\"'),('fields.0a46a4ed-9556-4b96-84fa-d8eda3ade2ba.instructions','\"\"'),('fields.0a46a4ed-9556-4b96-84fa-d8eda3ade2ba.name','\"Adresse\"'),('fields.0a46a4ed-9556-4b96-84fa-d8eda3ade2ba.searchable','false'),('fields.0a46a4ed-9556-4b96-84fa-d8eda3ade2ba.settings.contentTable','\"{{%matrixcontent_adresse}}\"'),('fields.0a46a4ed-9556-4b96-84fa-d8eda3ade2ba.settings.maxBlocks','\"\"'),('fields.0a46a4ed-9556-4b96-84fa-d8eda3ade2ba.settings.minBlocks','\"\"'),('fields.0a46a4ed-9556-4b96-84fa-d8eda3ade2ba.settings.propagationMethod','\"all\"'),('fields.0a46a4ed-9556-4b96-84fa-d8eda3ade2ba.translationKeyFormat','null'),('fields.0a46a4ed-9556-4b96-84fa-d8eda3ade2ba.translationMethod','\"site\"'),('fields.0a46a4ed-9556-4b96-84fa-d8eda3ade2ba.type','\"craft\\\\fields\\\\Matrix\"'),('fields.138e98a9-42de-48bc-b510-6d191e15e10d.contentColumnType','\"string(40)\"'),('fields.138e98a9-42de-48bc-b510-6d191e15e10d.fieldGroup','\"a8fcab52-fc56-45a3-8fb2-23ed44993312\"'),('fields.138e98a9-42de-48bc-b510-6d191e15e10d.handle','\"firstTitle\"'),('fields.138e98a9-42de-48bc-b510-6d191e15e10d.instructions','\"\"'),('fields.138e98a9-42de-48bc-b510-6d191e15e10d.name','\"FirstTitle\"'),('fields.138e98a9-42de-48bc-b510-6d191e15e10d.searchable','false'),('fields.138e98a9-42de-48bc-b510-6d191e15e10d.settings.byteLimit','null'),('fields.138e98a9-42de-48bc-b510-6d191e15e10d.settings.charLimit','10'),('fields.138e98a9-42de-48bc-b510-6d191e15e10d.settings.code','\"\"'),('fields.138e98a9-42de-48bc-b510-6d191e15e10d.settings.columnType','null'),('fields.138e98a9-42de-48bc-b510-6d191e15e10d.settings.initialRows','\"4\"'),('fields.138e98a9-42de-48bc-b510-6d191e15e10d.settings.multiline','\"\"'),('fields.138e98a9-42de-48bc-b510-6d191e15e10d.settings.placeholder','\"\"'),('fields.138e98a9-42de-48bc-b510-6d191e15e10d.settings.uiMode','\"normal\"'),('fields.138e98a9-42de-48bc-b510-6d191e15e10d.translationKeyFormat','null'),('fields.138e98a9-42de-48bc-b510-6d191e15e10d.translationMethod','\"none\"'),('fields.138e98a9-42de-48bc-b510-6d191e15e10d.type','\"craft\\\\fields\\\\PlainText\"'),('fields.2516d21b-a4cf-4c46-b249-36b8cda1c705.contentColumnType','\"string(40)\"'),('fields.2516d21b-a4cf-4c46-b249-36b8cda1c705.fieldGroup','\"a8fcab52-fc56-45a3-8fb2-23ed44993312\"'),('fields.2516d21b-a4cf-4c46-b249-36b8cda1c705.handle','\"secondTitle\"'),('fields.2516d21b-a4cf-4c46-b249-36b8cda1c705.instructions','\"\"'),('fields.2516d21b-a4cf-4c46-b249-36b8cda1c705.name','\"SecondTitle\"'),('fields.2516d21b-a4cf-4c46-b249-36b8cda1c705.searchable','false'),('fields.2516d21b-a4cf-4c46-b249-36b8cda1c705.settings.byteLimit','null'),('fields.2516d21b-a4cf-4c46-b249-36b8cda1c705.settings.charLimit','10'),('fields.2516d21b-a4cf-4c46-b249-36b8cda1c705.settings.code','\"\"'),('fields.2516d21b-a4cf-4c46-b249-36b8cda1c705.settings.columnType','null'),('fields.2516d21b-a4cf-4c46-b249-36b8cda1c705.settings.initialRows','\"4\"'),('fields.2516d21b-a4cf-4c46-b249-36b8cda1c705.settings.multiline','\"\"'),('fields.2516d21b-a4cf-4c46-b249-36b8cda1c705.settings.placeholder','\"\"'),('fields.2516d21b-a4cf-4c46-b249-36b8cda1c705.settings.uiMode','\"normal\"'),('fields.2516d21b-a4cf-4c46-b249-36b8cda1c705.translationKeyFormat','null'),('fields.2516d21b-a4cf-4c46-b249-36b8cda1c705.translationMethod','\"none\"'),('fields.2516d21b-a4cf-4c46-b249-36b8cda1c705.type','\"craft\\\\fields\\\\PlainText\"'),('fields.2f04b707-164a-4ec2-aaa7-c570fc70a429.contentColumnType','\"string\"'),('fields.2f04b707-164a-4ec2-aaa7-c570fc70a429.fieldGroup','\"a8fcab52-fc56-45a3-8fb2-23ed44993312\"'),('fields.2f04b707-164a-4ec2-aaa7-c570fc70a429.handle','\"pageEntrys\"'),('fields.2f04b707-164a-4ec2-aaa7-c570fc70a429.instructions','\"\"'),('fields.2f04b707-164a-4ec2-aaa7-c570fc70a429.name','\"SeitenInhalte\"'),('fields.2f04b707-164a-4ec2-aaa7-c570fc70a429.searchable','false'),('fields.2f04b707-164a-4ec2-aaa7-c570fc70a429.settings.contentTable','\"{{%matrixcontent_pageentrys}}\"'),('fields.2f04b707-164a-4ec2-aaa7-c570fc70a429.settings.maxBlocks','\"\"'),('fields.2f04b707-164a-4ec2-aaa7-c570fc70a429.settings.minBlocks','\"\"'),('fields.2f04b707-164a-4ec2-aaa7-c570fc70a429.settings.propagationMethod','\"all\"'),('fields.2f04b707-164a-4ec2-aaa7-c570fc70a429.translationKeyFormat','null'),('fields.2f04b707-164a-4ec2-aaa7-c570fc70a429.translationMethod','\"site\"'),('fields.2f04b707-164a-4ec2-aaa7-c570fc70a429.type','\"craft\\\\fields\\\\Matrix\"'),('fields.3051443f-8b0d-4289-b9aa-32d2fb661d04.contentColumnType','\"text\"'),('fields.3051443f-8b0d-4289-b9aa-32d2fb661d04.fieldGroup','\"a8fcab52-fc56-45a3-8fb2-23ed44993312\"'),('fields.3051443f-8b0d-4289-b9aa-32d2fb661d04.handle','\"seo\"'),('fields.3051443f-8b0d-4289-b9aa-32d2fb661d04.instructions','\"\"'),('fields.3051443f-8b0d-4289-b9aa-32d2fb661d04.name','\"SEO\"'),('fields.3051443f-8b0d-4289-b9aa-32d2fb661d04.searchable','false'),('fields.3051443f-8b0d-4289-b9aa-32d2fb661d04.settings.description','\"\"'),('fields.3051443f-8b0d-4289-b9aa-32d2fb661d04.settings.hideSocial','\"\"'),('fields.3051443f-8b0d-4289-b9aa-32d2fb661d04.settings.robots.0','\"\"'),('fields.3051443f-8b0d-4289-b9aa-32d2fb661d04.settings.robots.1','\"\"'),('fields.3051443f-8b0d-4289-b9aa-32d2fb661d04.settings.robots.2','\"\"'),('fields.3051443f-8b0d-4289-b9aa-32d2fb661d04.settings.robots.3','\"\"'),('fields.3051443f-8b0d-4289-b9aa-32d2fb661d04.settings.robots.4','\"\"'),('fields.3051443f-8b0d-4289-b9aa-32d2fb661d04.settings.robots.5','\"\"'),('fields.3051443f-8b0d-4289-b9aa-32d2fb661d04.settings.socialImage','\"\"'),('fields.3051443f-8b0d-4289-b9aa-32d2fb661d04.settings.suffixAsPrefix','null'),('fields.3051443f-8b0d-4289-b9aa-32d2fb661d04.settings.title.0.__assoc__.0.0','\"key\"'),('fields.3051443f-8b0d-4289-b9aa-32d2fb661d04.settings.title.0.__assoc__.0.1','\"1\"'),('fields.3051443f-8b0d-4289-b9aa-32d2fb661d04.settings.title.0.__assoc__.1.0','\"locked\"'),('fields.3051443f-8b0d-4289-b9aa-32d2fb661d04.settings.title.0.__assoc__.1.1','\"0\"'),('fields.3051443f-8b0d-4289-b9aa-32d2fb661d04.settings.title.0.__assoc__.2.0','\"template\"'),('fields.3051443f-8b0d-4289-b9aa-32d2fb661d04.settings.title.0.__assoc__.2.1','\"{title}\"'),('fields.3051443f-8b0d-4289-b9aa-32d2fb661d04.settings.title.1.__assoc__.0.0','\"key\"'),('fields.3051443f-8b0d-4289-b9aa-32d2fb661d04.settings.title.1.__assoc__.0.1','\"2\"'),('fields.3051443f-8b0d-4289-b9aa-32d2fb661d04.settings.title.1.__assoc__.1.0','\"locked\"'),('fields.3051443f-8b0d-4289-b9aa-32d2fb661d04.settings.title.1.__assoc__.1.1','\"1\"'),('fields.3051443f-8b0d-4289-b9aa-32d2fb661d04.settings.title.1.__assoc__.2.0','\"template\"'),('fields.3051443f-8b0d-4289-b9aa-32d2fb661d04.settings.title.1.__assoc__.2.1','\" - {{ siteName }}\"'),('fields.3051443f-8b0d-4289-b9aa-32d2fb661d04.settings.titleSuffix','null'),('fields.3051443f-8b0d-4289-b9aa-32d2fb661d04.translationKeyFormat','null'),('fields.3051443f-8b0d-4289-b9aa-32d2fb661d04.translationMethod','\"none\"'),('fields.3051443f-8b0d-4289-b9aa-32d2fb661d04.type','\"ether\\\\seo\\\\fields\\\\SeoField\"'),('fields.57dee96c-7db3-4f24-aecd-0247689d33af.contentColumnType','\"text\"'),('fields.57dee96c-7db3-4f24-aecd-0247689d33af.fieldGroup','\"a8fcab52-fc56-45a3-8fb2-23ed44993312\"'),('fields.57dee96c-7db3-4f24-aecd-0247689d33af.handle','\"richText\"'),('fields.57dee96c-7db3-4f24-aecd-0247689d33af.instructions','\"\"'),('fields.57dee96c-7db3-4f24-aecd-0247689d33af.name','\"Rich Text\"'),('fields.57dee96c-7db3-4f24-aecd-0247689d33af.searchable','false'),('fields.57dee96c-7db3-4f24-aecd-0247689d33af.settings.availableTransforms','\"*\"'),('fields.57dee96c-7db3-4f24-aecd-0247689d33af.settings.availableVolumes','\"*\"'),('fields.57dee96c-7db3-4f24-aecd-0247689d33af.settings.cleanupHtml','true'),('fields.57dee96c-7db3-4f24-aecd-0247689d33af.settings.columnType','\"text\"'),('fields.57dee96c-7db3-4f24-aecd-0247689d33af.settings.configSelectionMode','\"choose\"'),('fields.57dee96c-7db3-4f24-aecd-0247689d33af.settings.defaultTransform','\"\"'),('fields.57dee96c-7db3-4f24-aecd-0247689d33af.settings.manualConfig','\"\"'),('fields.57dee96c-7db3-4f24-aecd-0247689d33af.settings.purifierConfig','\"\"'),('fields.57dee96c-7db3-4f24-aecd-0247689d33af.settings.purifyHtml','\"1\"'),('fields.57dee96c-7db3-4f24-aecd-0247689d33af.settings.redactorConfig','\"Simple.json\"'),('fields.57dee96c-7db3-4f24-aecd-0247689d33af.settings.removeEmptyTags','\"1\"'),('fields.57dee96c-7db3-4f24-aecd-0247689d33af.settings.removeInlineStyles','\"1\"'),('fields.57dee96c-7db3-4f24-aecd-0247689d33af.settings.removeNbsp','\"1\"'),('fields.57dee96c-7db3-4f24-aecd-0247689d33af.settings.showHtmlButtonForNonAdmins','\"\"'),('fields.57dee96c-7db3-4f24-aecd-0247689d33af.settings.showUnpermittedFiles','false'),('fields.57dee96c-7db3-4f24-aecd-0247689d33af.settings.showUnpermittedVolumes','false'),('fields.57dee96c-7db3-4f24-aecd-0247689d33af.settings.uiMode','\"enlarged\"'),('fields.57dee96c-7db3-4f24-aecd-0247689d33af.translationKeyFormat','null'),('fields.57dee96c-7db3-4f24-aecd-0247689d33af.translationMethod','\"none\"'),('fields.57dee96c-7db3-4f24-aecd-0247689d33af.type','\"craft\\\\redactor\\\\Field\"'),('fields.8a447b6c-8baf-4bde-8ff8-1983ae8638f8.contentColumnType','\"string\"'),('fields.8a447b6c-8baf-4bde-8ff8-1983ae8638f8.fieldGroup','\"a8fcab52-fc56-45a3-8fb2-23ed44993312\"'),('fields.8a447b6c-8baf-4bde-8ff8-1983ae8638f8.handle','\"featureImage\"'),('fields.8a447b6c-8baf-4bde-8ff8-1983ae8638f8.instructions','\"\"'),('fields.8a447b6c-8baf-4bde-8ff8-1983ae8638f8.name','\"Feature Image\"'),('fields.8a447b6c-8baf-4bde-8ff8-1983ae8638f8.searchable','false'),('fields.8a447b6c-8baf-4bde-8ff8-1983ae8638f8.settings.allowedKinds.0','\"image\"'),('fields.8a447b6c-8baf-4bde-8ff8-1983ae8638f8.settings.allowSelfRelations','false'),('fields.8a447b6c-8baf-4bde-8ff8-1983ae8638f8.settings.allowUploads','true'),('fields.8a447b6c-8baf-4bde-8ff8-1983ae8638f8.settings.defaultUploadLocationSource','\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"'),('fields.8a447b6c-8baf-4bde-8ff8-1983ae8638f8.settings.defaultUploadLocationSubpath','\"\"'),('fields.8a447b6c-8baf-4bde-8ff8-1983ae8638f8.settings.limit','\"1\"'),('fields.8a447b6c-8baf-4bde-8ff8-1983ae8638f8.settings.localizeRelations','false'),('fields.8a447b6c-8baf-4bde-8ff8-1983ae8638f8.settings.previewMode','\"full\"'),('fields.8a447b6c-8baf-4bde-8ff8-1983ae8638f8.settings.restrictFiles','\"1\"'),('fields.8a447b6c-8baf-4bde-8ff8-1983ae8638f8.settings.selectionLabel','\"Add Feature Image\"'),('fields.8a447b6c-8baf-4bde-8ff8-1983ae8638f8.settings.showSiteMenu','false'),('fields.8a447b6c-8baf-4bde-8ff8-1983ae8638f8.settings.showUnpermittedFiles','false'),('fields.8a447b6c-8baf-4bde-8ff8-1983ae8638f8.settings.showUnpermittedVolumes','false'),('fields.8a447b6c-8baf-4bde-8ff8-1983ae8638f8.settings.singleUploadLocationSource','\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"'),('fields.8a447b6c-8baf-4bde-8ff8-1983ae8638f8.settings.singleUploadLocationSubpath','\"\"'),('fields.8a447b6c-8baf-4bde-8ff8-1983ae8638f8.settings.source','null'),('fields.8a447b6c-8baf-4bde-8ff8-1983ae8638f8.settings.sources.0','\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"'),('fields.8a447b6c-8baf-4bde-8ff8-1983ae8638f8.settings.targetSiteId','null'),('fields.8a447b6c-8baf-4bde-8ff8-1983ae8638f8.settings.useSingleFolder','false'),('fields.8a447b6c-8baf-4bde-8ff8-1983ae8638f8.settings.validateRelatedElements','false'),('fields.8a447b6c-8baf-4bde-8ff8-1983ae8638f8.settings.viewMode','\"large\"'),('fields.8a447b6c-8baf-4bde-8ff8-1983ae8638f8.translationKeyFormat','null'),('fields.8a447b6c-8baf-4bde-8ff8-1983ae8638f8.translationMethod','\"site\"'),('fields.8a447b6c-8baf-4bde-8ff8-1983ae8638f8.type','\"craft\\\\fields\\\\Assets\"'),('fields.b90564fa-6300-465d-b445-7295838b8730.contentColumnType','\"string(400)\"'),('fields.b90564fa-6300-465d-b445-7295838b8730.fieldGroup','\"a8fcab52-fc56-45a3-8fb2-23ed44993312\"'),('fields.b90564fa-6300-465d-b445-7295838b8730.handle','\"titel\"'),('fields.b90564fa-6300-465d-b445-7295838b8730.instructions','\"\"'),('fields.b90564fa-6300-465d-b445-7295838b8730.name','\"Titel\"'),('fields.b90564fa-6300-465d-b445-7295838b8730.searchable','false'),('fields.b90564fa-6300-465d-b445-7295838b8730.settings.byteLimit','null'),('fields.b90564fa-6300-465d-b445-7295838b8730.settings.charLimit','100'),('fields.b90564fa-6300-465d-b445-7295838b8730.settings.code','\"\"'),('fields.b90564fa-6300-465d-b445-7295838b8730.settings.columnType','null'),('fields.b90564fa-6300-465d-b445-7295838b8730.settings.initialRows','\"4\"'),('fields.b90564fa-6300-465d-b445-7295838b8730.settings.multiline','\"\"'),('fields.b90564fa-6300-465d-b445-7295838b8730.settings.placeholder','\"\"'),('fields.b90564fa-6300-465d-b445-7295838b8730.settings.uiMode','\"normal\"'),('fields.b90564fa-6300-465d-b445-7295838b8730.translationKeyFormat','null'),('fields.b90564fa-6300-465d-b445-7295838b8730.translationMethod','\"none\"'),('fields.b90564fa-6300-465d-b445-7295838b8730.type','\"craft\\\\fields\\\\PlainText\"'),('fields.bb73c66e-f14a-4f5c-856b-c4869e2aa7e5.contentColumnType','\"string\"'),('fields.bb73c66e-f14a-4f5c-856b-c4869e2aa7e5.fieldGroup','\"a8fcab52-fc56-45a3-8fb2-23ed44993312\"'),('fields.bb73c66e-f14a-4f5c-856b-c4869e2aa7e5.handle','\"homeNavigationImage\"'),('fields.bb73c66e-f14a-4f5c-856b-c4869e2aa7e5.instructions','\"\"'),('fields.bb73c66e-f14a-4f5c-856b-c4869e2aa7e5.name','\"Startseite Bild\"'),('fields.bb73c66e-f14a-4f5c-856b-c4869e2aa7e5.searchable','false'),('fields.bb73c66e-f14a-4f5c-856b-c4869e2aa7e5.settings.allowedKinds','null'),('fields.bb73c66e-f14a-4f5c-856b-c4869e2aa7e5.settings.allowSelfRelations','false'),('fields.bb73c66e-f14a-4f5c-856b-c4869e2aa7e5.settings.allowUploads','true'),('fields.bb73c66e-f14a-4f5c-856b-c4869e2aa7e5.settings.defaultUploadLocationSource','\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"'),('fields.bb73c66e-f14a-4f5c-856b-c4869e2aa7e5.settings.defaultUploadLocationSubpath','\"\"'),('fields.bb73c66e-f14a-4f5c-856b-c4869e2aa7e5.settings.limit','\"\"'),('fields.bb73c66e-f14a-4f5c-856b-c4869e2aa7e5.settings.localizeRelations','false'),('fields.bb73c66e-f14a-4f5c-856b-c4869e2aa7e5.settings.previewMode','\"full\"'),('fields.bb73c66e-f14a-4f5c-856b-c4869e2aa7e5.settings.restrictFiles','\"\"'),('fields.bb73c66e-f14a-4f5c-856b-c4869e2aa7e5.settings.selectionLabel','\"\"'),('fields.bb73c66e-f14a-4f5c-856b-c4869e2aa7e5.settings.showSiteMenu','false'),('fields.bb73c66e-f14a-4f5c-856b-c4869e2aa7e5.settings.showUnpermittedFiles','false'),('fields.bb73c66e-f14a-4f5c-856b-c4869e2aa7e5.settings.showUnpermittedVolumes','false'),('fields.bb73c66e-f14a-4f5c-856b-c4869e2aa7e5.settings.singleUploadLocationSource','\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"'),('fields.bb73c66e-f14a-4f5c-856b-c4869e2aa7e5.settings.singleUploadLocationSubpath','\"\"'),('fields.bb73c66e-f14a-4f5c-856b-c4869e2aa7e5.settings.source','null'),('fields.bb73c66e-f14a-4f5c-856b-c4869e2aa7e5.settings.sources.0','\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"'),('fields.bb73c66e-f14a-4f5c-856b-c4869e2aa7e5.settings.targetSiteId','null'),('fields.bb73c66e-f14a-4f5c-856b-c4869e2aa7e5.settings.useSingleFolder','false'),('fields.bb73c66e-f14a-4f5c-856b-c4869e2aa7e5.settings.validateRelatedElements','false'),('fields.bb73c66e-f14a-4f5c-856b-c4869e2aa7e5.settings.viewMode','\"list\"'),('fields.bb73c66e-f14a-4f5c-856b-c4869e2aa7e5.translationKeyFormat','null'),('fields.bb73c66e-f14a-4f5c-856b-c4869e2aa7e5.translationMethod','\"site\"'),('fields.bb73c66e-f14a-4f5c-856b-c4869e2aa7e5.type','\"craft\\\\fields\\\\Assets\"'),('globalSets.24aee758-3b1e-4075-8529-4677b30ce2c7.fieldLayouts.7e902028-73bf-4b93-9261-8b477cbeebe6.tabs.0.elements.0.fieldUid','\"0a46a4ed-9556-4b96-84fa-d8eda3ade2ba\"'),('globalSets.24aee758-3b1e-4075-8529-4677b30ce2c7.fieldLayouts.7e902028-73bf-4b93-9261-8b477cbeebe6.tabs.0.elements.0.instructions','null'),('globalSets.24aee758-3b1e-4075-8529-4677b30ce2c7.fieldLayouts.7e902028-73bf-4b93-9261-8b477cbeebe6.tabs.0.elements.0.label','null'),('globalSets.24aee758-3b1e-4075-8529-4677b30ce2c7.fieldLayouts.7e902028-73bf-4b93-9261-8b477cbeebe6.tabs.0.elements.0.required','false'),('globalSets.24aee758-3b1e-4075-8529-4677b30ce2c7.fieldLayouts.7e902028-73bf-4b93-9261-8b477cbeebe6.tabs.0.elements.0.tip','null'),('globalSets.24aee758-3b1e-4075-8529-4677b30ce2c7.fieldLayouts.7e902028-73bf-4b93-9261-8b477cbeebe6.tabs.0.elements.0.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),('globalSets.24aee758-3b1e-4075-8529-4677b30ce2c7.fieldLayouts.7e902028-73bf-4b93-9261-8b477cbeebe6.tabs.0.elements.0.warning','null'),('globalSets.24aee758-3b1e-4075-8529-4677b30ce2c7.fieldLayouts.7e902028-73bf-4b93-9261-8b477cbeebe6.tabs.0.elements.0.width','100'),('globalSets.24aee758-3b1e-4075-8529-4677b30ce2c7.fieldLayouts.7e902028-73bf-4b93-9261-8b477cbeebe6.tabs.0.name','\"Tab 1\"'),('globalSets.24aee758-3b1e-4075-8529-4677b30ce2c7.fieldLayouts.7e902028-73bf-4b93-9261-8b477cbeebe6.tabs.0.sortOrder','1'),('globalSets.24aee758-3b1e-4075-8529-4677b30ce2c7.handle','\"kontaktDaten\"'),('globalSets.24aee758-3b1e-4075-8529-4677b30ce2c7.name','\"Kontakt Daten\"'),('globalSets.760dd7bf-1d0f-4f6f-addf-6b3f732ad96b.fieldLayouts.3e1f527f-210e-4de0-9c52-093dc408e077.tabs.0.elements.0.fieldUid','\"138e98a9-42de-48bc-b510-6d191e15e10d\"'),('globalSets.760dd7bf-1d0f-4f6f-addf-6b3f732ad96b.fieldLayouts.3e1f527f-210e-4de0-9c52-093dc408e077.tabs.0.elements.0.instructions','null'),('globalSets.760dd7bf-1d0f-4f6f-addf-6b3f732ad96b.fieldLayouts.3e1f527f-210e-4de0-9c52-093dc408e077.tabs.0.elements.0.label','null'),('globalSets.760dd7bf-1d0f-4f6f-addf-6b3f732ad96b.fieldLayouts.3e1f527f-210e-4de0-9c52-093dc408e077.tabs.0.elements.0.required','false'),('globalSets.760dd7bf-1d0f-4f6f-addf-6b3f732ad96b.fieldLayouts.3e1f527f-210e-4de0-9c52-093dc408e077.tabs.0.elements.0.tip','null'),('globalSets.760dd7bf-1d0f-4f6f-addf-6b3f732ad96b.fieldLayouts.3e1f527f-210e-4de0-9c52-093dc408e077.tabs.0.elements.0.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),('globalSets.760dd7bf-1d0f-4f6f-addf-6b3f732ad96b.fieldLayouts.3e1f527f-210e-4de0-9c52-093dc408e077.tabs.0.elements.0.warning','null'),('globalSets.760dd7bf-1d0f-4f6f-addf-6b3f732ad96b.fieldLayouts.3e1f527f-210e-4de0-9c52-093dc408e077.tabs.0.elements.0.width','100'),('globalSets.760dd7bf-1d0f-4f6f-addf-6b3f732ad96b.fieldLayouts.3e1f527f-210e-4de0-9c52-093dc408e077.tabs.0.elements.1.fieldUid','\"2516d21b-a4cf-4c46-b249-36b8cda1c705\"'),('globalSets.760dd7bf-1d0f-4f6f-addf-6b3f732ad96b.fieldLayouts.3e1f527f-210e-4de0-9c52-093dc408e077.tabs.0.elements.1.instructions','null'),('globalSets.760dd7bf-1d0f-4f6f-addf-6b3f732ad96b.fieldLayouts.3e1f527f-210e-4de0-9c52-093dc408e077.tabs.0.elements.1.label','null'),('globalSets.760dd7bf-1d0f-4f6f-addf-6b3f732ad96b.fieldLayouts.3e1f527f-210e-4de0-9c52-093dc408e077.tabs.0.elements.1.required','false'),('globalSets.760dd7bf-1d0f-4f6f-addf-6b3f732ad96b.fieldLayouts.3e1f527f-210e-4de0-9c52-093dc408e077.tabs.0.elements.1.tip','null'),('globalSets.760dd7bf-1d0f-4f6f-addf-6b3f732ad96b.fieldLayouts.3e1f527f-210e-4de0-9c52-093dc408e077.tabs.0.elements.1.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),('globalSets.760dd7bf-1d0f-4f6f-addf-6b3f732ad96b.fieldLayouts.3e1f527f-210e-4de0-9c52-093dc408e077.tabs.0.elements.1.warning','null'),('globalSets.760dd7bf-1d0f-4f6f-addf-6b3f732ad96b.fieldLayouts.3e1f527f-210e-4de0-9c52-093dc408e077.tabs.0.elements.1.width','100'),('globalSets.760dd7bf-1d0f-4f6f-addf-6b3f732ad96b.fieldLayouts.3e1f527f-210e-4de0-9c52-093dc408e077.tabs.0.elements.2.fieldUid','\"8a447b6c-8baf-4bde-8ff8-1983ae8638f8\"'),('globalSets.760dd7bf-1d0f-4f6f-addf-6b3f732ad96b.fieldLayouts.3e1f527f-210e-4de0-9c52-093dc408e077.tabs.0.elements.2.instructions','null'),('globalSets.760dd7bf-1d0f-4f6f-addf-6b3f732ad96b.fieldLayouts.3e1f527f-210e-4de0-9c52-093dc408e077.tabs.0.elements.2.label','null'),('globalSets.760dd7bf-1d0f-4f6f-addf-6b3f732ad96b.fieldLayouts.3e1f527f-210e-4de0-9c52-093dc408e077.tabs.0.elements.2.required','false'),('globalSets.760dd7bf-1d0f-4f6f-addf-6b3f732ad96b.fieldLayouts.3e1f527f-210e-4de0-9c52-093dc408e077.tabs.0.elements.2.tip','null'),('globalSets.760dd7bf-1d0f-4f6f-addf-6b3f732ad96b.fieldLayouts.3e1f527f-210e-4de0-9c52-093dc408e077.tabs.0.elements.2.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),('globalSets.760dd7bf-1d0f-4f6f-addf-6b3f732ad96b.fieldLayouts.3e1f527f-210e-4de0-9c52-093dc408e077.tabs.0.elements.2.warning','null'),('globalSets.760dd7bf-1d0f-4f6f-addf-6b3f732ad96b.fieldLayouts.3e1f527f-210e-4de0-9c52-093dc408e077.tabs.0.elements.2.width','100'),('globalSets.760dd7bf-1d0f-4f6f-addf-6b3f732ad96b.fieldLayouts.3e1f527f-210e-4de0-9c52-093dc408e077.tabs.0.elements.3.fieldUid','\"bb73c66e-f14a-4f5c-856b-c4869e2aa7e5\"'),('globalSets.760dd7bf-1d0f-4f6f-addf-6b3f732ad96b.fieldLayouts.3e1f527f-210e-4de0-9c52-093dc408e077.tabs.0.elements.3.instructions','null'),('globalSets.760dd7bf-1d0f-4f6f-addf-6b3f732ad96b.fieldLayouts.3e1f527f-210e-4de0-9c52-093dc408e077.tabs.0.elements.3.label','null'),('globalSets.760dd7bf-1d0f-4f6f-addf-6b3f732ad96b.fieldLayouts.3e1f527f-210e-4de0-9c52-093dc408e077.tabs.0.elements.3.required','false'),('globalSets.760dd7bf-1d0f-4f6f-addf-6b3f732ad96b.fieldLayouts.3e1f527f-210e-4de0-9c52-093dc408e077.tabs.0.elements.3.tip','null'),('globalSets.760dd7bf-1d0f-4f6f-addf-6b3f732ad96b.fieldLayouts.3e1f527f-210e-4de0-9c52-093dc408e077.tabs.0.elements.3.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),('globalSets.760dd7bf-1d0f-4f6f-addf-6b3f732ad96b.fieldLayouts.3e1f527f-210e-4de0-9c52-093dc408e077.tabs.0.elements.3.warning','null'),('globalSets.760dd7bf-1d0f-4f6f-addf-6b3f732ad96b.fieldLayouts.3e1f527f-210e-4de0-9c52-093dc408e077.tabs.0.elements.3.width','100'),('globalSets.760dd7bf-1d0f-4f6f-addf-6b3f732ad96b.fieldLayouts.3e1f527f-210e-4de0-9c52-093dc408e077.tabs.0.name','\"Tab 1\"'),('globalSets.760dd7bf-1d0f-4f6f-addf-6b3f732ad96b.fieldLayouts.3e1f527f-210e-4de0-9c52-093dc408e077.tabs.0.sortOrder','1'),('globalSets.760dd7bf-1d0f-4f6f-addf-6b3f732ad96b.handle','\"header\"'),('globalSets.760dd7bf-1d0f-4f6f-addf-6b3f732ad96b.name','\"Header\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.field','\"2f04b707-164a-4ec2-aaa7-c570fc70a429\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fieldLayouts.4982bec9-f34e-403e-b46f-0bcc1384757e.tabs.0.elements.0.fieldUid','\"c27a80ef-83b3-4da1-a3b4-170cb07b16ef\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fieldLayouts.4982bec9-f34e-403e-b46f-0bcc1384757e.tabs.0.elements.0.instructions','null'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fieldLayouts.4982bec9-f34e-403e-b46f-0bcc1384757e.tabs.0.elements.0.label','null'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fieldLayouts.4982bec9-f34e-403e-b46f-0bcc1384757e.tabs.0.elements.0.required','true'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fieldLayouts.4982bec9-f34e-403e-b46f-0bcc1384757e.tabs.0.elements.0.tip','null'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fieldLayouts.4982bec9-f34e-403e-b46f-0bcc1384757e.tabs.0.elements.0.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fieldLayouts.4982bec9-f34e-403e-b46f-0bcc1384757e.tabs.0.elements.0.warning','null'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fieldLayouts.4982bec9-f34e-403e-b46f-0bcc1384757e.tabs.0.elements.0.width','100'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fieldLayouts.4982bec9-f34e-403e-b46f-0bcc1384757e.tabs.0.elements.1.fieldUid','\"e9bdc3b6-4eb6-4043-8b52-813ac45b5aca\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fieldLayouts.4982bec9-f34e-403e-b46f-0bcc1384757e.tabs.0.elements.1.instructions','null'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fieldLayouts.4982bec9-f34e-403e-b46f-0bcc1384757e.tabs.0.elements.1.label','null'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fieldLayouts.4982bec9-f34e-403e-b46f-0bcc1384757e.tabs.0.elements.1.required','true'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fieldLayouts.4982bec9-f34e-403e-b46f-0bcc1384757e.tabs.0.elements.1.tip','null'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fieldLayouts.4982bec9-f34e-403e-b46f-0bcc1384757e.tabs.0.elements.1.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fieldLayouts.4982bec9-f34e-403e-b46f-0bcc1384757e.tabs.0.elements.1.warning','null'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fieldLayouts.4982bec9-f34e-403e-b46f-0bcc1384757e.tabs.0.elements.1.width','100'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fieldLayouts.4982bec9-f34e-403e-b46f-0bcc1384757e.tabs.0.elements.2.fieldUid','\"433925d5-4dc7-4a35-8a80-42573edd3c1e\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fieldLayouts.4982bec9-f34e-403e-b46f-0bcc1384757e.tabs.0.elements.2.instructions','null'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fieldLayouts.4982bec9-f34e-403e-b46f-0bcc1384757e.tabs.0.elements.2.label','null'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fieldLayouts.4982bec9-f34e-403e-b46f-0bcc1384757e.tabs.0.elements.2.required','true'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fieldLayouts.4982bec9-f34e-403e-b46f-0bcc1384757e.tabs.0.elements.2.tip','null'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fieldLayouts.4982bec9-f34e-403e-b46f-0bcc1384757e.tabs.0.elements.2.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fieldLayouts.4982bec9-f34e-403e-b46f-0bcc1384757e.tabs.0.elements.2.warning','null'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fieldLayouts.4982bec9-f34e-403e-b46f-0bcc1384757e.tabs.0.elements.2.width','100'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fieldLayouts.4982bec9-f34e-403e-b46f-0bcc1384757e.tabs.0.name','\"Content\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fieldLayouts.4982bec9-f34e-403e-b46f-0bcc1384757e.tabs.0.sortOrder','1'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.433925d5-4dc7-4a35-8a80-42573edd3c1e.contentColumnType','\"text\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.433925d5-4dc7-4a35-8a80-42573edd3c1e.fieldGroup','null'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.433925d5-4dc7-4a35-8a80-42573edd3c1e.handle','\"titel\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.433925d5-4dc7-4a35-8a80-42573edd3c1e.instructions','\"\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.433925d5-4dc7-4a35-8a80-42573edd3c1e.name','\"Titel\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.433925d5-4dc7-4a35-8a80-42573edd3c1e.searchable','false'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.433925d5-4dc7-4a35-8a80-42573edd3c1e.settings.availableTransforms','\"*\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.433925d5-4dc7-4a35-8a80-42573edd3c1e.settings.availableVolumes','\"*\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.433925d5-4dc7-4a35-8a80-42573edd3c1e.settings.cleanupHtml','true'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.433925d5-4dc7-4a35-8a80-42573edd3c1e.settings.columnType','\"text\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.433925d5-4dc7-4a35-8a80-42573edd3c1e.settings.configSelectionMode','\"choose\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.433925d5-4dc7-4a35-8a80-42573edd3c1e.settings.defaultTransform','\"\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.433925d5-4dc7-4a35-8a80-42573edd3c1e.settings.manualConfig','\"\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.433925d5-4dc7-4a35-8a80-42573edd3c1e.settings.purifierConfig','\"\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.433925d5-4dc7-4a35-8a80-42573edd3c1e.settings.purifyHtml','\"1\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.433925d5-4dc7-4a35-8a80-42573edd3c1e.settings.redactorConfig','\"\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.433925d5-4dc7-4a35-8a80-42573edd3c1e.settings.removeEmptyTags','\"1\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.433925d5-4dc7-4a35-8a80-42573edd3c1e.settings.removeInlineStyles','\"1\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.433925d5-4dc7-4a35-8a80-42573edd3c1e.settings.removeNbsp','\"1\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.433925d5-4dc7-4a35-8a80-42573edd3c1e.settings.showHtmlButtonForNonAdmins','\"\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.433925d5-4dc7-4a35-8a80-42573edd3c1e.settings.showUnpermittedFiles','false'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.433925d5-4dc7-4a35-8a80-42573edd3c1e.settings.showUnpermittedVolumes','false'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.433925d5-4dc7-4a35-8a80-42573edd3c1e.settings.uiMode','\"enlarged\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.433925d5-4dc7-4a35-8a80-42573edd3c1e.translationKeyFormat','null'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.433925d5-4dc7-4a35-8a80-42573edd3c1e.translationMethod','\"none\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.433925d5-4dc7-4a35-8a80-42573edd3c1e.type','\"craft\\\\redactor\\\\Field\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.c27a80ef-83b3-4da1-a3b4-170cb07b16ef.contentColumnType','\"string\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.c27a80ef-83b3-4da1-a3b4-170cb07b16ef.fieldGroup','null'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.c27a80ef-83b3-4da1-a3b4-170cb07b16ef.handle','\"navigationImage\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.c27a80ef-83b3-4da1-a3b4-170cb07b16ef.instructions','\"\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.c27a80ef-83b3-4da1-a3b4-170cb07b16ef.name','\"Navigation Bild\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.c27a80ef-83b3-4da1-a3b4-170cb07b16ef.searchable','false'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.c27a80ef-83b3-4da1-a3b4-170cb07b16ef.settings.allowedKinds','null'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.c27a80ef-83b3-4da1-a3b4-170cb07b16ef.settings.allowSelfRelations','false'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.c27a80ef-83b3-4da1-a3b4-170cb07b16ef.settings.allowUploads','true'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.c27a80ef-83b3-4da1-a3b4-170cb07b16ef.settings.defaultUploadLocationSource','\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.c27a80ef-83b3-4da1-a3b4-170cb07b16ef.settings.defaultUploadLocationSubpath','\"\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.c27a80ef-83b3-4da1-a3b4-170cb07b16ef.settings.limit','\"1\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.c27a80ef-83b3-4da1-a3b4-170cb07b16ef.settings.localizeRelations','false'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.c27a80ef-83b3-4da1-a3b4-170cb07b16ef.settings.previewMode','\"full\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.c27a80ef-83b3-4da1-a3b4-170cb07b16ef.settings.restrictFiles','\"\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.c27a80ef-83b3-4da1-a3b4-170cb07b16ef.settings.selectionLabel','\"\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.c27a80ef-83b3-4da1-a3b4-170cb07b16ef.settings.showSiteMenu','true'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.c27a80ef-83b3-4da1-a3b4-170cb07b16ef.settings.showUnpermittedFiles','false'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.c27a80ef-83b3-4da1-a3b4-170cb07b16ef.settings.showUnpermittedVolumes','false'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.c27a80ef-83b3-4da1-a3b4-170cb07b16ef.settings.singleUploadLocationSource','\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.c27a80ef-83b3-4da1-a3b4-170cb07b16ef.settings.singleUploadLocationSubpath','\"\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.c27a80ef-83b3-4da1-a3b4-170cb07b16ef.settings.source','null'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.c27a80ef-83b3-4da1-a3b4-170cb07b16ef.settings.sources.0','\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.c27a80ef-83b3-4da1-a3b4-170cb07b16ef.settings.targetSiteId','null'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.c27a80ef-83b3-4da1-a3b4-170cb07b16ef.settings.useSingleFolder','false'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.c27a80ef-83b3-4da1-a3b4-170cb07b16ef.settings.validateRelatedElements','false'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.c27a80ef-83b3-4da1-a3b4-170cb07b16ef.settings.viewMode','\"list\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.c27a80ef-83b3-4da1-a3b4-170cb07b16ef.translationKeyFormat','null'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.c27a80ef-83b3-4da1-a3b4-170cb07b16ef.translationMethod','\"site\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.c27a80ef-83b3-4da1-a3b4-170cb07b16ef.type','\"craft\\\\fields\\\\Assets\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.e9bdc3b6-4eb6-4043-8b52-813ac45b5aca.contentColumnType','\"string(400)\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.e9bdc3b6-4eb6-4043-8b52-813ac45b5aca.fieldGroup','null'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.e9bdc3b6-4eb6-4043-8b52-813ac45b5aca.handle','\"smalltitel\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.e9bdc3b6-4eb6-4043-8b52-813ac45b5aca.instructions','\"\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.e9bdc3b6-4eb6-4043-8b52-813ac45b5aca.name','\"Kleiner Titel\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.e9bdc3b6-4eb6-4043-8b52-813ac45b5aca.searchable','false'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.e9bdc3b6-4eb6-4043-8b52-813ac45b5aca.settings.byteLimit','null'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.e9bdc3b6-4eb6-4043-8b52-813ac45b5aca.settings.charLimit','100'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.e9bdc3b6-4eb6-4043-8b52-813ac45b5aca.settings.code','\"\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.e9bdc3b6-4eb6-4043-8b52-813ac45b5aca.settings.columnType','null'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.e9bdc3b6-4eb6-4043-8b52-813ac45b5aca.settings.initialRows','\"4\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.e9bdc3b6-4eb6-4043-8b52-813ac45b5aca.settings.multiline','\"\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.e9bdc3b6-4eb6-4043-8b52-813ac45b5aca.settings.placeholder','\"\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.e9bdc3b6-4eb6-4043-8b52-813ac45b5aca.settings.uiMode','\"normal\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.e9bdc3b6-4eb6-4043-8b52-813ac45b5aca.translationKeyFormat','null'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.e9bdc3b6-4eb6-4043-8b52-813ac45b5aca.translationMethod','\"none\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.fields.e9bdc3b6-4eb6-4043-8b52-813ac45b5aca.type','\"craft\\\\fields\\\\PlainText\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.handle','\"titelBlock\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.name','\"TitelBlock\"'),('matrixBlockTypes.05fdf65f-1c94-44d5-bfc3-ec3b4181ccef.sortOrder','2'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.field','\"2f04b707-164a-4ec2-aaa7-c570fc70a429\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.0.fieldUid','\"94ac48cd-56f4-4a1e-b253-642e578a7ff8\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.0.instructions','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.0.label','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.0.required','false'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.0.tip','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.0.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.0.warning','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.0.width','100'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.1.fieldUid','\"d0f093c3-ec7a-485f-aa72-47422fbb714a\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.1.instructions','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.1.label','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.1.required','false'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.1.tip','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.1.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.1.warning','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.1.width','100'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.2.fieldUid','\"4b102177-d991-4a25-89dd-c901bfb85531\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.2.instructions','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.2.label','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.2.required','false'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.2.tip','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.2.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.2.warning','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.2.width','100'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.3.fieldUid','\"db31c084-1776-4689-80ab-a9d0f63570ac\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.3.instructions','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.3.label','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.3.required','false'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.3.tip','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.3.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.3.warning','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.3.width','100'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.4.fieldUid','\"e8093e48-1a6c-41ad-a551-ea40c773ad42\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.4.instructions','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.4.label','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.4.required','false'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.4.tip','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.4.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.4.warning','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.4.width','100'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.5.fieldUid','\"5fcf02d4-ca79-4845-b989-9b3206a156e1\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.5.instructions','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.5.label','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.5.required','false'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.5.tip','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.5.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.5.warning','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.elements.5.width','100'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.name','\"Content\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fieldLayouts.ca14a160-af33-4fff-9cd9-8c31e6a6593b.tabs.0.sortOrder','1'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.4b102177-d991-4a25-89dd-c901bfb85531.contentColumnType','\"string\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.4b102177-d991-4a25-89dd-c901bfb85531.fieldGroup','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.4b102177-d991-4a25-89dd-c901bfb85531.handle','\"thirdImage\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.4b102177-d991-4a25-89dd-c901bfb85531.instructions','\"\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.4b102177-d991-4a25-89dd-c901bfb85531.name','\"Drites Bild\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.4b102177-d991-4a25-89dd-c901bfb85531.searchable','false'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.4b102177-d991-4a25-89dd-c901bfb85531.settings.allowedKinds','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.4b102177-d991-4a25-89dd-c901bfb85531.settings.allowSelfRelations','false'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.4b102177-d991-4a25-89dd-c901bfb85531.settings.allowUploads','true'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.4b102177-d991-4a25-89dd-c901bfb85531.settings.defaultUploadLocationSource','\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.4b102177-d991-4a25-89dd-c901bfb85531.settings.defaultUploadLocationSubpath','\"\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.4b102177-d991-4a25-89dd-c901bfb85531.settings.limit','\"1\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.4b102177-d991-4a25-89dd-c901bfb85531.settings.localizeRelations','false'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.4b102177-d991-4a25-89dd-c901bfb85531.settings.previewMode','\"full\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.4b102177-d991-4a25-89dd-c901bfb85531.settings.restrictFiles','\"\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.4b102177-d991-4a25-89dd-c901bfb85531.settings.selectionLabel','\"\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.4b102177-d991-4a25-89dd-c901bfb85531.settings.showSiteMenu','true'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.4b102177-d991-4a25-89dd-c901bfb85531.settings.showUnpermittedFiles','false'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.4b102177-d991-4a25-89dd-c901bfb85531.settings.showUnpermittedVolumes','false'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.4b102177-d991-4a25-89dd-c901bfb85531.settings.singleUploadLocationSource','\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.4b102177-d991-4a25-89dd-c901bfb85531.settings.singleUploadLocationSubpath','\"\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.4b102177-d991-4a25-89dd-c901bfb85531.settings.source','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.4b102177-d991-4a25-89dd-c901bfb85531.settings.sources.0','\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.4b102177-d991-4a25-89dd-c901bfb85531.settings.targetSiteId','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.4b102177-d991-4a25-89dd-c901bfb85531.settings.useSingleFolder','false'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.4b102177-d991-4a25-89dd-c901bfb85531.settings.validateRelatedElements','false'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.4b102177-d991-4a25-89dd-c901bfb85531.settings.viewMode','\"list\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.4b102177-d991-4a25-89dd-c901bfb85531.translationKeyFormat','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.4b102177-d991-4a25-89dd-c901bfb85531.translationMethod','\"site\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.4b102177-d991-4a25-89dd-c901bfb85531.type','\"craft\\\\fields\\\\Assets\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.5fcf02d4-ca79-4845-b989-9b3206a156e1.contentColumnType','\"string\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.5fcf02d4-ca79-4845-b989-9b3206a156e1.fieldGroup','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.5fcf02d4-ca79-4845-b989-9b3206a156e1.handle','\"navigationImage\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.5fcf02d4-ca79-4845-b989-9b3206a156e1.instructions','\"\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.5fcf02d4-ca79-4845-b989-9b3206a156e1.name','\"Navigation Bild\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.5fcf02d4-ca79-4845-b989-9b3206a156e1.searchable','false'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.5fcf02d4-ca79-4845-b989-9b3206a156e1.settings.allowedKinds','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.5fcf02d4-ca79-4845-b989-9b3206a156e1.settings.allowSelfRelations','false'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.5fcf02d4-ca79-4845-b989-9b3206a156e1.settings.allowUploads','true'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.5fcf02d4-ca79-4845-b989-9b3206a156e1.settings.defaultUploadLocationSource','\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.5fcf02d4-ca79-4845-b989-9b3206a156e1.settings.defaultUploadLocationSubpath','\"\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.5fcf02d4-ca79-4845-b989-9b3206a156e1.settings.limit','\"1\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.5fcf02d4-ca79-4845-b989-9b3206a156e1.settings.localizeRelations','false'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.5fcf02d4-ca79-4845-b989-9b3206a156e1.settings.previewMode','\"full\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.5fcf02d4-ca79-4845-b989-9b3206a156e1.settings.restrictFiles','\"\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.5fcf02d4-ca79-4845-b989-9b3206a156e1.settings.selectionLabel','\"\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.5fcf02d4-ca79-4845-b989-9b3206a156e1.settings.showSiteMenu','false'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.5fcf02d4-ca79-4845-b989-9b3206a156e1.settings.showUnpermittedFiles','false'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.5fcf02d4-ca79-4845-b989-9b3206a156e1.settings.showUnpermittedVolumes','false'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.5fcf02d4-ca79-4845-b989-9b3206a156e1.settings.singleUploadLocationSource','\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.5fcf02d4-ca79-4845-b989-9b3206a156e1.settings.singleUploadLocationSubpath','\"\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.5fcf02d4-ca79-4845-b989-9b3206a156e1.settings.source','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.5fcf02d4-ca79-4845-b989-9b3206a156e1.settings.sources.0','\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.5fcf02d4-ca79-4845-b989-9b3206a156e1.settings.targetSiteId','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.5fcf02d4-ca79-4845-b989-9b3206a156e1.settings.useSingleFolder','false'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.5fcf02d4-ca79-4845-b989-9b3206a156e1.settings.validateRelatedElements','false'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.5fcf02d4-ca79-4845-b989-9b3206a156e1.settings.viewMode','\"list\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.5fcf02d4-ca79-4845-b989-9b3206a156e1.translationKeyFormat','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.5fcf02d4-ca79-4845-b989-9b3206a156e1.translationMethod','\"site\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.5fcf02d4-ca79-4845-b989-9b3206a156e1.type','\"craft\\\\fields\\\\Assets\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.94ac48cd-56f4-4a1e-b253-642e578a7ff8.contentColumnType','\"string\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.94ac48cd-56f4-4a1e-b253-642e578a7ff8.fieldGroup','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.94ac48cd-56f4-4a1e-b253-642e578a7ff8.handle','\"firstImage\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.94ac48cd-56f4-4a1e-b253-642e578a7ff8.instructions','\"\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.94ac48cd-56f4-4a1e-b253-642e578a7ff8.name','\"Erstes Bild\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.94ac48cd-56f4-4a1e-b253-642e578a7ff8.searchable','false'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.94ac48cd-56f4-4a1e-b253-642e578a7ff8.settings.allowedKinds','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.94ac48cd-56f4-4a1e-b253-642e578a7ff8.settings.allowSelfRelations','false'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.94ac48cd-56f4-4a1e-b253-642e578a7ff8.settings.allowUploads','true'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.94ac48cd-56f4-4a1e-b253-642e578a7ff8.settings.defaultUploadLocationSource','\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.94ac48cd-56f4-4a1e-b253-642e578a7ff8.settings.defaultUploadLocationSubpath','\"\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.94ac48cd-56f4-4a1e-b253-642e578a7ff8.settings.limit','\"1\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.94ac48cd-56f4-4a1e-b253-642e578a7ff8.settings.localizeRelations','false'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.94ac48cd-56f4-4a1e-b253-642e578a7ff8.settings.previewMode','\"full\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.94ac48cd-56f4-4a1e-b253-642e578a7ff8.settings.restrictFiles','\"\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.94ac48cd-56f4-4a1e-b253-642e578a7ff8.settings.selectionLabel','\"\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.94ac48cd-56f4-4a1e-b253-642e578a7ff8.settings.showSiteMenu','true'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.94ac48cd-56f4-4a1e-b253-642e578a7ff8.settings.showUnpermittedFiles','false'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.94ac48cd-56f4-4a1e-b253-642e578a7ff8.settings.showUnpermittedVolumes','false'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.94ac48cd-56f4-4a1e-b253-642e578a7ff8.settings.singleUploadLocationSource','\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.94ac48cd-56f4-4a1e-b253-642e578a7ff8.settings.singleUploadLocationSubpath','\"\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.94ac48cd-56f4-4a1e-b253-642e578a7ff8.settings.source','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.94ac48cd-56f4-4a1e-b253-642e578a7ff8.settings.sources.0','\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.94ac48cd-56f4-4a1e-b253-642e578a7ff8.settings.targetSiteId','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.94ac48cd-56f4-4a1e-b253-642e578a7ff8.settings.useSingleFolder','false'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.94ac48cd-56f4-4a1e-b253-642e578a7ff8.settings.validateRelatedElements','false'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.94ac48cd-56f4-4a1e-b253-642e578a7ff8.settings.viewMode','\"list\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.94ac48cd-56f4-4a1e-b253-642e578a7ff8.translationKeyFormat','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.94ac48cd-56f4-4a1e-b253-642e578a7ff8.translationMethod','\"site\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.94ac48cd-56f4-4a1e-b253-642e578a7ff8.type','\"craft\\\\fields\\\\Assets\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.d0f093c3-ec7a-485f-aa72-47422fbb714a.contentColumnType','\"string\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.d0f093c3-ec7a-485f-aa72-47422fbb714a.fieldGroup','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.d0f093c3-ec7a-485f-aa72-47422fbb714a.handle','\"secondImage\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.d0f093c3-ec7a-485f-aa72-47422fbb714a.instructions','\"\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.d0f093c3-ec7a-485f-aa72-47422fbb714a.name','\"Zweites Bild\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.d0f093c3-ec7a-485f-aa72-47422fbb714a.searchable','false'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.d0f093c3-ec7a-485f-aa72-47422fbb714a.settings.allowedKinds','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.d0f093c3-ec7a-485f-aa72-47422fbb714a.settings.allowSelfRelations','false'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.d0f093c3-ec7a-485f-aa72-47422fbb714a.settings.allowUploads','true'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.d0f093c3-ec7a-485f-aa72-47422fbb714a.settings.defaultUploadLocationSource','\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.d0f093c3-ec7a-485f-aa72-47422fbb714a.settings.defaultUploadLocationSubpath','\"\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.d0f093c3-ec7a-485f-aa72-47422fbb714a.settings.limit','\"1\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.d0f093c3-ec7a-485f-aa72-47422fbb714a.settings.localizeRelations','false'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.d0f093c3-ec7a-485f-aa72-47422fbb714a.settings.previewMode','\"full\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.d0f093c3-ec7a-485f-aa72-47422fbb714a.settings.restrictFiles','\"\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.d0f093c3-ec7a-485f-aa72-47422fbb714a.settings.selectionLabel','\"\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.d0f093c3-ec7a-485f-aa72-47422fbb714a.settings.showSiteMenu','true'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.d0f093c3-ec7a-485f-aa72-47422fbb714a.settings.showUnpermittedFiles','false'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.d0f093c3-ec7a-485f-aa72-47422fbb714a.settings.showUnpermittedVolumes','false'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.d0f093c3-ec7a-485f-aa72-47422fbb714a.settings.singleUploadLocationSource','\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.d0f093c3-ec7a-485f-aa72-47422fbb714a.settings.singleUploadLocationSubpath','\"\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.d0f093c3-ec7a-485f-aa72-47422fbb714a.settings.source','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.d0f093c3-ec7a-485f-aa72-47422fbb714a.settings.sources','\"*\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.d0f093c3-ec7a-485f-aa72-47422fbb714a.settings.targetSiteId','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.d0f093c3-ec7a-485f-aa72-47422fbb714a.settings.useSingleFolder','false'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.d0f093c3-ec7a-485f-aa72-47422fbb714a.settings.validateRelatedElements','false'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.d0f093c3-ec7a-485f-aa72-47422fbb714a.settings.viewMode','\"list\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.d0f093c3-ec7a-485f-aa72-47422fbb714a.translationKeyFormat','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.d0f093c3-ec7a-485f-aa72-47422fbb714a.translationMethod','\"site\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.d0f093c3-ec7a-485f-aa72-47422fbb714a.type','\"craft\\\\fields\\\\Assets\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.db31c084-1776-4689-80ab-a9d0f63570ac.contentColumnType','\"string(400)\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.db31c084-1776-4689-80ab-a9d0f63570ac.fieldGroup','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.db31c084-1776-4689-80ab-a9d0f63570ac.handle','\"smallTitle\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.db31c084-1776-4689-80ab-a9d0f63570ac.instructions','\"\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.db31c084-1776-4689-80ab-a9d0f63570ac.name','\"Kleiner Titel\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.db31c084-1776-4689-80ab-a9d0f63570ac.searchable','false'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.db31c084-1776-4689-80ab-a9d0f63570ac.settings.byteLimit','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.db31c084-1776-4689-80ab-a9d0f63570ac.settings.charLimit','100'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.db31c084-1776-4689-80ab-a9d0f63570ac.settings.code','\"\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.db31c084-1776-4689-80ab-a9d0f63570ac.settings.columnType','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.db31c084-1776-4689-80ab-a9d0f63570ac.settings.initialRows','\"4\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.db31c084-1776-4689-80ab-a9d0f63570ac.settings.multiline','\"\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.db31c084-1776-4689-80ab-a9d0f63570ac.settings.placeholder','\"\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.db31c084-1776-4689-80ab-a9d0f63570ac.settings.uiMode','\"normal\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.db31c084-1776-4689-80ab-a9d0f63570ac.translationKeyFormat','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.db31c084-1776-4689-80ab-a9d0f63570ac.translationMethod','\"none\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.db31c084-1776-4689-80ab-a9d0f63570ac.type','\"craft\\\\fields\\\\PlainText\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.e8093e48-1a6c-41ad-a551-ea40c773ad42.contentColumnType','\"text\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.e8093e48-1a6c-41ad-a551-ea40c773ad42.fieldGroup','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.e8093e48-1a6c-41ad-a551-ea40c773ad42.handle','\"biggTitle\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.e8093e48-1a6c-41ad-a551-ea40c773ad42.instructions','\"\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.e8093e48-1a6c-41ad-a551-ea40c773ad42.name','\"Grosser Titel\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.e8093e48-1a6c-41ad-a551-ea40c773ad42.searchable','false'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.e8093e48-1a6c-41ad-a551-ea40c773ad42.settings.availableTransforms','\"*\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.e8093e48-1a6c-41ad-a551-ea40c773ad42.settings.availableVolumes','\"*\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.e8093e48-1a6c-41ad-a551-ea40c773ad42.settings.cleanupHtml','true'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.e8093e48-1a6c-41ad-a551-ea40c773ad42.settings.columnType','\"text\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.e8093e48-1a6c-41ad-a551-ea40c773ad42.settings.configSelectionMode','\"choose\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.e8093e48-1a6c-41ad-a551-ea40c773ad42.settings.defaultTransform','\"\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.e8093e48-1a6c-41ad-a551-ea40c773ad42.settings.manualConfig','\"\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.e8093e48-1a6c-41ad-a551-ea40c773ad42.settings.purifierConfig','\"\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.e8093e48-1a6c-41ad-a551-ea40c773ad42.settings.purifyHtml','\"1\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.e8093e48-1a6c-41ad-a551-ea40c773ad42.settings.redactorConfig','\"\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.e8093e48-1a6c-41ad-a551-ea40c773ad42.settings.removeEmptyTags','\"1\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.e8093e48-1a6c-41ad-a551-ea40c773ad42.settings.removeInlineStyles','\"1\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.e8093e48-1a6c-41ad-a551-ea40c773ad42.settings.removeNbsp','\"1\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.e8093e48-1a6c-41ad-a551-ea40c773ad42.settings.showHtmlButtonForNonAdmins','\"\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.e8093e48-1a6c-41ad-a551-ea40c773ad42.settings.showUnpermittedFiles','false'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.e8093e48-1a6c-41ad-a551-ea40c773ad42.settings.showUnpermittedVolumes','false'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.e8093e48-1a6c-41ad-a551-ea40c773ad42.settings.uiMode','\"enlarged\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.e8093e48-1a6c-41ad-a551-ea40c773ad42.translationKeyFormat','null'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.e8093e48-1a6c-41ad-a551-ea40c773ad42.translationMethod','\"none\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.fields.e8093e48-1a6c-41ad-a551-ea40c773ad42.type','\"craft\\\\redactor\\\\Field\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.handle','\"sliderBlock\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.name','\"SliderBlock\"'),('matrixBlockTypes.24221623-0275-4d62-a202-d46fe0c06d09.sortOrder','5'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.field','\"2f04b707-164a-4ec2-aaa7-c570fc70a429\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fieldLayouts.9ef20be7-31a0-4833-b539-11f084cfac44.tabs.0.elements.0.fieldUid','\"4a513921-33e4-41ab-964f-76e8ab5a7ec1\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fieldLayouts.9ef20be7-31a0-4833-b539-11f084cfac44.tabs.0.elements.0.instructions','null'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fieldLayouts.9ef20be7-31a0-4833-b539-11f084cfac44.tabs.0.elements.0.label','null'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fieldLayouts.9ef20be7-31a0-4833-b539-11f084cfac44.tabs.0.elements.0.required','true'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fieldLayouts.9ef20be7-31a0-4833-b539-11f084cfac44.tabs.0.elements.0.tip','null'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fieldLayouts.9ef20be7-31a0-4833-b539-11f084cfac44.tabs.0.elements.0.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fieldLayouts.9ef20be7-31a0-4833-b539-11f084cfac44.tabs.0.elements.0.warning','null'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fieldLayouts.9ef20be7-31a0-4833-b539-11f084cfac44.tabs.0.elements.0.width','100'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fieldLayouts.9ef20be7-31a0-4833-b539-11f084cfac44.tabs.0.elements.1.fieldUid','\"7abc4ce2-f551-4614-bca6-c7cc4c8fda6a\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fieldLayouts.9ef20be7-31a0-4833-b539-11f084cfac44.tabs.0.elements.1.instructions','null'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fieldLayouts.9ef20be7-31a0-4833-b539-11f084cfac44.tabs.0.elements.1.label','null'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fieldLayouts.9ef20be7-31a0-4833-b539-11f084cfac44.tabs.0.elements.1.required','true'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fieldLayouts.9ef20be7-31a0-4833-b539-11f084cfac44.tabs.0.elements.1.tip','null'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fieldLayouts.9ef20be7-31a0-4833-b539-11f084cfac44.tabs.0.elements.1.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fieldLayouts.9ef20be7-31a0-4833-b539-11f084cfac44.tabs.0.elements.1.warning','null'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fieldLayouts.9ef20be7-31a0-4833-b539-11f084cfac44.tabs.0.elements.1.width','100'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fieldLayouts.9ef20be7-31a0-4833-b539-11f084cfac44.tabs.0.elements.2.fieldUid','\"ceba9e1b-8ab6-4012-8805-3b9581af35fc\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fieldLayouts.9ef20be7-31a0-4833-b539-11f084cfac44.tabs.0.elements.2.instructions','null'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fieldLayouts.9ef20be7-31a0-4833-b539-11f084cfac44.tabs.0.elements.2.label','null'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fieldLayouts.9ef20be7-31a0-4833-b539-11f084cfac44.tabs.0.elements.2.required','true'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fieldLayouts.9ef20be7-31a0-4833-b539-11f084cfac44.tabs.0.elements.2.tip','null'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fieldLayouts.9ef20be7-31a0-4833-b539-11f084cfac44.tabs.0.elements.2.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fieldLayouts.9ef20be7-31a0-4833-b539-11f084cfac44.tabs.0.elements.2.warning','null'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fieldLayouts.9ef20be7-31a0-4833-b539-11f084cfac44.tabs.0.elements.2.width','100'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fieldLayouts.9ef20be7-31a0-4833-b539-11f084cfac44.tabs.0.elements.3.fieldUid','\"f142b145-8349-4a9f-b9d5-6b1b815b6bb7\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fieldLayouts.9ef20be7-31a0-4833-b539-11f084cfac44.tabs.0.elements.3.instructions','null'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fieldLayouts.9ef20be7-31a0-4833-b539-11f084cfac44.tabs.0.elements.3.label','null'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fieldLayouts.9ef20be7-31a0-4833-b539-11f084cfac44.tabs.0.elements.3.required','true'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fieldLayouts.9ef20be7-31a0-4833-b539-11f084cfac44.tabs.0.elements.3.tip','null'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fieldLayouts.9ef20be7-31a0-4833-b539-11f084cfac44.tabs.0.elements.3.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fieldLayouts.9ef20be7-31a0-4833-b539-11f084cfac44.tabs.0.elements.3.warning','null'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fieldLayouts.9ef20be7-31a0-4833-b539-11f084cfac44.tabs.0.elements.3.width','100'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fieldLayouts.9ef20be7-31a0-4833-b539-11f084cfac44.tabs.0.name','\"Content\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fieldLayouts.9ef20be7-31a0-4833-b539-11f084cfac44.tabs.0.sortOrder','1'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.4a513921-33e4-41ab-964f-76e8ab5a7ec1.contentColumnType','\"string\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.4a513921-33e4-41ab-964f-76e8ab5a7ec1.fieldGroup','null'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.4a513921-33e4-41ab-964f-76e8ab5a7ec1.handle','\"navigationImage\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.4a513921-33e4-41ab-964f-76e8ab5a7ec1.instructions','\"\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.4a513921-33e4-41ab-964f-76e8ab5a7ec1.name','\"Navigation Bild\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.4a513921-33e4-41ab-964f-76e8ab5a7ec1.searchable','false'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.4a513921-33e4-41ab-964f-76e8ab5a7ec1.settings.allowedKinds','null'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.4a513921-33e4-41ab-964f-76e8ab5a7ec1.settings.allowSelfRelations','false'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.4a513921-33e4-41ab-964f-76e8ab5a7ec1.settings.allowUploads','true'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.4a513921-33e4-41ab-964f-76e8ab5a7ec1.settings.defaultUploadLocationSource','\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.4a513921-33e4-41ab-964f-76e8ab5a7ec1.settings.defaultUploadLocationSubpath','\"\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.4a513921-33e4-41ab-964f-76e8ab5a7ec1.settings.limit','\"1\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.4a513921-33e4-41ab-964f-76e8ab5a7ec1.settings.localizeRelations','false'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.4a513921-33e4-41ab-964f-76e8ab5a7ec1.settings.previewMode','\"full\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.4a513921-33e4-41ab-964f-76e8ab5a7ec1.settings.restrictFiles','\"\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.4a513921-33e4-41ab-964f-76e8ab5a7ec1.settings.selectionLabel','\"\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.4a513921-33e4-41ab-964f-76e8ab5a7ec1.settings.showSiteMenu','true'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.4a513921-33e4-41ab-964f-76e8ab5a7ec1.settings.showUnpermittedFiles','false'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.4a513921-33e4-41ab-964f-76e8ab5a7ec1.settings.showUnpermittedVolumes','false'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.4a513921-33e4-41ab-964f-76e8ab5a7ec1.settings.singleUploadLocationSource','\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.4a513921-33e4-41ab-964f-76e8ab5a7ec1.settings.singleUploadLocationSubpath','\"\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.4a513921-33e4-41ab-964f-76e8ab5a7ec1.settings.source','null'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.4a513921-33e4-41ab-964f-76e8ab5a7ec1.settings.sources.0','\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.4a513921-33e4-41ab-964f-76e8ab5a7ec1.settings.targetSiteId','null'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.4a513921-33e4-41ab-964f-76e8ab5a7ec1.settings.useSingleFolder','false'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.4a513921-33e4-41ab-964f-76e8ab5a7ec1.settings.validateRelatedElements','false'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.4a513921-33e4-41ab-964f-76e8ab5a7ec1.settings.viewMode','\"list\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.4a513921-33e4-41ab-964f-76e8ab5a7ec1.translationKeyFormat','null'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.4a513921-33e4-41ab-964f-76e8ab5a7ec1.translationMethod','\"site\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.4a513921-33e4-41ab-964f-76e8ab5a7ec1.type','\"craft\\\\fields\\\\Assets\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.7abc4ce2-f551-4614-bca6-c7cc4c8fda6a.contentColumnType','\"string(400)\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.7abc4ce2-f551-4614-bca6-c7cc4c8fda6a.fieldGroup','null'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.7abc4ce2-f551-4614-bca6-c7cc4c8fda6a.handle','\"smalltitel\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.7abc4ce2-f551-4614-bca6-c7cc4c8fda6a.instructions','\"\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.7abc4ce2-f551-4614-bca6-c7cc4c8fda6a.name','\"Kleiner Titel\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.7abc4ce2-f551-4614-bca6-c7cc4c8fda6a.searchable','false'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.7abc4ce2-f551-4614-bca6-c7cc4c8fda6a.settings.byteLimit','null'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.7abc4ce2-f551-4614-bca6-c7cc4c8fda6a.settings.charLimit','100'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.7abc4ce2-f551-4614-bca6-c7cc4c8fda6a.settings.code','\"\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.7abc4ce2-f551-4614-bca6-c7cc4c8fda6a.settings.columnType','null'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.7abc4ce2-f551-4614-bca6-c7cc4c8fda6a.settings.initialRows','\"4\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.7abc4ce2-f551-4614-bca6-c7cc4c8fda6a.settings.multiline','\"\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.7abc4ce2-f551-4614-bca6-c7cc4c8fda6a.settings.placeholder','\"\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.7abc4ce2-f551-4614-bca6-c7cc4c8fda6a.settings.uiMode','\"normal\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.7abc4ce2-f551-4614-bca6-c7cc4c8fda6a.translationKeyFormat','null'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.7abc4ce2-f551-4614-bca6-c7cc4c8fda6a.translationMethod','\"none\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.7abc4ce2-f551-4614-bca6-c7cc4c8fda6a.type','\"craft\\\\fields\\\\PlainText\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.ceba9e1b-8ab6-4012-8805-3b9581af35fc.contentColumnType','\"text\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.ceba9e1b-8ab6-4012-8805-3b9581af35fc.fieldGroup','null'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.ceba9e1b-8ab6-4012-8805-3b9581af35fc.handle','\"biggTitle\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.ceba9e1b-8ab6-4012-8805-3b9581af35fc.instructions','\"\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.ceba9e1b-8ab6-4012-8805-3b9581af35fc.name','\"Titel\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.ceba9e1b-8ab6-4012-8805-3b9581af35fc.searchable','false'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.ceba9e1b-8ab6-4012-8805-3b9581af35fc.settings.availableTransforms','\"*\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.ceba9e1b-8ab6-4012-8805-3b9581af35fc.settings.availableVolumes','\"*\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.ceba9e1b-8ab6-4012-8805-3b9581af35fc.settings.cleanupHtml','true'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.ceba9e1b-8ab6-4012-8805-3b9581af35fc.settings.columnType','\"text\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.ceba9e1b-8ab6-4012-8805-3b9581af35fc.settings.configSelectionMode','\"choose\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.ceba9e1b-8ab6-4012-8805-3b9581af35fc.settings.defaultTransform','\"\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.ceba9e1b-8ab6-4012-8805-3b9581af35fc.settings.manualConfig','\"\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.ceba9e1b-8ab6-4012-8805-3b9581af35fc.settings.purifierConfig','\"\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.ceba9e1b-8ab6-4012-8805-3b9581af35fc.settings.purifyHtml','\"1\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.ceba9e1b-8ab6-4012-8805-3b9581af35fc.settings.redactorConfig','\"\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.ceba9e1b-8ab6-4012-8805-3b9581af35fc.settings.removeEmptyTags','\"1\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.ceba9e1b-8ab6-4012-8805-3b9581af35fc.settings.removeInlineStyles','\"1\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.ceba9e1b-8ab6-4012-8805-3b9581af35fc.settings.removeNbsp','\"1\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.ceba9e1b-8ab6-4012-8805-3b9581af35fc.settings.showHtmlButtonForNonAdmins','\"\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.ceba9e1b-8ab6-4012-8805-3b9581af35fc.settings.showUnpermittedFiles','false'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.ceba9e1b-8ab6-4012-8805-3b9581af35fc.settings.showUnpermittedVolumes','false'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.ceba9e1b-8ab6-4012-8805-3b9581af35fc.settings.uiMode','\"enlarged\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.ceba9e1b-8ab6-4012-8805-3b9581af35fc.translationKeyFormat','null'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.ceba9e1b-8ab6-4012-8805-3b9581af35fc.translationMethod','\"none\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.ceba9e1b-8ab6-4012-8805-3b9581af35fc.type','\"craft\\\\redactor\\\\Field\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.f142b145-8349-4a9f-b9d5-6b1b815b6bb7.contentColumnType','\"string\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.f142b145-8349-4a9f-b9d5-6b1b815b6bb7.fieldGroup','null'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.f142b145-8349-4a9f-b9d5-6b1b815b6bb7.handle','\"image\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.f142b145-8349-4a9f-b9d5-6b1b815b6bb7.instructions','\"\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.f142b145-8349-4a9f-b9d5-6b1b815b6bb7.name','\"Bild\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.f142b145-8349-4a9f-b9d5-6b1b815b6bb7.searchable','false'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.f142b145-8349-4a9f-b9d5-6b1b815b6bb7.settings.allowedKinds','null'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.f142b145-8349-4a9f-b9d5-6b1b815b6bb7.settings.allowSelfRelations','false'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.f142b145-8349-4a9f-b9d5-6b1b815b6bb7.settings.allowUploads','true'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.f142b145-8349-4a9f-b9d5-6b1b815b6bb7.settings.defaultUploadLocationSource','\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.f142b145-8349-4a9f-b9d5-6b1b815b6bb7.settings.defaultUploadLocationSubpath','\"\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.f142b145-8349-4a9f-b9d5-6b1b815b6bb7.settings.limit','\"1\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.f142b145-8349-4a9f-b9d5-6b1b815b6bb7.settings.localizeRelations','false'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.f142b145-8349-4a9f-b9d5-6b1b815b6bb7.settings.previewMode','\"full\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.f142b145-8349-4a9f-b9d5-6b1b815b6bb7.settings.restrictFiles','\"\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.f142b145-8349-4a9f-b9d5-6b1b815b6bb7.settings.selectionLabel','\"\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.f142b145-8349-4a9f-b9d5-6b1b815b6bb7.settings.showSiteMenu','true'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.f142b145-8349-4a9f-b9d5-6b1b815b6bb7.settings.showUnpermittedFiles','false'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.f142b145-8349-4a9f-b9d5-6b1b815b6bb7.settings.showUnpermittedVolumes','false'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.f142b145-8349-4a9f-b9d5-6b1b815b6bb7.settings.singleUploadLocationSource','\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.f142b145-8349-4a9f-b9d5-6b1b815b6bb7.settings.singleUploadLocationSubpath','\"\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.f142b145-8349-4a9f-b9d5-6b1b815b6bb7.settings.source','null'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.f142b145-8349-4a9f-b9d5-6b1b815b6bb7.settings.sources.0','\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.f142b145-8349-4a9f-b9d5-6b1b815b6bb7.settings.targetSiteId','null'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.f142b145-8349-4a9f-b9d5-6b1b815b6bb7.settings.useSingleFolder','false'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.f142b145-8349-4a9f-b9d5-6b1b815b6bb7.settings.validateRelatedElements','false'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.f142b145-8349-4a9f-b9d5-6b1b815b6bb7.settings.viewMode','\"list\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.f142b145-8349-4a9f-b9d5-6b1b815b6bb7.translationKeyFormat','null'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.f142b145-8349-4a9f-b9d5-6b1b815b6bb7.translationMethod','\"site\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.fields.f142b145-8349-4a9f-b9d5-6b1b815b6bb7.type','\"craft\\\\fields\\\\Assets\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.handle','\"formularBlock\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.name','\"Formular Block\"'),('matrixBlockTypes.34079b35-1790-43d7-969a-07e64284d68b.sortOrder','4'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.field','\"2f04b707-164a-4ec2-aaa7-c570fc70a429\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fieldLayouts.38af1f6a-b125-4a33-9710-36d3f635fdea.tabs.0.elements.0.fieldUid','\"7aa2a284-f4bc-4917-989a-cf71fc62aef4\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fieldLayouts.38af1f6a-b125-4a33-9710-36d3f635fdea.tabs.0.elements.0.instructions','null'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fieldLayouts.38af1f6a-b125-4a33-9710-36d3f635fdea.tabs.0.elements.0.label','null'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fieldLayouts.38af1f6a-b125-4a33-9710-36d3f635fdea.tabs.0.elements.0.required','true'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fieldLayouts.38af1f6a-b125-4a33-9710-36d3f635fdea.tabs.0.elements.0.tip','null'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fieldLayouts.38af1f6a-b125-4a33-9710-36d3f635fdea.tabs.0.elements.0.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fieldLayouts.38af1f6a-b125-4a33-9710-36d3f635fdea.tabs.0.elements.0.warning','null'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fieldLayouts.38af1f6a-b125-4a33-9710-36d3f635fdea.tabs.0.elements.0.width','100'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fieldLayouts.38af1f6a-b125-4a33-9710-36d3f635fdea.tabs.0.elements.1.fieldUid','\"35e42c00-24b4-4493-a114-6100b1e82b46\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fieldLayouts.38af1f6a-b125-4a33-9710-36d3f635fdea.tabs.0.elements.1.instructions','null'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fieldLayouts.38af1f6a-b125-4a33-9710-36d3f635fdea.tabs.0.elements.1.label','null'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fieldLayouts.38af1f6a-b125-4a33-9710-36d3f635fdea.tabs.0.elements.1.required','true'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fieldLayouts.38af1f6a-b125-4a33-9710-36d3f635fdea.tabs.0.elements.1.tip','null'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fieldLayouts.38af1f6a-b125-4a33-9710-36d3f635fdea.tabs.0.elements.1.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fieldLayouts.38af1f6a-b125-4a33-9710-36d3f635fdea.tabs.0.elements.1.warning','null'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fieldLayouts.38af1f6a-b125-4a33-9710-36d3f635fdea.tabs.0.elements.1.width','100'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fieldLayouts.38af1f6a-b125-4a33-9710-36d3f635fdea.tabs.0.elements.2.fieldUid','\"d9014845-730e-4aab-be4f-d2a9ae7039a9\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fieldLayouts.38af1f6a-b125-4a33-9710-36d3f635fdea.tabs.0.elements.2.instructions','null'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fieldLayouts.38af1f6a-b125-4a33-9710-36d3f635fdea.tabs.0.elements.2.label','null'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fieldLayouts.38af1f6a-b125-4a33-9710-36d3f635fdea.tabs.0.elements.2.required','true'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fieldLayouts.38af1f6a-b125-4a33-9710-36d3f635fdea.tabs.0.elements.2.tip','null'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fieldLayouts.38af1f6a-b125-4a33-9710-36d3f635fdea.tabs.0.elements.2.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fieldLayouts.38af1f6a-b125-4a33-9710-36d3f635fdea.tabs.0.elements.2.warning','null'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fieldLayouts.38af1f6a-b125-4a33-9710-36d3f635fdea.tabs.0.elements.2.width','100'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fieldLayouts.38af1f6a-b125-4a33-9710-36d3f635fdea.tabs.0.name','\"Content\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fieldLayouts.38af1f6a-b125-4a33-9710-36d3f635fdea.tabs.0.sortOrder','1'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.35e42c00-24b4-4493-a114-6100b1e82b46.contentColumnType','\"text\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.35e42c00-24b4-4493-a114-6100b1e82b46.fieldGroup','null'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.35e42c00-24b4-4493-a114-6100b1e82b46.handle','\"text\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.35e42c00-24b4-4493-a114-6100b1e82b46.instructions','\"\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.35e42c00-24b4-4493-a114-6100b1e82b46.name','\"Text\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.35e42c00-24b4-4493-a114-6100b1e82b46.searchable','false'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.35e42c00-24b4-4493-a114-6100b1e82b46.settings.availableTransforms','\"*\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.35e42c00-24b4-4493-a114-6100b1e82b46.settings.availableVolumes','\"*\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.35e42c00-24b4-4493-a114-6100b1e82b46.settings.cleanupHtml','true'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.35e42c00-24b4-4493-a114-6100b1e82b46.settings.columnType','\"text\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.35e42c00-24b4-4493-a114-6100b1e82b46.settings.configSelectionMode','\"choose\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.35e42c00-24b4-4493-a114-6100b1e82b46.settings.defaultTransform','\"\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.35e42c00-24b4-4493-a114-6100b1e82b46.settings.manualConfig','\"\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.35e42c00-24b4-4493-a114-6100b1e82b46.settings.purifierConfig','\"\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.35e42c00-24b4-4493-a114-6100b1e82b46.settings.purifyHtml','\"1\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.35e42c00-24b4-4493-a114-6100b1e82b46.settings.redactorConfig','\"\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.35e42c00-24b4-4493-a114-6100b1e82b46.settings.removeEmptyTags','\"1\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.35e42c00-24b4-4493-a114-6100b1e82b46.settings.removeInlineStyles','\"1\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.35e42c00-24b4-4493-a114-6100b1e82b46.settings.removeNbsp','\"1\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.35e42c00-24b4-4493-a114-6100b1e82b46.settings.showHtmlButtonForNonAdmins','\"\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.35e42c00-24b4-4493-a114-6100b1e82b46.settings.showUnpermittedFiles','false'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.35e42c00-24b4-4493-a114-6100b1e82b46.settings.showUnpermittedVolumes','false'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.35e42c00-24b4-4493-a114-6100b1e82b46.settings.uiMode','\"enlarged\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.35e42c00-24b4-4493-a114-6100b1e82b46.translationKeyFormat','null'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.35e42c00-24b4-4493-a114-6100b1e82b46.translationMethod','\"none\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.35e42c00-24b4-4493-a114-6100b1e82b46.type','\"craft\\\\redactor\\\\Field\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.7aa2a284-f4bc-4917-989a-cf71fc62aef4.contentColumnType','\"string(400)\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.7aa2a284-f4bc-4917-989a-cf71fc62aef4.fieldGroup','null'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.7aa2a284-f4bc-4917-989a-cf71fc62aef4.handle','\"titel\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.7aa2a284-f4bc-4917-989a-cf71fc62aef4.instructions','\"\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.7aa2a284-f4bc-4917-989a-cf71fc62aef4.name','\"Titel\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.7aa2a284-f4bc-4917-989a-cf71fc62aef4.searchable','false'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.7aa2a284-f4bc-4917-989a-cf71fc62aef4.settings.byteLimit','null'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.7aa2a284-f4bc-4917-989a-cf71fc62aef4.settings.charLimit','100'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.7aa2a284-f4bc-4917-989a-cf71fc62aef4.settings.code','\"\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.7aa2a284-f4bc-4917-989a-cf71fc62aef4.settings.columnType','null'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.7aa2a284-f4bc-4917-989a-cf71fc62aef4.settings.initialRows','\"4\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.7aa2a284-f4bc-4917-989a-cf71fc62aef4.settings.multiline','\"\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.7aa2a284-f4bc-4917-989a-cf71fc62aef4.settings.placeholder','\"\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.7aa2a284-f4bc-4917-989a-cf71fc62aef4.settings.uiMode','\"normal\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.7aa2a284-f4bc-4917-989a-cf71fc62aef4.translationKeyFormat','null'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.7aa2a284-f4bc-4917-989a-cf71fc62aef4.translationMethod','\"none\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.7aa2a284-f4bc-4917-989a-cf71fc62aef4.type','\"craft\\\\fields\\\\PlainText\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.d9014845-730e-4aab-be4f-d2a9ae7039a9.contentColumnType','\"string\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.d9014845-730e-4aab-be4f-d2a9ae7039a9.fieldGroup','null'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.d9014845-730e-4aab-be4f-d2a9ae7039a9.handle','\"image\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.d9014845-730e-4aab-be4f-d2a9ae7039a9.instructions','\"\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.d9014845-730e-4aab-be4f-d2a9ae7039a9.name','\"Bild\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.d9014845-730e-4aab-be4f-d2a9ae7039a9.searchable','false'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.d9014845-730e-4aab-be4f-d2a9ae7039a9.settings.allowedKinds','null'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.d9014845-730e-4aab-be4f-d2a9ae7039a9.settings.allowSelfRelations','false'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.d9014845-730e-4aab-be4f-d2a9ae7039a9.settings.allowUploads','true'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.d9014845-730e-4aab-be4f-d2a9ae7039a9.settings.defaultUploadLocationSource','\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.d9014845-730e-4aab-be4f-d2a9ae7039a9.settings.defaultUploadLocationSubpath','\"\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.d9014845-730e-4aab-be4f-d2a9ae7039a9.settings.limit','\"1\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.d9014845-730e-4aab-be4f-d2a9ae7039a9.settings.localizeRelations','false'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.d9014845-730e-4aab-be4f-d2a9ae7039a9.settings.previewMode','\"full\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.d9014845-730e-4aab-be4f-d2a9ae7039a9.settings.restrictFiles','\"\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.d9014845-730e-4aab-be4f-d2a9ae7039a9.settings.selectionLabel','\"\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.d9014845-730e-4aab-be4f-d2a9ae7039a9.settings.showSiteMenu','true'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.d9014845-730e-4aab-be4f-d2a9ae7039a9.settings.showUnpermittedFiles','false'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.d9014845-730e-4aab-be4f-d2a9ae7039a9.settings.showUnpermittedVolumes','false'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.d9014845-730e-4aab-be4f-d2a9ae7039a9.settings.singleUploadLocationSource','\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.d9014845-730e-4aab-be4f-d2a9ae7039a9.settings.singleUploadLocationSubpath','\"\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.d9014845-730e-4aab-be4f-d2a9ae7039a9.settings.source','null'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.d9014845-730e-4aab-be4f-d2a9ae7039a9.settings.sources.0','\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.d9014845-730e-4aab-be4f-d2a9ae7039a9.settings.targetSiteId','null'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.d9014845-730e-4aab-be4f-d2a9ae7039a9.settings.useSingleFolder','false'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.d9014845-730e-4aab-be4f-d2a9ae7039a9.settings.validateRelatedElements','false'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.d9014845-730e-4aab-be4f-d2a9ae7039a9.settings.viewMode','\"list\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.d9014845-730e-4aab-be4f-d2a9ae7039a9.translationKeyFormat','null'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.d9014845-730e-4aab-be4f-d2a9ae7039a9.translationMethod','\"site\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.fields.d9014845-730e-4aab-be4f-d2a9ae7039a9.type','\"craft\\\\fields\\\\Assets\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.handle','\"newsBlock\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.name','\"News Block\"'),('matrixBlockTypes.3941620b-2988-47eb-8e03-c0d5596d2850.sortOrder','3'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.field','\"2f04b707-164a-4ec2-aaa7-c570fc70a429\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.0.fieldUid','\"651a4895-d59b-4cef-9d66-fb958fafdcd1\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.0.instructions','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.0.label','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.0.required','true'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.0.tip','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.0.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.0.warning','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.0.width','100'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.1.fieldUid','\"cf74e9d8-ee9a-478e-b286-029335d149cf\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.1.instructions','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.1.label','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.1.required','false'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.1.tip','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.1.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.1.warning','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.1.width','100'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.2.fieldUid','\"99d95261-96b1-4f80-b7f3-48b820ff449f\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.2.instructions','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.2.label','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.2.required','false'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.2.tip','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.2.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.2.warning','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.2.width','100'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.3.fieldUid','\"f111dd8f-f1ee-4aa9-97e2-a1d0941b9dc4\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.3.instructions','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.3.label','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.3.required','true'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.3.tip','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.3.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.3.warning','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.3.width','100'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.4.fieldUid','\"bf2e5a22-00a1-4932-b368-d9d4bb0bae6e\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.4.instructions','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.4.label','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.4.required','false'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.4.tip','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.4.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.4.warning','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.4.width','100'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.5.fieldUid','\"4be8fe22-9a7d-4562-bd37-a6c2f75e4fc8\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.5.instructions','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.5.label','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.5.required','false'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.5.tip','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.5.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.5.warning','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.elements.5.width','100'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.name','\"Content\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fieldLayouts.7027674a-06cc-43d1-8272-3bc9cabb1fed.tabs.0.sortOrder','1'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.4be8fe22-9a7d-4562-bd37-a6c2f75e4fc8.contentColumnType','\"boolean\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.4be8fe22-9a7d-4562-bd37-a6c2f75e4fc8.fieldGroup','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.4be8fe22-9a7d-4562-bd37-a6c2f75e4fc8.handle','\"rightLeft\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.4be8fe22-9a7d-4562-bd37-a6c2f75e4fc8.instructions','\"\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.4be8fe22-9a7d-4562-bd37-a6c2f75e4fc8.name','\"Block Position\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.4be8fe22-9a7d-4562-bd37-a6c2f75e4fc8.searchable','false'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.4be8fe22-9a7d-4562-bd37-a6c2f75e4fc8.settings.default','false'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.4be8fe22-9a7d-4562-bd37-a6c2f75e4fc8.settings.offLabel','\"links\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.4be8fe22-9a7d-4562-bd37-a6c2f75e4fc8.settings.onLabel','\"rechts\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.4be8fe22-9a7d-4562-bd37-a6c2f75e4fc8.translationKeyFormat','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.4be8fe22-9a7d-4562-bd37-a6c2f75e4fc8.translationMethod','\"none\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.4be8fe22-9a7d-4562-bd37-a6c2f75e4fc8.type','\"craft\\\\fields\\\\Lightswitch\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.651a4895-d59b-4cef-9d66-fb958fafdcd1.contentColumnType','\"string\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.651a4895-d59b-4cef-9d66-fb958fafdcd1.fieldGroup','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.651a4895-d59b-4cef-9d66-fb958fafdcd1.handle','\"navigationImage\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.651a4895-d59b-4cef-9d66-fb958fafdcd1.instructions','\"\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.651a4895-d59b-4cef-9d66-fb958fafdcd1.name','\"Navigation Bild\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.651a4895-d59b-4cef-9d66-fb958fafdcd1.searchable','false'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.651a4895-d59b-4cef-9d66-fb958fafdcd1.settings.allowedKinds','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.651a4895-d59b-4cef-9d66-fb958fafdcd1.settings.allowSelfRelations','false'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.651a4895-d59b-4cef-9d66-fb958fafdcd1.settings.allowUploads','true'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.651a4895-d59b-4cef-9d66-fb958fafdcd1.settings.defaultUploadLocationSource','\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.651a4895-d59b-4cef-9d66-fb958fafdcd1.settings.defaultUploadLocationSubpath','\"\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.651a4895-d59b-4cef-9d66-fb958fafdcd1.settings.limit','\"1\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.651a4895-d59b-4cef-9d66-fb958fafdcd1.settings.localizeRelations','false'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.651a4895-d59b-4cef-9d66-fb958fafdcd1.settings.previewMode','\"full\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.651a4895-d59b-4cef-9d66-fb958fafdcd1.settings.restrictFiles','\"\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.651a4895-d59b-4cef-9d66-fb958fafdcd1.settings.selectionLabel','\"\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.651a4895-d59b-4cef-9d66-fb958fafdcd1.settings.showSiteMenu','true'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.651a4895-d59b-4cef-9d66-fb958fafdcd1.settings.showUnpermittedFiles','false'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.651a4895-d59b-4cef-9d66-fb958fafdcd1.settings.showUnpermittedVolumes','false'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.651a4895-d59b-4cef-9d66-fb958fafdcd1.settings.singleUploadLocationSource','\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.651a4895-d59b-4cef-9d66-fb958fafdcd1.settings.singleUploadLocationSubpath','\"\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.651a4895-d59b-4cef-9d66-fb958fafdcd1.settings.source','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.651a4895-d59b-4cef-9d66-fb958fafdcd1.settings.sources.0','\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.651a4895-d59b-4cef-9d66-fb958fafdcd1.settings.targetSiteId','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.651a4895-d59b-4cef-9d66-fb958fafdcd1.settings.useSingleFolder','false'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.651a4895-d59b-4cef-9d66-fb958fafdcd1.settings.validateRelatedElements','false'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.651a4895-d59b-4cef-9d66-fb958fafdcd1.settings.viewMode','\"list\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.651a4895-d59b-4cef-9d66-fb958fafdcd1.translationKeyFormat','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.651a4895-d59b-4cef-9d66-fb958fafdcd1.translationMethod','\"site\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.651a4895-d59b-4cef-9d66-fb958fafdcd1.type','\"craft\\\\fields\\\\Assets\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.99d95261-96b1-4f80-b7f3-48b820ff449f.contentColumnType','\"text\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.99d95261-96b1-4f80-b7f3-48b820ff449f.fieldGroup','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.99d95261-96b1-4f80-b7f3-48b820ff449f.handle','\"titel\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.99d95261-96b1-4f80-b7f3-48b820ff449f.instructions','\"\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.99d95261-96b1-4f80-b7f3-48b820ff449f.name','\"Titel\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.99d95261-96b1-4f80-b7f3-48b820ff449f.searchable','false'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.99d95261-96b1-4f80-b7f3-48b820ff449f.settings.availableTransforms','\"*\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.99d95261-96b1-4f80-b7f3-48b820ff449f.settings.availableVolumes','\"*\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.99d95261-96b1-4f80-b7f3-48b820ff449f.settings.cleanupHtml','true'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.99d95261-96b1-4f80-b7f3-48b820ff449f.settings.columnType','\"text\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.99d95261-96b1-4f80-b7f3-48b820ff449f.settings.configSelectionMode','\"choose\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.99d95261-96b1-4f80-b7f3-48b820ff449f.settings.defaultTransform','\"\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.99d95261-96b1-4f80-b7f3-48b820ff449f.settings.manualConfig','\"\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.99d95261-96b1-4f80-b7f3-48b820ff449f.settings.purifierConfig','\"\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.99d95261-96b1-4f80-b7f3-48b820ff449f.settings.purifyHtml','\"1\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.99d95261-96b1-4f80-b7f3-48b820ff449f.settings.redactorConfig','\"\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.99d95261-96b1-4f80-b7f3-48b820ff449f.settings.removeEmptyTags','\"1\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.99d95261-96b1-4f80-b7f3-48b820ff449f.settings.removeInlineStyles','\"1\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.99d95261-96b1-4f80-b7f3-48b820ff449f.settings.removeNbsp','\"1\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.99d95261-96b1-4f80-b7f3-48b820ff449f.settings.showHtmlButtonForNonAdmins','\"\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.99d95261-96b1-4f80-b7f3-48b820ff449f.settings.showUnpermittedFiles','false'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.99d95261-96b1-4f80-b7f3-48b820ff449f.settings.showUnpermittedVolumes','false'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.99d95261-96b1-4f80-b7f3-48b820ff449f.settings.uiMode','\"enlarged\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.99d95261-96b1-4f80-b7f3-48b820ff449f.translationKeyFormat','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.99d95261-96b1-4f80-b7f3-48b820ff449f.translationMethod','\"none\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.99d95261-96b1-4f80-b7f3-48b820ff449f.type','\"craft\\\\redactor\\\\Field\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.bf2e5a22-00a1-4932-b368-d9d4bb0bae6e.contentColumnType','\"string\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.bf2e5a22-00a1-4932-b368-d9d4bb0bae6e.fieldGroup','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.bf2e5a22-00a1-4932-b368-d9d4bb0bae6e.handle','\"image\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.bf2e5a22-00a1-4932-b368-d9d4bb0bae6e.instructions','\"\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.bf2e5a22-00a1-4932-b368-d9d4bb0bae6e.name','\"Bild\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.bf2e5a22-00a1-4932-b368-d9d4bb0bae6e.searchable','false'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.bf2e5a22-00a1-4932-b368-d9d4bb0bae6e.settings.allowedKinds','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.bf2e5a22-00a1-4932-b368-d9d4bb0bae6e.settings.allowSelfRelations','false'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.bf2e5a22-00a1-4932-b368-d9d4bb0bae6e.settings.allowUploads','true'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.bf2e5a22-00a1-4932-b368-d9d4bb0bae6e.settings.defaultUploadLocationSource','\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.bf2e5a22-00a1-4932-b368-d9d4bb0bae6e.settings.defaultUploadLocationSubpath','\"\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.bf2e5a22-00a1-4932-b368-d9d4bb0bae6e.settings.limit','\"1\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.bf2e5a22-00a1-4932-b368-d9d4bb0bae6e.settings.localizeRelations','false'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.bf2e5a22-00a1-4932-b368-d9d4bb0bae6e.settings.previewMode','\"full\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.bf2e5a22-00a1-4932-b368-d9d4bb0bae6e.settings.restrictFiles','\"\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.bf2e5a22-00a1-4932-b368-d9d4bb0bae6e.settings.selectionLabel','\"\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.bf2e5a22-00a1-4932-b368-d9d4bb0bae6e.settings.showSiteMenu','true'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.bf2e5a22-00a1-4932-b368-d9d4bb0bae6e.settings.showUnpermittedFiles','false'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.bf2e5a22-00a1-4932-b368-d9d4bb0bae6e.settings.showUnpermittedVolumes','false'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.bf2e5a22-00a1-4932-b368-d9d4bb0bae6e.settings.singleUploadLocationSource','\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.bf2e5a22-00a1-4932-b368-d9d4bb0bae6e.settings.singleUploadLocationSubpath','\"\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.bf2e5a22-00a1-4932-b368-d9d4bb0bae6e.settings.source','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.bf2e5a22-00a1-4932-b368-d9d4bb0bae6e.settings.sources.0','\"volume:9ab8fbf3-1f45-4e90-ae46-4541be742a4c\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.bf2e5a22-00a1-4932-b368-d9d4bb0bae6e.settings.targetSiteId','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.bf2e5a22-00a1-4932-b368-d9d4bb0bae6e.settings.useSingleFolder','false'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.bf2e5a22-00a1-4932-b368-d9d4bb0bae6e.settings.validateRelatedElements','false'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.bf2e5a22-00a1-4932-b368-d9d4bb0bae6e.settings.viewMode','\"list\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.bf2e5a22-00a1-4932-b368-d9d4bb0bae6e.translationKeyFormat','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.bf2e5a22-00a1-4932-b368-d9d4bb0bae6e.translationMethod','\"site\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.bf2e5a22-00a1-4932-b368-d9d4bb0bae6e.type','\"craft\\\\fields\\\\Assets\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.cf74e9d8-ee9a-478e-b286-029335d149cf.contentColumnType','\"string(400)\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.cf74e9d8-ee9a-478e-b286-029335d149cf.fieldGroup','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.cf74e9d8-ee9a-478e-b286-029335d149cf.handle','\"smalltitel\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.cf74e9d8-ee9a-478e-b286-029335d149cf.instructions','\"\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.cf74e9d8-ee9a-478e-b286-029335d149cf.name','\"Kleiner Titel\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.cf74e9d8-ee9a-478e-b286-029335d149cf.searchable','false'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.cf74e9d8-ee9a-478e-b286-029335d149cf.settings.byteLimit','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.cf74e9d8-ee9a-478e-b286-029335d149cf.settings.charLimit','100'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.cf74e9d8-ee9a-478e-b286-029335d149cf.settings.code','\"\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.cf74e9d8-ee9a-478e-b286-029335d149cf.settings.columnType','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.cf74e9d8-ee9a-478e-b286-029335d149cf.settings.initialRows','\"4\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.cf74e9d8-ee9a-478e-b286-029335d149cf.settings.multiline','\"\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.cf74e9d8-ee9a-478e-b286-029335d149cf.settings.placeholder','\"\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.cf74e9d8-ee9a-478e-b286-029335d149cf.settings.uiMode','\"normal\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.cf74e9d8-ee9a-478e-b286-029335d149cf.translationKeyFormat','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.cf74e9d8-ee9a-478e-b286-029335d149cf.translationMethod','\"none\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.cf74e9d8-ee9a-478e-b286-029335d149cf.type','\"craft\\\\fields\\\\PlainText\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.f111dd8f-f1ee-4aa9-97e2-a1d0941b9dc4.contentColumnType','\"text\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.f111dd8f-f1ee-4aa9-97e2-a1d0941b9dc4.fieldGroup','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.f111dd8f-f1ee-4aa9-97e2-a1d0941b9dc4.handle','\"text\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.f111dd8f-f1ee-4aa9-97e2-a1d0941b9dc4.instructions','\"\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.f111dd8f-f1ee-4aa9-97e2-a1d0941b9dc4.name','\"Text\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.f111dd8f-f1ee-4aa9-97e2-a1d0941b9dc4.searchable','false'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.f111dd8f-f1ee-4aa9-97e2-a1d0941b9dc4.settings.availableTransforms','\"*\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.f111dd8f-f1ee-4aa9-97e2-a1d0941b9dc4.settings.availableVolumes','\"*\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.f111dd8f-f1ee-4aa9-97e2-a1d0941b9dc4.settings.cleanupHtml','true'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.f111dd8f-f1ee-4aa9-97e2-a1d0941b9dc4.settings.columnType','\"text\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.f111dd8f-f1ee-4aa9-97e2-a1d0941b9dc4.settings.configSelectionMode','\"choose\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.f111dd8f-f1ee-4aa9-97e2-a1d0941b9dc4.settings.defaultTransform','\"\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.f111dd8f-f1ee-4aa9-97e2-a1d0941b9dc4.settings.manualConfig','\"\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.f111dd8f-f1ee-4aa9-97e2-a1d0941b9dc4.settings.purifierConfig','\"\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.f111dd8f-f1ee-4aa9-97e2-a1d0941b9dc4.settings.purifyHtml','\"1\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.f111dd8f-f1ee-4aa9-97e2-a1d0941b9dc4.settings.redactorConfig','\"Simple.json\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.f111dd8f-f1ee-4aa9-97e2-a1d0941b9dc4.settings.removeEmptyTags','\"1\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.f111dd8f-f1ee-4aa9-97e2-a1d0941b9dc4.settings.removeInlineStyles','\"1\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.f111dd8f-f1ee-4aa9-97e2-a1d0941b9dc4.settings.removeNbsp','\"1\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.f111dd8f-f1ee-4aa9-97e2-a1d0941b9dc4.settings.showHtmlButtonForNonAdmins','\"\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.f111dd8f-f1ee-4aa9-97e2-a1d0941b9dc4.settings.showUnpermittedFiles','false'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.f111dd8f-f1ee-4aa9-97e2-a1d0941b9dc4.settings.showUnpermittedVolumes','false'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.f111dd8f-f1ee-4aa9-97e2-a1d0941b9dc4.settings.uiMode','\"enlarged\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.f111dd8f-f1ee-4aa9-97e2-a1d0941b9dc4.translationKeyFormat','null'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.f111dd8f-f1ee-4aa9-97e2-a1d0941b9dc4.translationMethod','\"none\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.fields.f111dd8f-f1ee-4aa9-97e2-a1d0941b9dc4.type','\"craft\\\\redactor\\\\Field\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.handle','\"textImageTitleBlock\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.name','\"Titel, Text, Bild Block\"'),('matrixBlockTypes.5676be3e-cf5d-4812-b000-f654fc171776.sortOrder','1'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.field','\"0a46a4ed-9556-4b96-84fa-d8eda3ade2ba\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.0.fieldUid','\"daa6ae63-4514-429c-92ae-3662b6c0c8b7\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.0.instructions','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.0.label','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.0.required','false'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.0.tip','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.0.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.0.warning','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.0.width','100'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.1.fieldUid','\"5c08b56b-7d90-4196-b7dc-3ce429006d54\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.1.instructions','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.1.label','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.1.required','false'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.1.tip','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.1.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.1.warning','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.1.width','100'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.2.fieldUid','\"0b9f6e5a-e94a-432f-8bc5-5e60f076bf1d\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.2.instructions','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.2.label','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.2.required','false'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.2.tip','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.2.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.2.warning','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.2.width','100'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.3.fieldUid','\"7ce298ad-35b8-47ee-8ed9-7ac00e81eda3\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.3.instructions','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.3.label','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.3.required','false'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.3.tip','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.3.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.3.warning','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.3.width','100'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.4.fieldUid','\"3b6ac2b4-44b5-4dd3-a94f-41d5950b6385\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.4.instructions','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.4.label','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.4.required','false'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.4.tip','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.4.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.4.warning','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.4.width','100'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.5.fieldUid','\"53a6f531-cd7b-481b-ac60-9ab3593a547a\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.5.instructions','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.5.label','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.5.required','false'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.5.tip','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.5.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.5.warning','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.elements.5.width','100'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.name','\"Content\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fieldLayouts.c17e9d4b-19f4-4aaf-93b1-d698a9083473.tabs.0.sortOrder','1'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.0b9f6e5a-e94a-432f-8bc5-5e60f076bf1d.contentColumnType','\"text\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.0b9f6e5a-e94a-432f-8bc5-5e60f076bf1d.fieldGroup','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.0b9f6e5a-e94a-432f-8bc5-5e60f076bf1d.handle','\"plz\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.0b9f6e5a-e94a-432f-8bc5-5e60f076bf1d.instructions','\"\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.0b9f6e5a-e94a-432f-8bc5-5e60f076bf1d.name','\"Postleitzahl\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.0b9f6e5a-e94a-432f-8bc5-5e60f076bf1d.searchable','false'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.0b9f6e5a-e94a-432f-8bc5-5e60f076bf1d.settings.byteLimit','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.0b9f6e5a-e94a-432f-8bc5-5e60f076bf1d.settings.charLimit','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.0b9f6e5a-e94a-432f-8bc5-5e60f076bf1d.settings.code','\"\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.0b9f6e5a-e94a-432f-8bc5-5e60f076bf1d.settings.columnType','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.0b9f6e5a-e94a-432f-8bc5-5e60f076bf1d.settings.initialRows','\"4\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.0b9f6e5a-e94a-432f-8bc5-5e60f076bf1d.settings.multiline','\"\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.0b9f6e5a-e94a-432f-8bc5-5e60f076bf1d.settings.placeholder','\"\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.0b9f6e5a-e94a-432f-8bc5-5e60f076bf1d.settings.uiMode','\"normal\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.0b9f6e5a-e94a-432f-8bc5-5e60f076bf1d.translationKeyFormat','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.0b9f6e5a-e94a-432f-8bc5-5e60f076bf1d.translationMethod','\"none\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.0b9f6e5a-e94a-432f-8bc5-5e60f076bf1d.type','\"craft\\\\fields\\\\PlainText\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.3b6ac2b4-44b5-4dd3-a94f-41d5950b6385.contentColumnType','\"text\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.3b6ac2b4-44b5-4dd3-a94f-41d5950b6385.fieldGroup','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.3b6ac2b4-44b5-4dd3-a94f-41d5950b6385.handle','\"phone\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.3b6ac2b4-44b5-4dd3-a94f-41d5950b6385.instructions','\"\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.3b6ac2b4-44b5-4dd3-a94f-41d5950b6385.name','\"Telefon\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.3b6ac2b4-44b5-4dd3-a94f-41d5950b6385.searchable','false'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.3b6ac2b4-44b5-4dd3-a94f-41d5950b6385.settings.byteLimit','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.3b6ac2b4-44b5-4dd3-a94f-41d5950b6385.settings.charLimit','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.3b6ac2b4-44b5-4dd3-a94f-41d5950b6385.settings.code','\"\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.3b6ac2b4-44b5-4dd3-a94f-41d5950b6385.settings.columnType','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.3b6ac2b4-44b5-4dd3-a94f-41d5950b6385.settings.initialRows','\"4\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.3b6ac2b4-44b5-4dd3-a94f-41d5950b6385.settings.multiline','\"\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.3b6ac2b4-44b5-4dd3-a94f-41d5950b6385.settings.placeholder','\"\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.3b6ac2b4-44b5-4dd3-a94f-41d5950b6385.settings.uiMode','\"normal\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.3b6ac2b4-44b5-4dd3-a94f-41d5950b6385.translationKeyFormat','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.3b6ac2b4-44b5-4dd3-a94f-41d5950b6385.translationMethod','\"none\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.3b6ac2b4-44b5-4dd3-a94f-41d5950b6385.type','\"craft\\\\fields\\\\PlainText\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.53a6f531-cd7b-481b-ac60-9ab3593a547a.contentColumnType','\"text\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.53a6f531-cd7b-481b-ac60-9ab3593a547a.fieldGroup','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.53a6f531-cd7b-481b-ac60-9ab3593a547a.handle','\"mail\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.53a6f531-cd7b-481b-ac60-9ab3593a547a.instructions','\"\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.53a6f531-cd7b-481b-ac60-9ab3593a547a.name','\"Email\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.53a6f531-cd7b-481b-ac60-9ab3593a547a.searchable','false'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.53a6f531-cd7b-481b-ac60-9ab3593a547a.settings.byteLimit','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.53a6f531-cd7b-481b-ac60-9ab3593a547a.settings.charLimit','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.53a6f531-cd7b-481b-ac60-9ab3593a547a.settings.code','\"\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.53a6f531-cd7b-481b-ac60-9ab3593a547a.settings.columnType','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.53a6f531-cd7b-481b-ac60-9ab3593a547a.settings.initialRows','\"4\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.53a6f531-cd7b-481b-ac60-9ab3593a547a.settings.multiline','\"\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.53a6f531-cd7b-481b-ac60-9ab3593a547a.settings.placeholder','\"\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.53a6f531-cd7b-481b-ac60-9ab3593a547a.settings.uiMode','\"normal\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.53a6f531-cd7b-481b-ac60-9ab3593a547a.translationKeyFormat','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.53a6f531-cd7b-481b-ac60-9ab3593a547a.translationMethod','\"none\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.53a6f531-cd7b-481b-ac60-9ab3593a547a.type','\"craft\\\\fields\\\\PlainText\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.5c08b56b-7d90-4196-b7dc-3ce429006d54.contentColumnType','\"text\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.5c08b56b-7d90-4196-b7dc-3ce429006d54.fieldGroup','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.5c08b56b-7d90-4196-b7dc-3ce429006d54.handle','\"street\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.5c08b56b-7d90-4196-b7dc-3ce429006d54.instructions','\"\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.5c08b56b-7d90-4196-b7dc-3ce429006d54.name','\"Strasse, Nr\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.5c08b56b-7d90-4196-b7dc-3ce429006d54.searchable','false'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.5c08b56b-7d90-4196-b7dc-3ce429006d54.settings.byteLimit','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.5c08b56b-7d90-4196-b7dc-3ce429006d54.settings.charLimit','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.5c08b56b-7d90-4196-b7dc-3ce429006d54.settings.code','\"\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.5c08b56b-7d90-4196-b7dc-3ce429006d54.settings.columnType','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.5c08b56b-7d90-4196-b7dc-3ce429006d54.settings.initialRows','\"4\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.5c08b56b-7d90-4196-b7dc-3ce429006d54.settings.multiline','\"\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.5c08b56b-7d90-4196-b7dc-3ce429006d54.settings.placeholder','\"\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.5c08b56b-7d90-4196-b7dc-3ce429006d54.settings.uiMode','\"normal\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.5c08b56b-7d90-4196-b7dc-3ce429006d54.translationKeyFormat','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.5c08b56b-7d90-4196-b7dc-3ce429006d54.translationMethod','\"none\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.5c08b56b-7d90-4196-b7dc-3ce429006d54.type','\"craft\\\\fields\\\\PlainText\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.7ce298ad-35b8-47ee-8ed9-7ac00e81eda3.contentColumnType','\"text\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.7ce298ad-35b8-47ee-8ed9-7ac00e81eda3.fieldGroup','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.7ce298ad-35b8-47ee-8ed9-7ac00e81eda3.handle','\"city\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.7ce298ad-35b8-47ee-8ed9-7ac00e81eda3.instructions','\"\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.7ce298ad-35b8-47ee-8ed9-7ac00e81eda3.name','\"Stadt\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.7ce298ad-35b8-47ee-8ed9-7ac00e81eda3.searchable','false'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.7ce298ad-35b8-47ee-8ed9-7ac00e81eda3.settings.byteLimit','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.7ce298ad-35b8-47ee-8ed9-7ac00e81eda3.settings.charLimit','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.7ce298ad-35b8-47ee-8ed9-7ac00e81eda3.settings.code','\"\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.7ce298ad-35b8-47ee-8ed9-7ac00e81eda3.settings.columnType','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.7ce298ad-35b8-47ee-8ed9-7ac00e81eda3.settings.initialRows','\"4\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.7ce298ad-35b8-47ee-8ed9-7ac00e81eda3.settings.multiline','\"\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.7ce298ad-35b8-47ee-8ed9-7ac00e81eda3.settings.placeholder','\"\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.7ce298ad-35b8-47ee-8ed9-7ac00e81eda3.settings.uiMode','\"normal\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.7ce298ad-35b8-47ee-8ed9-7ac00e81eda3.translationKeyFormat','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.7ce298ad-35b8-47ee-8ed9-7ac00e81eda3.translationMethod','\"none\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.7ce298ad-35b8-47ee-8ed9-7ac00e81eda3.type','\"craft\\\\fields\\\\PlainText\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.daa6ae63-4514-429c-92ae-3662b6c0c8b7.contentColumnType','\"text\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.daa6ae63-4514-429c-92ae-3662b6c0c8b7.fieldGroup','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.daa6ae63-4514-429c-92ae-3662b6c0c8b7.handle','\"company\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.daa6ae63-4514-429c-92ae-3662b6c0c8b7.instructions','\"\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.daa6ae63-4514-429c-92ae-3662b6c0c8b7.name','\"Firma\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.daa6ae63-4514-429c-92ae-3662b6c0c8b7.searchable','false'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.daa6ae63-4514-429c-92ae-3662b6c0c8b7.settings.byteLimit','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.daa6ae63-4514-429c-92ae-3662b6c0c8b7.settings.charLimit','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.daa6ae63-4514-429c-92ae-3662b6c0c8b7.settings.code','\"\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.daa6ae63-4514-429c-92ae-3662b6c0c8b7.settings.columnType','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.daa6ae63-4514-429c-92ae-3662b6c0c8b7.settings.initialRows','\"4\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.daa6ae63-4514-429c-92ae-3662b6c0c8b7.settings.multiline','\"\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.daa6ae63-4514-429c-92ae-3662b6c0c8b7.settings.placeholder','\"\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.daa6ae63-4514-429c-92ae-3662b6c0c8b7.settings.uiMode','\"normal\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.daa6ae63-4514-429c-92ae-3662b6c0c8b7.translationKeyFormat','null'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.daa6ae63-4514-429c-92ae-3662b6c0c8b7.translationMethod','\"none\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.fields.daa6ae63-4514-429c-92ae-3662b6c0c8b7.type','\"craft\\\\fields\\\\PlainText\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.handle','\"address\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.name','\"Adress\"'),('matrixBlockTypes.5b468bfc-cfaf-49ed-ad5f-b67cd5d3ab88.sortOrder','1'),('plugins.contact-form.edition','\"standard\"'),('plugins.contact-form.enabled','true'),('plugins.contact-form.schemaVersion','\"1.0.0\"'),('plugins.contact-form.settings.allowAttachments','\"1\"'),('plugins.contact-form.settings.prependSender','\"Mitteilung von\"'),('plugins.contact-form.settings.prependSubject','\"Neue Nachricht von youInvestment\"'),('plugins.contact-form.settings.successFlashMessage','\"Ihre Nachricht wurde versendet.\"'),('plugins.contact-form.settings.toEmail','\"julian.henz@youengineering.ch\"'),('plugins.obfuscator.edition','\"standard\"'),('plugins.obfuscator.enabled','true'),('plugins.obfuscator.schemaVersion','\"1.0.0\"'),('plugins.redactor-custom-styles.edition','\"standard\"'),('plugins.redactor-custom-styles.enabled','true'),('plugins.redactor-custom-styles.schemaVersion','\"1.0.0\"'),('plugins.redactor.edition','\"standard\"'),('plugins.redactor.enabled','true'),('plugins.redactor.schemaVersion','\"2.3.0\"'),('plugins.seo.edition','\"standard\"'),('plugins.seo.enabled','true'),('plugins.seo.schemaVersion','\"3.2.0\"'),('sections.8d13db31-6006-40c9-93ed-af826ba88337.enableVersioning','true'),('sections.8d13db31-6006-40c9-93ed-af826ba88337.handle','\"home\"'),('sections.8d13db31-6006-40c9-93ed-af826ba88337.name','\"Home\"'),('sections.8d13db31-6006-40c9-93ed-af826ba88337.propagationMethod','\"all\"'),('sections.8d13db31-6006-40c9-93ed-af826ba88337.siteSettings.04fae7e4-6314-4ce9-8093-b3177d9a29f6.enabledByDefault','true'),('sections.8d13db31-6006-40c9-93ed-af826ba88337.siteSettings.04fae7e4-6314-4ce9-8093-b3177d9a29f6.hasUrls','true'),('sections.8d13db31-6006-40c9-93ed-af826ba88337.siteSettings.04fae7e4-6314-4ce9-8093-b3177d9a29f6.template','\"index\"'),('sections.8d13db31-6006-40c9-93ed-af826ba88337.siteSettings.04fae7e4-6314-4ce9-8093-b3177d9a29f6.uriFormat','\"__home__\"'),('sections.8d13db31-6006-40c9-93ed-af826ba88337.type','\"single\"'),('sections.98c8b9df-a9ab-4d7b-8bff-02c330477b94.enableVersioning','true'),('sections.98c8b9df-a9ab-4d7b-8bff-02c330477b94.handle','\"datenschutz\"'),('sections.98c8b9df-a9ab-4d7b-8bff-02c330477b94.name','\"Datenschutz\"'),('sections.98c8b9df-a9ab-4d7b-8bff-02c330477b94.propagationMethod','\"all\"'),('sections.98c8b9df-a9ab-4d7b-8bff-02c330477b94.siteSettings.04fae7e4-6314-4ce9-8093-b3177d9a29f6.enabledByDefault','true'),('sections.98c8b9df-a9ab-4d7b-8bff-02c330477b94.siteSettings.04fae7e4-6314-4ce9-8093-b3177d9a29f6.hasUrls','true'),('sections.98c8b9df-a9ab-4d7b-8bff-02c330477b94.siteSettings.04fae7e4-6314-4ce9-8093-b3177d9a29f6.template','\"page\"'),('sections.98c8b9df-a9ab-4d7b-8bff-02c330477b94.siteSettings.04fae7e4-6314-4ce9-8093-b3177d9a29f6.uriFormat','\"datenschutz\"'),('sections.98c8b9df-a9ab-4d7b-8bff-02c330477b94.type','\"single\"'),('sections.c26c6845-e3fa-4bdf-a418-c0de68a63354.enableVersioning','true'),('sections.c26c6845-e3fa-4bdf-a418-c0de68a63354.handle','\"impressum\"'),('sections.c26c6845-e3fa-4bdf-a418-c0de68a63354.name','\"Impressum\"'),('sections.c26c6845-e3fa-4bdf-a418-c0de68a63354.propagationMethod','\"all\"'),('sections.c26c6845-e3fa-4bdf-a418-c0de68a63354.siteSettings.04fae7e4-6314-4ce9-8093-b3177d9a29f6.enabledByDefault','true'),('sections.c26c6845-e3fa-4bdf-a418-c0de68a63354.siteSettings.04fae7e4-6314-4ce9-8093-b3177d9a29f6.hasUrls','true'),('sections.c26c6845-e3fa-4bdf-a418-c0de68a63354.siteSettings.04fae7e4-6314-4ce9-8093-b3177d9a29f6.template','\"page\"'),('sections.c26c6845-e3fa-4bdf-a418-c0de68a63354.siteSettings.04fae7e4-6314-4ce9-8093-b3177d9a29f6.uriFormat','\"impressum\"'),('sections.c26c6845-e3fa-4bdf-a418-c0de68a63354.type','\"single\"'),('siteGroups.25c2c1c5-1438-4ab5-9005-9737c29ce61f.name','\"youInvestment\"'),('sites.04fae7e4-6314-4ce9-8093-b3177d9a29f6.baseUrl','\"$PRIMARY_SITE_URL\"'),('sites.04fae7e4-6314-4ce9-8093-b3177d9a29f6.handle','\"default\"'),('sites.04fae7e4-6314-4ce9-8093-b3177d9a29f6.hasUrls','true'),('sites.04fae7e4-6314-4ce9-8093-b3177d9a29f6.language','\"en-US\"'),('sites.04fae7e4-6314-4ce9-8093-b3177d9a29f6.name','\"youInvestment\"'),('sites.04fae7e4-6314-4ce9-8093-b3177d9a29f6.primary','true'),('sites.04fae7e4-6314-4ce9-8093-b3177d9a29f6.siteGroup','\"25c2c1c5-1438-4ab5-9005-9737c29ce61f\"'),('sites.04fae7e4-6314-4ce9-8093-b3177d9a29f6.sortOrder','1'),('system.edition','\"solo\"'),('system.live','true'),('system.name','\"youInvestment\"'),('system.retryDuration','null'),('system.schemaVersion','\"3.5.13\"'),('system.timeZone','\"Europe/Zurich\"'),('users.allowPublicRegistration','false'),('users.defaultGroup','null'),('users.photoSubpath','null'),('users.photoVolumeUid','null'),('users.requireEmailVerification','true'),('volumes.4728db2c-e1bf-4b67-9928-84174a45345e.fieldLayouts.e0e4c89c-9b93-4d18-a065-4efd0861be5d.tabs.0.elements.0.autocapitalize','true'),('volumes.4728db2c-e1bf-4b67-9928-84174a45345e.fieldLayouts.e0e4c89c-9b93-4d18-a065-4efd0861be5d.tabs.0.elements.0.autocomplete','false'),('volumes.4728db2c-e1bf-4b67-9928-84174a45345e.fieldLayouts.e0e4c89c-9b93-4d18-a065-4efd0861be5d.tabs.0.elements.0.autocorrect','true'),('volumes.4728db2c-e1bf-4b67-9928-84174a45345e.fieldLayouts.e0e4c89c-9b93-4d18-a065-4efd0861be5d.tabs.0.elements.0.class','null'),('volumes.4728db2c-e1bf-4b67-9928-84174a45345e.fieldLayouts.e0e4c89c-9b93-4d18-a065-4efd0861be5d.tabs.0.elements.0.disabled','false'),('volumes.4728db2c-e1bf-4b67-9928-84174a45345e.fieldLayouts.e0e4c89c-9b93-4d18-a065-4efd0861be5d.tabs.0.elements.0.id','null'),('volumes.4728db2c-e1bf-4b67-9928-84174a45345e.fieldLayouts.e0e4c89c-9b93-4d18-a065-4efd0861be5d.tabs.0.elements.0.instructions','null'),('volumes.4728db2c-e1bf-4b67-9928-84174a45345e.fieldLayouts.e0e4c89c-9b93-4d18-a065-4efd0861be5d.tabs.0.elements.0.label','null'),('volumes.4728db2c-e1bf-4b67-9928-84174a45345e.fieldLayouts.e0e4c89c-9b93-4d18-a065-4efd0861be5d.tabs.0.elements.0.max','null'),('volumes.4728db2c-e1bf-4b67-9928-84174a45345e.fieldLayouts.e0e4c89c-9b93-4d18-a065-4efd0861be5d.tabs.0.elements.0.min','null'),('volumes.4728db2c-e1bf-4b67-9928-84174a45345e.fieldLayouts.e0e4c89c-9b93-4d18-a065-4efd0861be5d.tabs.0.elements.0.name','null'),('volumes.4728db2c-e1bf-4b67-9928-84174a45345e.fieldLayouts.e0e4c89c-9b93-4d18-a065-4efd0861be5d.tabs.0.elements.0.orientation','null'),('volumes.4728db2c-e1bf-4b67-9928-84174a45345e.fieldLayouts.e0e4c89c-9b93-4d18-a065-4efd0861be5d.tabs.0.elements.0.placeholder','null'),('volumes.4728db2c-e1bf-4b67-9928-84174a45345e.fieldLayouts.e0e4c89c-9b93-4d18-a065-4efd0861be5d.tabs.0.elements.0.readonly','false'),('volumes.4728db2c-e1bf-4b67-9928-84174a45345e.fieldLayouts.e0e4c89c-9b93-4d18-a065-4efd0861be5d.tabs.0.elements.0.requirable','false'),('volumes.4728db2c-e1bf-4b67-9928-84174a45345e.fieldLayouts.e0e4c89c-9b93-4d18-a065-4efd0861be5d.tabs.0.elements.0.size','null'),('volumes.4728db2c-e1bf-4b67-9928-84174a45345e.fieldLayouts.e0e4c89c-9b93-4d18-a065-4efd0861be5d.tabs.0.elements.0.step','null'),('volumes.4728db2c-e1bf-4b67-9928-84174a45345e.fieldLayouts.e0e4c89c-9b93-4d18-a065-4efd0861be5d.tabs.0.elements.0.tip','null'),('volumes.4728db2c-e1bf-4b67-9928-84174a45345e.fieldLayouts.e0e4c89c-9b93-4d18-a065-4efd0861be5d.tabs.0.elements.0.title','null'),('volumes.4728db2c-e1bf-4b67-9928-84174a45345e.fieldLayouts.e0e4c89c-9b93-4d18-a065-4efd0861be5d.tabs.0.elements.0.type','\"craft\\\\fieldlayoutelements\\\\TitleField\"'),('volumes.4728db2c-e1bf-4b67-9928-84174a45345e.fieldLayouts.e0e4c89c-9b93-4d18-a065-4efd0861be5d.tabs.0.elements.0.warning','null'),('volumes.4728db2c-e1bf-4b67-9928-84174a45345e.fieldLayouts.e0e4c89c-9b93-4d18-a065-4efd0861be5d.tabs.0.elements.0.width','100'),('volumes.4728db2c-e1bf-4b67-9928-84174a45345e.fieldLayouts.e0e4c89c-9b93-4d18-a065-4efd0861be5d.tabs.0.elements.1.fieldUid','\"0e3aff52-ddb4-4366-9bc4-15996f6a433c\"'),('volumes.4728db2c-e1bf-4b67-9928-84174a45345e.fieldLayouts.e0e4c89c-9b93-4d18-a065-4efd0861be5d.tabs.0.elements.1.instructions','null'),('volumes.4728db2c-e1bf-4b67-9928-84174a45345e.fieldLayouts.e0e4c89c-9b93-4d18-a065-4efd0861be5d.tabs.0.elements.1.label','null'),('volumes.4728db2c-e1bf-4b67-9928-84174a45345e.fieldLayouts.e0e4c89c-9b93-4d18-a065-4efd0861be5d.tabs.0.elements.1.required','false'),('volumes.4728db2c-e1bf-4b67-9928-84174a45345e.fieldLayouts.e0e4c89c-9b93-4d18-a065-4efd0861be5d.tabs.0.elements.1.tip','null'),('volumes.4728db2c-e1bf-4b67-9928-84174a45345e.fieldLayouts.e0e4c89c-9b93-4d18-a065-4efd0861be5d.tabs.0.elements.1.type','\"craft\\\\fieldlayoutelements\\\\CustomField\"'),('volumes.4728db2c-e1bf-4b67-9928-84174a45345e.fieldLayouts.e0e4c89c-9b93-4d18-a065-4efd0861be5d.tabs.0.elements.1.warning','null'),('volumes.4728db2c-e1bf-4b67-9928-84174a45345e.fieldLayouts.e0e4c89c-9b93-4d18-a065-4efd0861be5d.tabs.0.elements.1.width','100'),('volumes.4728db2c-e1bf-4b67-9928-84174a45345e.fieldLayouts.e0e4c89c-9b93-4d18-a065-4efd0861be5d.tabs.0.name','\"Content\"'),('volumes.4728db2c-e1bf-4b67-9928-84174a45345e.fieldLayouts.e0e4c89c-9b93-4d18-a065-4efd0861be5d.tabs.0.sortOrder','1'),('volumes.4728db2c-e1bf-4b67-9928-84174a45345e.handle','\"news\"'),('volumes.4728db2c-e1bf-4b67-9928-84174a45345e.hasUrls','true'),('volumes.4728db2c-e1bf-4b67-9928-84174a45345e.name','\"News\"'),('volumes.4728db2c-e1bf-4b67-9928-84174a45345e.settings.path','\"@webroot/news\"'),('volumes.4728db2c-e1bf-4b67-9928-84174a45345e.sortOrder','2'),('volumes.4728db2c-e1bf-4b67-9928-84174a45345e.type','\"craft\\\\volumes\\\\Local\"'),('volumes.4728db2c-e1bf-4b67-9928-84174a45345e.url','\"@web/news\"'),('volumes.9ab8fbf3-1f45-4e90-ae46-4541be742a4c.fieldLayouts.66a3acfc-9e2f-462d-9363-5206d283791b.tabs.0.elements.0.autocapitalize','true'),('volumes.9ab8fbf3-1f45-4e90-ae46-4541be742a4c.fieldLayouts.66a3acfc-9e2f-462d-9363-5206d283791b.tabs.0.elements.0.autocomplete','false'),('volumes.9ab8fbf3-1f45-4e90-ae46-4541be742a4c.fieldLayouts.66a3acfc-9e2f-462d-9363-5206d283791b.tabs.0.elements.0.autocorrect','true'),('volumes.9ab8fbf3-1f45-4e90-ae46-4541be742a4c.fieldLayouts.66a3acfc-9e2f-462d-9363-5206d283791b.tabs.0.elements.0.class','null'),('volumes.9ab8fbf3-1f45-4e90-ae46-4541be742a4c.fieldLayouts.66a3acfc-9e2f-462d-9363-5206d283791b.tabs.0.elements.0.disabled','false'),('volumes.9ab8fbf3-1f45-4e90-ae46-4541be742a4c.fieldLayouts.66a3acfc-9e2f-462d-9363-5206d283791b.tabs.0.elements.0.id','null'),('volumes.9ab8fbf3-1f45-4e90-ae46-4541be742a4c.fieldLayouts.66a3acfc-9e2f-462d-9363-5206d283791b.tabs.0.elements.0.instructions','null'),('volumes.9ab8fbf3-1f45-4e90-ae46-4541be742a4c.fieldLayouts.66a3acfc-9e2f-462d-9363-5206d283791b.tabs.0.elements.0.label','null'),('volumes.9ab8fbf3-1f45-4e90-ae46-4541be742a4c.fieldLayouts.66a3acfc-9e2f-462d-9363-5206d283791b.tabs.0.elements.0.max','null'),('volumes.9ab8fbf3-1f45-4e90-ae46-4541be742a4c.fieldLayouts.66a3acfc-9e2f-462d-9363-5206d283791b.tabs.0.elements.0.min','null'),('volumes.9ab8fbf3-1f45-4e90-ae46-4541be742a4c.fieldLayouts.66a3acfc-9e2f-462d-9363-5206d283791b.tabs.0.elements.0.name','null'),('volumes.9ab8fbf3-1f45-4e90-ae46-4541be742a4c.fieldLayouts.66a3acfc-9e2f-462d-9363-5206d283791b.tabs.0.elements.0.orientation','null'),('volumes.9ab8fbf3-1f45-4e90-ae46-4541be742a4c.fieldLayouts.66a3acfc-9e2f-462d-9363-5206d283791b.tabs.0.elements.0.placeholder','null'),('volumes.9ab8fbf3-1f45-4e90-ae46-4541be742a4c.fieldLayouts.66a3acfc-9e2f-462d-9363-5206d283791b.tabs.0.elements.0.readonly','false'),('volumes.9ab8fbf3-1f45-4e90-ae46-4541be742a4c.fieldLayouts.66a3acfc-9e2f-462d-9363-5206d283791b.tabs.0.elements.0.requirable','false'),('volumes.9ab8fbf3-1f45-4e90-ae46-4541be742a4c.fieldLayouts.66a3acfc-9e2f-462d-9363-5206d283791b.tabs.0.elements.0.size','null'),('volumes.9ab8fbf3-1f45-4e90-ae46-4541be742a4c.fieldLayouts.66a3acfc-9e2f-462d-9363-5206d283791b.tabs.0.elements.0.step','null'),('volumes.9ab8fbf3-1f45-4e90-ae46-4541be742a4c.fieldLayouts.66a3acfc-9e2f-462d-9363-5206d283791b.tabs.0.elements.0.tip','null'),('volumes.9ab8fbf3-1f45-4e90-ae46-4541be742a4c.fieldLayouts.66a3acfc-9e2f-462d-9363-5206d283791b.tabs.0.elements.0.title','null'),('volumes.9ab8fbf3-1f45-4e90-ae46-4541be742a4c.fieldLayouts.66a3acfc-9e2f-462d-9363-5206d283791b.tabs.0.elements.0.type','\"craft\\\\fieldlayoutelements\\\\TitleField\"'),('volumes.9ab8fbf3-1f45-4e90-ae46-4541be742a4c.fieldLayouts.66a3acfc-9e2f-462d-9363-5206d283791b.tabs.0.elements.0.warning','null'),('volumes.9ab8fbf3-1f45-4e90-ae46-4541be742a4c.fieldLayouts.66a3acfc-9e2f-462d-9363-5206d283791b.tabs.0.elements.0.width','100'),('volumes.9ab8fbf3-1f45-4e90-ae46-4541be742a4c.fieldLayouts.66a3acfc-9e2f-462d-9363-5206d283791b.tabs.0.name','\"Content\"'),('volumes.9ab8fbf3-1f45-4e90-ae46-4541be742a4c.fieldLayouts.66a3acfc-9e2f-462d-9363-5206d283791b.tabs.0.sortOrder','1'),('volumes.9ab8fbf3-1f45-4e90-ae46-4541be742a4c.handle','\"uploads\"'),('volumes.9ab8fbf3-1f45-4e90-ae46-4541be742a4c.hasUrls','true'),('volumes.9ab8fbf3-1f45-4e90-ae46-4541be742a4c.name','\"Uploads\"'),('volumes.9ab8fbf3-1f45-4e90-ae46-4541be742a4c.settings.path','\"@webroot/uploads\"'),('volumes.9ab8fbf3-1f45-4e90-ae46-4541be742a4c.sortOrder','1'),('volumes.9ab8fbf3-1f45-4e90-ae46-4541be742a4c.type','\"craft\\\\volumes\\\\Local\"'),('volumes.9ab8fbf3-1f45-4e90-ae46-4541be742a4c.url','\"@web/uploads\"');
/*!40000 ALTER TABLE `projectconfig` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `queue`
--

DROP TABLE IF EXISTS `queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel` varchar(255) NOT NULL DEFAULT 'queue',
  `job` longblob NOT NULL,
  `description` text,
  `timePushed` int(11) NOT NULL,
  `ttr` int(11) NOT NULL,
  `delay` int(11) NOT NULL DEFAULT '0',
  `priority` int(11) unsigned NOT NULL DEFAULT '1024',
  `dateReserved` datetime DEFAULT NULL,
  `timeUpdated` int(11) DEFAULT NULL,
  `progress` smallint(6) NOT NULL DEFAULT '0',
  `progressLabel` varchar(255) DEFAULT NULL,
  `attempt` int(11) DEFAULT NULL,
  `fail` tinyint(1) DEFAULT '0',
  `dateFailed` datetime DEFAULT NULL,
  `error` text,
  PRIMARY KEY (`id`),
  KEY `queue_channel_fail_timeUpdated_timePushed_idx` (`channel`,`fail`,`timeUpdated`,`timePushed`),
  KEY `queue_channel_fail_timeUpdated_delay_idx` (`channel`,`fail`,`timeUpdated`,`delay`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `queue`
--

LOCK TABLES `queue` WRITE;
/*!40000 ALTER TABLE `queue` DISABLE KEYS */;
/*!40000 ALTER TABLE `queue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relations`
--

DROP TABLE IF EXISTS `relations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldId` int(11) NOT NULL,
  `sourceId` int(11) NOT NULL,
  `sourceSiteId` int(11) DEFAULT NULL,
  `targetId` int(11) NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `relations_fieldId_sourceId_sourceSiteId_targetId_unq_idx` (`fieldId`,`sourceId`,`sourceSiteId`,`targetId`),
  KEY `relations_sourceId_idx` (`sourceId`),
  KEY `relations_targetId_idx` (`targetId`),
  KEY `relations_sourceSiteId_idx` (`sourceSiteId`),
  CONSTRAINT `relations_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `relations_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `relations_sourceSiteId_fk` FOREIGN KEY (`sourceSiteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `relations_targetId_fk` FOREIGN KEY (`targetId`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relations`
--

LOCK TABLES `relations` WRITE;
/*!40000 ALTER TABLE `relations` DISABLE KEYS */;
/*!40000 ALTER TABLE `relations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resourcepaths`
--

DROP TABLE IF EXISTS `resourcepaths`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resourcepaths` (
  `hash` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  PRIMARY KEY (`hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resourcepaths`
--

LOCK TABLES `resourcepaths` WRITE;
/*!40000 ALTER TABLE `resourcepaths` DISABLE KEYS */;
INSERT INTO `resourcepaths` VALUES ('2233a82e','@craft/web/assets/cp/dist'),('241c8d67','@lib/selectize'),('2e70c9ed','@lib/xregexp'),('3dbf2a7b','@lib/datepicker-i18n'),('4d7a9734','@lib/garnishjs'),('583c430a','@lib/fabric'),('5b4e18cf','@app/web/assets/dashboard/dist'),('64299e0a','@lib/iframe-resizer'),('688aa11d','@lib/fileupload'),('7062a0e3','@lib/picturefill'),('754096a8','@lib/prismjs'),('77c69c21','@lib/jquery-touch-events'),('78c190e4','@craft/web/assets/utilities/dist'),('7ec780b2','@lib/d3'),('850e710e','@app/web/assets/recententries/dist'),('98435683','@app/web/assets/cp/dist'),('9c7d43fd','@app/web/assets/login/dist'),('a3eef0bc','@lib/jquery-ui'),('a939b0f5','@lib/axios'),('abe1615f','@app/web/assets/updater/dist'),('b1243a2f','@craft/redactor/assets/redactor/dist'),('b5269fa1','@app/web/assets/craftsupport/dist'),('bbb43360','@app/web/assets/updateswidget/dist'),('d3583791','@lib/element-resize-detector'),('e1581d3d','@app/web/assets/feed/dist'),('e84d7b3d','@carlcs/redactorcustomstyles/assets/redactorplugin/dist'),('ed476b19','@lib/velocity'),('f160c4f1','@lib/jquery.payment'),('fa381412','@app/web/assets/utilities/dist'),('fcc89c64','@bower/jquery/dist');
/*!40000 ALTER TABLE `resourcepaths` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `revisions`
--

DROP TABLE IF EXISTS `revisions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `revisions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sourceId` int(11) NOT NULL,
  `creatorId` int(11) DEFAULT NULL,
  `num` int(11) NOT NULL,
  `notes` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `revisions_sourceId_num_unq_idx` (`sourceId`,`num`),
  KEY `revisions_creatorId_fk` (`creatorId`),
  CONSTRAINT `revisions_creatorId_fk` FOREIGN KEY (`creatorId`) REFERENCES `users` (`id`) ON DELETE SET NULL,
  CONSTRAINT `revisions_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `revisions`
--

LOCK TABLES `revisions` WRITE;
/*!40000 ALTER TABLE `revisions` DISABLE KEYS */;
INSERT INTO `revisions` VALUES (1,4,1,1,NULL),(2,6,1,1,NULL),(3,8,1,1,NULL);
/*!40000 ALTER TABLE `revisions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `searchindex`
--

DROP TABLE IF EXISTS `searchindex`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `searchindex` (
  `elementId` int(11) NOT NULL,
  `attribute` varchar(25) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `keywords` text NOT NULL,
  PRIMARY KEY (`elementId`,`attribute`,`fieldId`,`siteId`),
  FULLTEXT KEY `searchindex_keywords_idx` (`keywords`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `searchindex`
--

LOCK TABLES `searchindex` WRITE;
/*!40000 ALTER TABLE `searchindex` DISABLE KEYS */;
INSERT INTO `searchindex` VALUES (1,'username',0,2,' admin '),(1,'firstname',0,2,''),(1,'lastname',0,2,''),(1,'fullname',0,2,''),(1,'email',0,2,' julian henz youengineering ch '),(1,'slug',0,2,''),(2,'slug',0,2,''),(3,'slug',0,2,''),(4,'slug',0,2,' impressum '),(4,'title',0,2,' impressum '),(6,'slug',0,2,' datenschutz '),(6,'title',0,2,' datenschutz '),(8,'slug',0,2,' home '),(8,'title',0,2,' home ');
/*!40000 ALTER TABLE `searchindex` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sections`
--

DROP TABLE IF EXISTS `sections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `type` enum('single','channel','structure') NOT NULL DEFAULT 'channel',
  `enableVersioning` tinyint(1) NOT NULL DEFAULT '0',
  `propagationMethod` varchar(255) NOT NULL DEFAULT 'all',
  `previewTargets` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `sections_handle_idx` (`handle`),
  KEY `sections_name_idx` (`name`),
  KEY `sections_structureId_idx` (`structureId`),
  KEY `sections_dateDeleted_idx` (`dateDeleted`),
  CONSTRAINT `sections_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `structures` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sections`
--

LOCK TABLES `sections` WRITE;
/*!40000 ALTER TABLE `sections` DISABLE KEYS */;
INSERT INTO `sections` VALUES (1,NULL,'Impressum','impressum','single',1,'all',NULL,'2021-05-21 13:41:01','2021-05-21 13:41:01',NULL,'c26c6845-e3fa-4bdf-a418-c0de68a63354'),(2,NULL,'Home','home','single',1,'all',NULL,'2021-05-21 13:41:01','2021-05-21 13:41:01',NULL,'8d13db31-6006-40c9-93ed-af826ba88337'),(3,NULL,'Datenschutz','datenschutz','single',1,'all',NULL,'2021-05-21 13:41:01','2021-05-21 13:41:01',NULL,'98c8b9df-a9ab-4d7b-8bff-02c330477b94');
/*!40000 ALTER TABLE `sections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sections_sites`
--

DROP TABLE IF EXISTS `sections_sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sections_sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sectionId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '1',
  `uriFormat` text,
  `template` varchar(500) DEFAULT NULL,
  `enabledByDefault` tinyint(1) NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sections_sites_sectionId_siteId_unq_idx` (`sectionId`,`siteId`),
  KEY `sections_sites_siteId_idx` (`siteId`),
  CONSTRAINT `sections_sites_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE,
  CONSTRAINT `sections_sites_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sections_sites`
--

LOCK TABLES `sections_sites` WRITE;
/*!40000 ALTER TABLE `sections_sites` DISABLE KEYS */;
INSERT INTO `sections_sites` VALUES (1,1,2,1,'impressum','page',1,'2021-05-21 13:41:01','2021-05-21 13:41:01','13d1450b-eedb-42c1-86f1-164698c23ff3'),(2,2,2,1,'__home__','index',1,'2021-05-21 13:41:01','2021-05-21 13:41:01','3f7504ae-42d4-4874-a325-cffcab430821'),(3,3,2,1,'datenschutz','page',1,'2021-05-21 13:41:01','2021-05-21 13:41:01','a87916a5-963a-4881-99a3-8d17108bf10b');
/*!40000 ALTER TABLE `sections_sites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seo_redirects`
--

DROP TABLE IF EXISTS `seo_redirects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seo_redirects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uri` varchar(255) NOT NULL,
  `to` varchar(255) NOT NULL,
  `type` enum('301','302') NOT NULL,
  `siteId` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seo_redirects`
--

LOCK TABLES `seo_redirects` WRITE;
/*!40000 ALTER TABLE `seo_redirects` DISABLE KEYS */;
/*!40000 ALTER TABLE `seo_redirects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seo_sitemap`
--

DROP TABLE IF EXISTS `seo_sitemap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seo_sitemap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group` enum('sections','categories','productTypes','customUrls') NOT NULL,
  `url` varchar(255) NOT NULL,
  `frequency` enum('always','hourly','daily','weekly','monthly','yearly','never') NOT NULL,
  `priority` float NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seo_sitemap`
--

LOCK TABLES `seo_sitemap` WRITE;
/*!40000 ALTER TABLE `seo_sitemap` DISABLE KEYS */;
/*!40000 ALTER TABLE `seo_sitemap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sequences`
--

DROP TABLE IF EXISTS `sequences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sequences` (
  `name` varchar(255) NOT NULL,
  `next` int(11) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sequences`
--

LOCK TABLES `sequences` WRITE;
/*!40000 ALTER TABLE `sequences` DISABLE KEYS */;
/*!40000 ALTER TABLE `sequences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `token` char(100) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `sessions_uid_idx` (`uid`),
  KEY `sessions_token_idx` (`token`),
  KEY `sessions_dateUpdated_idx` (`dateUpdated`),
  KEY `sessions_userId_idx` (`userId`),
  CONSTRAINT `sessions_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` VALUES (1,1,'fnjywQdySKyxvDIeIgJLX_o8qDvipvf3IAKqkrxfKvG2IDM-GyVg2fG-4geddjtRML7PzQX6s1Y1CESWozrV-ACMEujjbLlLQ0Qh','2021-05-21 13:40:34','2021-05-21 13:41:12','5e225644-bb3a-4039-b932-afd6a1e63f9f');
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shunnedmessages`
--

DROP TABLE IF EXISTS `shunnedmessages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shunnedmessages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `message` varchar(255) NOT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `shunnedmessages_userId_message_unq_idx` (`userId`,`message`),
  CONSTRAINT `shunnedmessages_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shunnedmessages`
--

LOCK TABLES `shunnedmessages` WRITE;
/*!40000 ALTER TABLE `shunnedmessages` DISABLE KEYS */;
/*!40000 ALTER TABLE `shunnedmessages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sitegroups`
--

DROP TABLE IF EXISTS `sitegroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sitegroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `sitegroups_name_idx` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sitegroups`
--

LOCK TABLES `sitegroups` WRITE;
/*!40000 ALTER TABLE `sitegroups` DISABLE KEYS */;
INSERT INTO `sitegroups` VALUES (1,'youInvestment','2021-01-07 10:01:15','2021-05-21 13:40:59','2021-05-21 13:40:59','46d7ede1-3681-4147-bfe4-7cd28015fca7'),(2,'youInvestment','2021-05-21 13:40:59','2021-05-21 13:40:59',NULL,'25c2c1c5-1438-4ab5-9005-9737c29ce61f');
/*!40000 ALTER TABLE `sitegroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sites`
--

DROP TABLE IF EXISTS `sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `primary` tinyint(1) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `language` varchar(12) NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '0',
  `baseUrl` varchar(255) DEFAULT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `sites_dateDeleted_idx` (`dateDeleted`),
  KEY `sites_handle_idx` (`handle`),
  KEY `sites_sortOrder_idx` (`sortOrder`),
  KEY `sites_groupId_fk` (`groupId`),
  CONSTRAINT `sites_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `sitegroups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sites`
--

LOCK TABLES `sites` WRITE;
/*!40000 ALTER TABLE `sites` DISABLE KEYS */;
INSERT INTO `sites` VALUES (1,1,0,1,'youInvestment','default','en-US',1,'$PRIMARY_SITE_URL',1,'2021-01-07 10:01:15','2021-05-21 13:40:59','2021-05-21 13:40:59','4e4790a3-5142-496d-b722-9ff42c12ee4d'),(2,2,1,1,'youInvestment','default','en-US',1,'$PRIMARY_SITE_URL',1,'2021-05-21 13:40:59','2021-05-21 13:40:59',NULL,'04fae7e4-6314-4ce9-8093-b3177d9a29f6');
/*!40000 ALTER TABLE `sites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `structureelements`
--

DROP TABLE IF EXISTS `structureelements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `structureelements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) NOT NULL,
  `elementId` int(11) DEFAULT NULL,
  `root` int(11) unsigned DEFAULT NULL,
  `lft` int(11) unsigned NOT NULL,
  `rgt` int(11) unsigned NOT NULL,
  `level` smallint(6) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `structureelements_structureId_elementId_unq_idx` (`structureId`,`elementId`),
  KEY `structureelements_root_idx` (`root`),
  KEY `structureelements_lft_idx` (`lft`),
  KEY `structureelements_rgt_idx` (`rgt`),
  KEY `structureelements_level_idx` (`level`),
  KEY `structureelements_elementId_idx` (`elementId`),
  CONSTRAINT `structureelements_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `structureelements_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `structures` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `structureelements`
--

LOCK TABLES `structureelements` WRITE;
/*!40000 ALTER TABLE `structureelements` DISABLE KEYS */;
/*!40000 ALTER TABLE `structureelements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `structures`
--

DROP TABLE IF EXISTS `structures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `structures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `maxLevels` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `structures_dateDeleted_idx` (`dateDeleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `structures`
--

LOCK TABLES `structures` WRITE;
/*!40000 ALTER TABLE `structures` DISABLE KEYS */;
/*!40000 ALTER TABLE `structures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `systemmessages`
--

DROP TABLE IF EXISTS `systemmessages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `systemmessages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(255) NOT NULL,
  `key` varchar(255) NOT NULL,
  `subject` text NOT NULL,
  `body` text NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `systemmessages_key_language_unq_idx` (`key`,`language`),
  KEY `systemmessages_language_idx` (`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `systemmessages`
--

LOCK TABLES `systemmessages` WRITE;
/*!40000 ALTER TABLE `systemmessages` DISABLE KEYS */;
/*!40000 ALTER TABLE `systemmessages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `taggroups`
--

DROP TABLE IF EXISTS `taggroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taggroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `taggroups_name_idx` (`name`),
  KEY `taggroups_handle_idx` (`handle`),
  KEY `taggroups_dateDeleted_idx` (`dateDeleted`),
  KEY `taggroups_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `taggroups_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `taggroups`
--

LOCK TABLES `taggroups` WRITE;
/*!40000 ALTER TABLE `taggroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `taggroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `deletedWithGroup` tinyint(1) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `tags_groupId_idx` (`groupId`),
  CONSTRAINT `tags_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `taggroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tags_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `templatecacheelements`
--

DROP TABLE IF EXISTS `templatecacheelements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `templatecacheelements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cacheId` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `templatecacheelements_cacheId_idx` (`cacheId`),
  KEY `templatecacheelements_elementId_idx` (`elementId`),
  CONSTRAINT `templatecacheelements_cacheId_fk` FOREIGN KEY (`cacheId`) REFERENCES `templatecaches` (`id`) ON DELETE CASCADE,
  CONSTRAINT `templatecacheelements_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `templatecacheelements`
--

LOCK TABLES `templatecacheelements` WRITE;
/*!40000 ALTER TABLE `templatecacheelements` DISABLE KEYS */;
/*!40000 ALTER TABLE `templatecacheelements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `templatecachequeries`
--

DROP TABLE IF EXISTS `templatecachequeries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `templatecachequeries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cacheId` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `query` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `templatecachequeries_cacheId_idx` (`cacheId`),
  KEY `templatecachequeries_type_idx` (`type`),
  CONSTRAINT `templatecachequeries_cacheId_fk` FOREIGN KEY (`cacheId`) REFERENCES `templatecaches` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `templatecachequeries`
--

LOCK TABLES `templatecachequeries` WRITE;
/*!40000 ALTER TABLE `templatecachequeries` DISABLE KEYS */;
/*!40000 ALTER TABLE `templatecachequeries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `templatecaches`
--

DROP TABLE IF EXISTS `templatecaches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `templatecaches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `siteId` int(11) NOT NULL,
  `cacheKey` varchar(255) NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  `expiryDate` datetime NOT NULL,
  `body` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `templatecaches_cacheKey_siteId_expiryDate_path_idx` (`cacheKey`,`siteId`,`expiryDate`,`path`),
  KEY `templatecaches_cacheKey_siteId_expiryDate_idx` (`cacheKey`,`siteId`,`expiryDate`),
  KEY `templatecaches_siteId_idx` (`siteId`),
  CONSTRAINT `templatecaches_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `templatecaches`
--

LOCK TABLES `templatecaches` WRITE;
/*!40000 ALTER TABLE `templatecaches` DISABLE KEYS */;
/*!40000 ALTER TABLE `templatecaches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tokens`
--

DROP TABLE IF EXISTS `tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` char(32) NOT NULL,
  `route` text,
  `usageLimit` tinyint(3) unsigned DEFAULT NULL,
  `usageCount` tinyint(3) unsigned DEFAULT NULL,
  `expiryDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `tokens_token_unq_idx` (`token`),
  KEY `tokens_expiryDate_idx` (`expiryDate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tokens`
--

LOCK TABLES `tokens` WRITE;
/*!40000 ALTER TABLE `tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usergroups`
--

DROP TABLE IF EXISTS `usergroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usergroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `description` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `usergroups_handle_idx` (`handle`),
  KEY `usergroups_name_idx` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usergroups`
--

LOCK TABLES `usergroups` WRITE;
/*!40000 ALTER TABLE `usergroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `usergroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usergroups_users`
--

DROP TABLE IF EXISTS `usergroups_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usergroups_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `usergroups_users_groupId_userId_unq_idx` (`groupId`,`userId`),
  KEY `usergroups_users_userId_idx` (`userId`),
  CONSTRAINT `usergroups_users_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `usergroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `usergroups_users_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usergroups_users`
--

LOCK TABLES `usergroups_users` WRITE;
/*!40000 ALTER TABLE `usergroups_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `usergroups_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userpermissions`
--

DROP TABLE IF EXISTS `userpermissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userpermissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `userpermissions_name_unq_idx` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userpermissions`
--

LOCK TABLES `userpermissions` WRITE;
/*!40000 ALTER TABLE `userpermissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `userpermissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userpermissions_usergroups`
--

DROP TABLE IF EXISTS `userpermissions_usergroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userpermissions_usergroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permissionId` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `userpermissions_usergroups_permissionId_groupId_unq_idx` (`permissionId`,`groupId`),
  KEY `userpermissions_usergroups_groupId_idx` (`groupId`),
  CONSTRAINT `userpermissions_usergroups_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `usergroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `userpermissions_usergroups_permissionId_fk` FOREIGN KEY (`permissionId`) REFERENCES `userpermissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userpermissions_usergroups`
--

LOCK TABLES `userpermissions_usergroups` WRITE;
/*!40000 ALTER TABLE `userpermissions_usergroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `userpermissions_usergroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userpermissions_users`
--

DROP TABLE IF EXISTS `userpermissions_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userpermissions_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permissionId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `userpermissions_users_permissionId_userId_unq_idx` (`permissionId`,`userId`),
  KEY `userpermissions_users_userId_idx` (`userId`),
  CONSTRAINT `userpermissions_users_permissionId_fk` FOREIGN KEY (`permissionId`) REFERENCES `userpermissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `userpermissions_users_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userpermissions_users`
--

LOCK TABLES `userpermissions_users` WRITE;
/*!40000 ALTER TABLE `userpermissions_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `userpermissions_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userpreferences`
--

DROP TABLE IF EXISTS `userpreferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userpreferences` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `preferences` text,
  PRIMARY KEY (`userId`),
  CONSTRAINT `userpreferences_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userpreferences`
--

LOCK TABLES `userpreferences` WRITE;
/*!40000 ALTER TABLE `userpreferences` DISABLE KEYS */;
INSERT INTO `userpreferences` VALUES (1,'{\"language\":\"en-US\"}');
/*!40000 ALTER TABLE `userpreferences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `photoId` int(11) DEFAULT NULL,
  `firstName` varchar(100) DEFAULT NULL,
  `lastName` varchar(100) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `locked` tinyint(1) NOT NULL DEFAULT '0',
  `suspended` tinyint(1) NOT NULL DEFAULT '0',
  `pending` tinyint(1) NOT NULL DEFAULT '0',
  `lastLoginDate` datetime DEFAULT NULL,
  `lastLoginAttemptIp` varchar(45) DEFAULT NULL,
  `invalidLoginWindowStart` datetime DEFAULT NULL,
  `invalidLoginCount` tinyint(3) unsigned DEFAULT NULL,
  `lastInvalidLoginDate` datetime DEFAULT NULL,
  `lockoutDate` datetime DEFAULT NULL,
  `hasDashboard` tinyint(1) NOT NULL DEFAULT '0',
  `verificationCode` varchar(255) DEFAULT NULL,
  `verificationCodeIssuedDate` datetime DEFAULT NULL,
  `unverifiedEmail` varchar(255) DEFAULT NULL,
  `passwordResetRequired` tinyint(1) NOT NULL DEFAULT '0',
  `lastPasswordChangeDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `users_uid_idx` (`uid`),
  KEY `users_verificationCode_idx` (`verificationCode`),
  KEY `users_email_idx` (`email`),
  KEY `users_username_idx` (`username`),
  KEY `users_photoId_fk` (`photoId`),
  CONSTRAINT `users_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `users_photoId_fk` FOREIGN KEY (`photoId`) REFERENCES `assets` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin',NULL,NULL,NULL,'marc.baur@youengineering.ch','$2y$13$o/YB3kdnDqYqTS/PHTAkVOPfnbaSxaQdYTNFkJdJSg//SnLeEUWIS',1,0,0,0,'2021-05-21 13:40:34',NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,0,'2021-01-07 10:01:16','2021-01-07 10:01:16','2021-05-21 13:40:36','bf96c981-e4c0-4422-b545-e96205c972a9');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `volumefolders`
--

DROP TABLE IF EXISTS `volumefolders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `volumefolders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentId` int(11) DEFAULT NULL,
  `volumeId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `volumefolders_name_parentId_volumeId_unq_idx` (`name`,`parentId`,`volumeId`),
  KEY `volumefolders_parentId_idx` (`parentId`),
  KEY `volumefolders_volumeId_idx` (`volumeId`),
  CONSTRAINT `volumefolders_parentId_fk` FOREIGN KEY (`parentId`) REFERENCES `volumefolders` (`id`) ON DELETE CASCADE,
  CONSTRAINT `volumefolders_volumeId_fk` FOREIGN KEY (`volumeId`) REFERENCES `volumes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `volumefolders`
--

LOCK TABLES `volumefolders` WRITE;
/*!40000 ALTER TABLE `volumefolders` DISABLE KEYS */;
INSERT INTO `volumefolders` VALUES (1,NULL,1,'Uploads','','2021-05-21 13:41:01','2021-05-21 13:41:01','034a8d62-4e80-41c6-a315-3d631bd7b326'),(2,NULL,2,'News','','2021-05-21 13:41:01','2021-05-21 13:41:01','2bb33255-2324-484f-ad51-a833b3f0dd48');
/*!40000 ALTER TABLE `volumefolders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `volumes`
--

DROP TABLE IF EXISTS `volumes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `volumes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '1',
  `url` varchar(255) DEFAULT NULL,
  `settings` text,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `volumes_name_idx` (`name`),
  KEY `volumes_handle_idx` (`handle`),
  KEY `volumes_fieldLayoutId_idx` (`fieldLayoutId`),
  KEY `volumes_dateDeleted_idx` (`dateDeleted`),
  CONSTRAINT `volumes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `volumes`
--

LOCK TABLES `volumes` WRITE;
/*!40000 ALTER TABLE `volumes` DISABLE KEYS */;
INSERT INTO `volumes` VALUES (1,8,'Uploads','uploads','craft\\volumes\\Local',1,'@web/uploads','{\"path\":\"@webroot/uploads\"}',1,'2021-05-21 13:41:01','2021-05-21 13:41:01',NULL,'9ab8fbf3-1f45-4e90-ae46-4541be742a4c'),(2,9,'News','news','craft\\volumes\\Local',1,'@web/news','{\"path\":\"@webroot/news\"}',2,'2021-05-21 13:41:01','2021-05-21 13:41:01',NULL,'4728db2c-e1bf-4b67-9928-84174a45345e');
/*!40000 ALTER TABLE `volumes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `widgets`
--

DROP TABLE IF EXISTS `widgets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `widgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `colspan` tinyint(3) DEFAULT NULL,
  `settings` text,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `widgets_userId_idx` (`userId`),
  CONSTRAINT `widgets_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `widgets`
--

LOCK TABLES `widgets` WRITE;
/*!40000 ALTER TABLE `widgets` DISABLE KEYS */;
INSERT INTO `widgets` VALUES (1,1,'craft\\widgets\\RecentEntries',1,NULL,'{\"siteId\":1,\"section\":\"*\",\"limit\":10}',1,'2021-05-21 13:40:36','2021-05-21 13:40:36','50b54a26-8581-40d9-981a-df781503a2c0'),(2,1,'craft\\widgets\\CraftSupport',2,NULL,'[]',1,'2021-05-21 13:40:36','2021-05-21 13:40:36','9b124f08-e1a2-42e8-b49b-37f6508ff973'),(3,1,'craft\\widgets\\Updates',3,NULL,'[]',1,'2021-05-21 13:40:36','2021-05-21 13:40:36','c6250d88-bb6f-4a4c-b473-fbe9faef8e78'),(4,1,'craft\\widgets\\Feed',4,NULL,'{\"url\":\"https://craftcms.com/news.rss\",\"title\":\"Craft News\",\"limit\":5}',1,'2021-05-21 13:40:36','2021-05-21 13:40:36','6f9b0b41-f64f-494d-83ba-94fff530b19a');
/*!40000 ALTER TABLE `widgets` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-21 13:44:04
