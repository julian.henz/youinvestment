#!/usr/bin/env bash

DOCKER_CONTAINER="youInvestmentDb"
USER="root"
PASSWORD="2013liestal"
# TODO_INIT change
DATABASE="craft-youInvestment"
INSERT_FILE=initdb/dump.sql

cat ${INSERT_FILE} | docker exec -i ${DOCKER_CONTAINER} /usr/bin/mysql -u ${USER} --password=${PASSWORD} ${DATABASE}

if [[ $? -eq 0 ]]; then
    echo ">>> OK - Data has been inserted."
fi
