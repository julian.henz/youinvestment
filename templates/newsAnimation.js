let links = document.querySelectorAll('.News');

for (let link of links) {
    link.addEventListener('click', () => {

        if(link.children[0].style.height == "100%"){
            link.children[0].style.height = "30rem";
            link.children[0].document.getElementById("arrow").style.transform = "rotate(0deg)";
        }else {
            link.children[0].style.height = "100%";
            link.children[0].document.getElementById("arrow").style.transform = "rotate(180deg)";
        }


    });
}

let arrows = document.querySelectorAll('.arrow');

for (let link of arrows) {
    link.addEventListener('click', () => {
        if(link.children[0].style.transform == "rotate(180deg)"){
            link.children[0].style.transform = "rotate(0deg)";
        }else {
            link.children[0].style.transform = "rotate(180deg)";
        }
    });
}
