<?php

if (!empty($_POST)) {
    $Vorname = $_POST['Vorname'];
    $Nachname = $_POST['Nachname'];
    $Anzahl = $_POST['Anzahl'];
    $Kategorie = $_POST['Kategorie'];

    $endpoint = 'https://api.icndb.com/jokes/random/';

    if (count($Kategorie) == 0) {
        $Kat = "";
    } else if (count($Kategorie) == 1) {
        $Kat = $Kategorie[0];
    } else if (count($Kategorie) == 2) {
        $Kat = $Kategorie[0] . ',' . $Kategorie[1];
    }

    $url = $endpoint . urlencode($Anzahl) . "?firstName=" . urlencode($Vorname) . "&lastName=" . urlencode($Nachname) . "&limitTo=[$Kat]";

    $json = file_get_contents($url);
    $data = json_decode($json, true);

    if (!file_exists("chucki.json")) fopen("chucki.json", "w");

    $jsonArray = $data;

    $json = json_encode($jsonArray, JSON_PRETTY_PRINT);
    file_put_contents("chucki.json", $json);

}
?>
