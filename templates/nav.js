const navSlide = () => {

    const burger = document.querySelector('.burger');
    const nav = document.querySelector('.nav-links');
    const navLinks = document.querySelectorAll('.nav-links li');

    burger.addEventListener('click', () => {

        nav.classList.toggle('nav-active');

        navLinks.forEach((link, index) => {
            if (link.style.animation) {
                link.style.animation = '';
            } else {
                link.style.animation = `navLinkFade 0.5s ease forwards ${index / 7 + 0.3}`
            }
        });

        burger.classList.toggle('toggle');
    });
}

navSlide();

function removeClass() {
    let element = document.getElementById("navigation-burger");
    element.classList.remove("toggle");

    let ncontainer = document.getElementById("nav-content");
    ncontainer.classList.remove("nav-active");

    let elementLogo = document.getElementById("Ebene_1");
    elementLogo.classList.remove("logoColor-Active");
    elementLogo.getElementById("slash-logo").style.fill = "#0096ff";
}

document.addEventListener('scroll', () => {
    let element = document.getElementById("navigation-burger");
    element.classList.remove("toggle");

    let ncontainer = document.getElementById("nav-content");
    ncontainer.classList.remove("nav-active");

    let elementLogo = document.getElementById("Ebene_1");
    elementLogo.classList.remove("logoColor-Active");
    elementLogo.getElementById("slash-logo").style.fill = "#0096ff";
});

function changeLogoColor() {
    let element = document.getElementById("Ebene_1");

    element.classList.toggle('logoColor-Active');

    if(element.getElementById("slash-logo").style.fill == "black" || element.getElementById("slash-logo").style.fill == "#0096ff"){
        element.getElementById("slash-logo").style.fill = "#0096ff";
    }else{
        element.getElementById("slash-logo").style.fill = "black";
    }

}

let navHeader = document.querySelectorAll('.navigation-text');
let navigationImage = document.querySelectorAll('.navigation-image');

for (let link of navHeader) {

    link.addEventListener('mouseover', () => {

        link.children[0].style.color = "#000000";
        let imagName = link.children[0].innerText;
        document.getElementById(imagName+"-image").classList.toggle('hidden');

    });

    link.addEventListener('mouseout', () => {

        link.children[0].style.color = "white";
        let imagName = link.children[0].innerText;
        document.getElementById(imagName+"-image").classList.add('hidden');

    });
}
